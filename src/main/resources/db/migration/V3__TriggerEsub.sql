/*trigger spaj*/
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_spaj_number()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE new_spaj_code varchar;
    DECLARE check_spaj varchar;
   check_exist_date varchar;

BEGIN
    SELECT INTO check_spaj
        LPAD(cast(cast(substring(spaj_code, 8) AS integer) + 1 AS varchar), 4, '0')
    FROM m_spaj
    ORDER BY id
        DESC LIMIT 1;


    IF check_spaj IS NULL THEN
        new.spaj_code := concat('E', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));

    ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_code, 2, 6) AS varchar)
	    FROM m_spaj
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_code := concat('E', to_char(now(), 'yymmdd'), LPAD(check_spaj, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_code := concat('E', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_spaj_number
  ON public.m_spaj;
CREATE TRIGGER generate_spaj_number
    BEFORE INSERT
    ON public.m_spaj
    FOR EACH ROW
EXECUTE FUNCTION generate_spaj_number();


/* trigger proposal number */
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_proposal_number()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE new_proposal_no varchar;
    DECLARE check_proposal varchar;
   check_exist_date varchar;

BEGIN
    SELECT INTO check_proposal
        LPAD(cast(cast(substring(proposal_no, 8) AS integer) + 1 AS varchar), 4, '0')
    FROM m_proposal
    ORDER BY id
        DESC LIMIT 1;


    IF check_proposal IS NULL THEN
        new.proposal_no := concat('P', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));

    ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(proposal_no, 2, 6) AS varchar)
	    FROM m_proposal
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.proposal_no := concat('P', to_char(now(), 'yymmdd'), LPAD(check_proposal, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.proposal_no := concat('P', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

/*Trigger*/
DROP TRIGGER IF EXISTS generate_proposal_number
  ON public.m_proposal;
CREATE TRIGGER generate_proposal_number
    BEFORE INSERT
    ON public.m_proposal
    FOR EACH ROW
EXECUTE FUNCTION generate_proposal_number();


/* trigger spaj_document_history */
/*Trigger Function*/
CREATE OR REPLACE FUNCTION public.generate_spaj_document_history_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE check_spaj_document_history varchar;
declare check_exist_date varchar;

BEGIN
  SELECT INTO check_spaj_document_history
                LPAD(cast(cast(substring(spaj_document_history_code, 9) AS INTEGER) + 1 AS varchar), 4, '0')
  FROM m_spaj_document_history
  ORDER BY id
  DESC LIMIT 1;

  IF check_spaj_document_history IS NULL THEN
          new.spaj_document_history_code := concat('SH', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_document_history_code, 3, 6) AS varchar)
	    FROM m_spaj_document_history
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_document_history_code := concat('SH', to_char(now(), 'yymmdd'), LPAD(check_spaj_document_history, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_document_history_code := concat('SH', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

DROP TRIGGER IF EXISTS generate_spaj_document_history_code
  ON public. m_spaj_document_history;
CREATE TRIGGER generate_spaj_document_history_code
  BEFORE INSERT
  ON public.m_spaj_document_history
  FOR EACH ROW
  EXECUTE FUNCTION generate_spaj_document_history_code();

/* */
CREATE OR REPLACE FUNCTION public.generate_spaj_beneficiary_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE check_spaj_beneficiary varchar;
check_exist_date varchar;

BEGIN
  SELECT INTO check_spaj_beneficiary
                LPAD(cast(cast(substring(spaj_beneficiary_code, 9) AS integer) + 1 AS varchar), 4, '0')
  FROM m_spaj_beneficiary
  ORDER BY id
  DESC LIMIT 1;

  IF check_spaj_beneficiary IS NULL THEN
          new.spaj_beneficiary_code := concat('SB', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_beneficiary_code, 3, 6) AS varchar)
	    FROM m_spaj_beneficiary
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_beneficiary_code := concat('SB', to_char(now(), 'yymmdd'), LPAD(check_spaj_beneficiary, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_beneficiary_code := concat('SB', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;


DROP TRIGGER IF EXISTS generate_spaj_beneficiary_code
  ON public.m_spaj_beneficiary;
CREATE TRIGGER generate_spaj_beneficiary_code
    BEFORE INSERT
    ON public.m_spaj_beneficiary
    FOR EACH ROW
EXECUTE FUNCTION generate_spaj_beneficiary_code();

/* */
CREATE OR REPLACE FUNCTION public.generate_spaj_premium_payment_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE check_spaj_premium varchar;
check_exist_date varchar;

BEGIN
  SELECT INTO check_spaj_premium
                LPAD(cast(cast(substring(spaj_premium_payment_code, 9) AS integer) + 1 AS varchar), 4, '0')
  FROM m_spaj_premium_payment
  ORDER BY id
  DESC LIMIT 1;

  IF check_spaj_premium IS NULL THEN
          new.spaj_premium_payment_code := concat('SP', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_premium_payment_code, 3, 6) AS varchar)
	    FROM m_spaj_premium_payment
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_premium_payment_code := concat('SP', to_char(now(), 'yymmdd'), LPAD(check_spaj_premium, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_premium_payment_code := concat('SP', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

DROP TRIGGER IF EXISTS generate_spaj_premium_payment_code
ON public.m_spaj_premium_payment;
create trigger generate_spaj_premium_payment_code before
insert on
public.m_spaj_premium_payment for each row execute function generate_spaj_premium_payment_code();

/**/
CREATE OR REPLACE FUNCTION public.generate_spaj_health_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE check_spaj_health varchar;
check_exist_date varchar;

BEGIN
  SELECT INTO check_spaj_health
                LPAD(cast(cast(substring(spaj_health_code, 9) AS integer) + 1 AS varchar), 4, '0')
  FROM m_spaj_health
  ORDER BY id
  DESC LIMIT 1;

  IF check_spaj_health IS NULL THEN
          new.spaj_health_code := concat('ST', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_health_code, 3, 6) AS varchar)
	    FROM m_spaj_health
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_health_code := concat('ST', to_char(now(), 'yymmdd'), LPAD(check_spaj_health, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_health_code := concat('ST', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

DROP TRIGGER IF EXISTS generate_spaj_health_code
ON public.m_spaj_health;
create trigger generate_spaj_health_code before
insert
on
public.m_spaj_health for each row execute function generate_spaj_health_code();

/**/
CREATE OR REPLACE FUNCTION public.generate_spaj_policy_holder_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE check_spaj_policy_holder varchar;
check_exist_date varchar;

BEGIN
  SELECT INTO check_spaj_policy_holder
                LPAD(cast(cast(substring(spaj_policy_holder_code, 9) AS integer) + 1 AS varchar), 4, '0')
  FROM m_spaj_policy_holder
  ORDER BY id
  DESC LIMIT 1;

  IF check_spaj_policy_holder IS NULL THEN
          new.spaj_policy_holder_code := concat('PY', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_policy_holder_code, 3, 6) AS varchar)
	    FROM m_spaj_policy_holder
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_policy_holder_code := concat('PY', to_char(now(), 'yymmdd'), LPAD(check_spaj_policy_holder, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_policy_holder_code := concat('PY', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

DROP TRIGGER IF EXISTS generate_spaj_policy_holder_code
ON public.m_spaj_policy_holder;
create trigger generate_spaj_policy_holder_code before
insert
on
public.m_spaj_policy_holder for each row execute function generate_spaj_policy_holder_code();

/**/
CREATE OR REPLACE FUNCTION public.generate_spaj_additional_stat_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE check_spaj_additional_stat varchar;
check_exist_date varchar;

BEGIN
  SELECT INTO check_spaj_additional_stat
                LPAD(cast(cast(substring(spaj_additional_stat_code, 9) AS integer) + 1 AS varchar), 4, '0')
  FROM m_spaj_additional_stat
  ORDER BY id
  DESC LIMIT 1;

  IF check_spaj_additional_stat IS NULL THEN
          new.spaj_additional_stat_code := concat('SA', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_additional_stat_code, 3, 6) AS varchar)
	    FROM m_spaj_additional_stat
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_additional_stat_code := concat('SA', to_char(now(), 'yymmdd'), LPAD(check_spaj_additional_stat, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_additional_stat_code := concat('SA', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

DROP TRIGGER IF EXISTS generate_spaj_additional_stat_code
ON public.m_spaj_additional_stat;
create trigger generate_spaj_additional_stat_code before
insert
on
public.m_spaj_additional_stat for each row execute function generate_spaj_additional_stat_code();

/**/
CREATE OR REPLACE FUNCTION public.generate_spaj_document_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE check_spaj_document varchar;
check_exist_date varchar;

BEGIN
  SELECT INTO check_spaj_document
                LPAD(cast(cast(substring(spaj_document_code, 9) AS integer) + 1 AS varchar), 4, '0')
  FROM m_spaj_document
  ORDER BY id
  DESC LIMIT 1;

  IF check_spaj_document IS NULL THEN
          new.spaj_document_code := concat('SD', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_document_code, 3, 6) AS varchar)
	    FROM m_spaj_document
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_document_code := concat('SD', to_char(now(), 'yymmdd'), LPAD(check_spaj_document, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_document_code := concat('SD', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

DROP TRIGGER IF EXISTS generate_spaj_document_code
ON public.m_spaj_document;
CREATE TRIGGER generate_spaj_document_code
BEFORE INSERT
ON public.m_spaj_document
FOR EACH ROW
EXECUTE FUNCTION generate_spaj_document_code();

/**/
CREATE OR REPLACE FUNCTION public.generate_spaj_coverage_premium_detail_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE spaj_coverage_premium_detail varchar;
check_exist_date varchar;

BEGIN
  SELECT INTO spaj_coverage_premium_detail
                LPAD(cast(cast(substring(spaj_coverage_premium_detail_code, 9) AS integer) + 1 AS varchar), 4, '0')
  FROM m_spaj_coverage_premium_detail
  ORDER BY id
  DESC LIMIT 1;

  IF spaj_coverage_premium_detail IS NULL THEN
          new.spaj_coverage_premium_detail_code := concat('SC', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_coverage_premium_detail_code, 3, 6) AS varchar)
	    FROM m_spaj_coverage_premium_detail
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_coverage_premium_detail_code := concat('SC', to_char(now(), 'yymmdd'), LPAD(spaj_coverage_premium_detail, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_coverage_premium_detail_code := concat('SC', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

DROP TRIGGER IF EXISTS generate_spaj_coverage_premium_detail_code
ON public.m_spaj_coverage_premium_detail;
create trigger generate_spaj_coverage_premium_detail_code before
insert
    on
    public.m_spaj_coverage_premium_detail for each row execute function generate_spaj_coverage_premium_detail_code();

/**/
CREATE OR REPLACE FUNCTION public.generate_spaj_insured_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE spaj_insured varchar;
check_exist_date varchar;

BEGIN
  SELECT INTO spaj_insured
                LPAD(cast(cast(substring(spaj_policy_insured_code, 9) AS integer) + 1 AS varchar), 4, '0')
  FROM m_spaj_insured
  ORDER BY id
  DESC LIMIT 1;

  IF spaj_insured IS NULL THEN
          new.spaj_policy_insured_code := concat('SI', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_policy_insured_code, 3, 6) AS varchar)
	    FROM m_spaj_insured
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_policy_insured_code := concat('SI', to_char(now(), 'yymmdd'), LPAD(spaj_insured, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_policy_insured_code := concat('SI', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

CREATE OR REPLACE FUNCTION public.generate_spaj_statement_letter_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE spaj_statement_letter varchar;
check_exist_date varchar;

BEGIN
  SELECT INTO spaj_statement_letter
                LPAD(cast(cast(substring(spaj_statement_letter_code, 9) AS integer) + 1 AS varchar), 4, '0')
  FROM m_spaj_statement_letter
  ORDER BY id
  DESC LIMIT 1;

  IF spaj_statement_letter IS NULL THEN
          new.spaj_statement_letter_code := concat('SL', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_statement_letter_code, 3, 6) AS varchar)
	    FROM m_spaj_statement_letter
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_statement_letter_code := concat('SL', to_char(now(), 'yymmdd'), LPAD(spaj_statement_letter, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_statement_letter_code := concat('SL', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

DROP TRIGGER IF EXISTS generate_spaj_statement_letter_code ON public. m_spaj_statement_letter;
CREATE TRIGGER generate_spaj_statement_letter_code
BEFORE INSERT
ON public.m_spaj_statement_letter
FOR EACH ROW
EXECUTE FUNCTION generate_spaj_statement_letter_code();

/**/
CREATE OR REPLACE FUNCTION public.generate_spaj_agen_statement_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE check_spaj_agen_statement varchar;
check_exist_date varchar;

BEGIN
  SELECT INTO check_spaj_agen_statement
                LPAD(cast(cast(substring(spaj_agen_statement_code, 9) AS integer) + 1 AS varchar), 4, '0')
  FROM m_spaj_agen_statement
  ORDER BY id
  DESC LIMIT 1;

  IF check_spaj_agen_statement IS NULL THEN
          new.spaj_agen_statement_code := concat('AS', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_agen_statement_code, 3, 6) AS varchar)
	    FROM m_spaj_agen_statement
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_agen_statement_code := concat('AS', to_char(now(), 'yymmdd'), LPAD(check_spaj_agen_statement, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_agen_statement_code := concat('AS', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

DROP TRIGGER IF EXISTS generate_spaj_agen_statement_code ON public. m_spaj_agen_statement;
create trigger generate_spaj_agen_statement_code before
insert
    on
    public.m_spaj_agen_statement for each row execute function generate_spaj_agen_statement_code();

/**/
CREATE OR REPLACE FUNCTION public.generate_spaj_document_file_code()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE check_spaj_document_file varchar;
check_exist_date varchar;

BEGIN
  SELECT INTO check_spaj_document_file
                LPAD(cast(cast(substring(spaj_document_file_code, 9) AS integer) + 1 AS varchar), 4, '0')
  FROM m_spaj_document_file
  ORDER BY id
  DESC LIMIT 1;

  IF check_spaj_document_file IS NULL THEN
          new.spaj_document_file_code := concat('SF', to_char(now(), 'yymmdd'), LPAD('1', 4, '0'));
  ELSE

	    SELECT INTO check_exist_date
	    	CAST(SUBSTRING(spaj_document_file_code, 3, 6) AS varchar)
	    FROM m_spaj_document_file
	    ORDER BY id
	    DESC LIMIT 1;

   		CASE WHEN check_exist_date = CAST(to_char(now(), 'yymmdd') AS varchar) THEN
        	new.spaj_document_file_code := concat('SF', to_char(now(), 'yymmdd'), LPAD(check_spaj_document_file, 4, '0'));

       		WHEN check_exist_date <> CAST (to_char(now(), 'yymmdd') AS varchar) THEN
       		new.spaj_document_file_code := concat('SF', to_char(now(), 'yymmdd' ), LPAD('1', 4, '0'));
    	END CASE ;
    END IF;
    RETURN NEW;
END;
$function$;

DROP TRIGGER IF EXISTS generate_spaj_document_file_code ON public. m_spaj_document_file;
CREATE TRIGGER generate_spaj_document_file_code
BEFORE INSERT
ON public.m_spaj_document_file
FOR EACH ROW
EXECUTE FUNCTION generate_spaj_document_file_code();