package id.equity.nichemarket.retrofit.eSubmission;

import id.equity.nichemarket.service.eSubmissionStore.ESubmissionDokumenData;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Service
public class EditsService {
    private static final String BASE_URL = "http://dev-ws01.elife.co.id/dev-edits-api/index.php/";
    private Retrofit retrofit;
    private EditsApi editsApi;

    public EditsService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        editsApi = retrofit.create(EditsApi.class);
    }

    public EditsResponse postDataToEdits(ESubmissionDokumenData jsonNasabah) throws Exception {
        Call<EditsResponse> dataResponse = editsApi.postDataToEdits(jsonNasabah);
        return dataResponse.execute().body();
    }

    public EditsResponse resendToEdits(ESubmissionDokumenData jsonNasabah) throws Exception {
        Call<EditsResponse> dataResponse = editsApi.resendToEdits(jsonNasabah);
        return dataResponse.execute().body();
    }
}