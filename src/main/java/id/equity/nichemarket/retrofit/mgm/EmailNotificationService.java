package id.equity.nichemarket.retrofit.mgm;

import com.google.gson.JsonObject;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Service
public class EmailNotificationService {
    private static final String BASE_URL = "https://sit-eli.myequity.id/";
//    private static final String BASE_URL = "http://dev-ws01.elife.co.id/dev-api/api_niche/";
    private Retrofit retrofit;
    private EmailNotificationApi emailNotificationApi;

    public EmailNotificationService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
         emailNotificationApi = retrofit.create(EmailNotificationApi.class);
    }

    public EmailNotificationResponse sendEmailNotif(JsonObject jsonEmail){
        try{
            Call<EmailNotificationResponse> dataResponse = emailNotificationApi.sendEmailNotif(jsonEmail);
            EmailNotificationResponse res = dataResponse.execute().body();
            return res;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public EmailNotificationResponse sendNotif(JsonObject jsonEmail){
        try{
            Call<EmailNotificationResponse> dataResponse = emailNotificationApi.sendNotif(jsonEmail);
            EmailNotificationResponse res =  dataResponse.execute().body();
            return res;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
