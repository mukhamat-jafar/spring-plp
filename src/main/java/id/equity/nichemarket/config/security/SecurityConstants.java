package id.equity.nichemarket.config.security;

public class SecurityConstants {
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String SECRET = "$2y$12$aJ/FrhCGm6DLs0hw0SJrcOj1oVkRX1msn7su40p8qRfA6Co8Zp2Y6";
    public static final String SIGN_UP_URL = "/api/v1/users";
    public static final String TOKEN_PREFIX = "Bearer ";
}
