package id.equity.nichemarket.config.response;

import lombok.Data;

@Data
public class BaseResponse<T> {
    private boolean ok;
    private T data;
    private String message;

    public BaseResponse(boolean ok, T data, String message) {
        this.ok = ok;
        this.data = data;
        this.message = message;
    }
}