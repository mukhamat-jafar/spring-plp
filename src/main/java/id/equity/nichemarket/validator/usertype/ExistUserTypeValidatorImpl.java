package id.equity.nichemarket.validator.usertype;

import id.equity.nichemarket.service.rm.UserTypeService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ExistUserTypeValidatorImpl implements ConstraintValidator<ExistUserTypeValidator, String> {
    @Autowired
    UserTypeService userTypeService;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null) return true;

        return userTypeService.isUserTypeExist(value);
    }
}
