package id.equity.nichemarket;

import id.equity.nichemarket.property.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication
@EnableConfigurationProperties({FileStorageProperties.class})
@IntegrationComponentScan
@EnableIntegration
public class NichemarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(NichemarketApplication.class, args);
	}

}