package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class DataKesehatan {
    private BeratBadan berat_badan;
    private TinggiBadan tinggi_badan;
    private GolonganDarah golongan_darah;
    private String hq_1_P1;
    private String hq_1_L1;
    private String hq_2a_P1;
    private String hq_2a_L1;
    private String hq_2b_P1;
    private String hq_2b_L1;
    private String hq_2c_P1;
    private String hq_2c_L1;
    private String hq_2d_P1;
    private String hq_2d_L1;
    private String hq_2e_P1;
    private String hq_2e_L1;
    private String hq_2f_P1;
    private String hq_2f_L1;
    private String hq_2g_P1;
    private String hq_2g_L1;
    private String hq_2h_P1;
    private String hq_2h_L1;
    private String hq_2i_P1;
    private String hq_2i_L1;
    private String hq_3_P1;
    private String hq_3_L1;
    private String hq_4_P1;
    private String hq_4_L1;
    private String hq_5_P1;
    private String hq_5_L1;
    private String hq_6a_P1;
    private String hq_6a_P1_bulan;
    private String hq_6a_L1;
    private String hq_6a_L1_bulan;
    private String hq_6b_P1;
    private String hq_6b_L1;
    private String hq_6c_P1;
    private String hq_6c_L1;
    private String hq_6d_P1;
    private String hq_6d_L1;
    private String hq_7_P1;
    private String hq_7_L1;
    private String hq_keterangan;
}
