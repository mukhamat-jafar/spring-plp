package id.equity.nichemarket.service.eSubmissionStore.CalonTertanggung;

import lombok.Data;

@Data
public class AlamatKorespondensi {
    private String alamat_korespondensi;
    private AlamatKorespondensiLainnya lainnya;
}
