package id.equity.nichemarket.service.eSubmissionStore.CalonTertanggung;

import lombok.Data;

@Data
public class Agama {
    private String agama;
    private String lainnya;
}
