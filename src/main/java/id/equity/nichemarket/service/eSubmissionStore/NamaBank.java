package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class NamaBank {
    private String nama_bank;
    private String lainnya;
}
