package id.equity.nichemarket.service.eSubmissionStore;

import id.equity.nichemarket.model.es.*;
import id.equity.nichemarket.service.eSubmissionStore.CalonTertanggung.DataCalonTertanggung;
import id.equity.nichemarket.service.eSubmissionStore.PemegangPolis.DataCalonPemegangPolis;
import id.equity.nichemarket.service.es.*;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Data
@Service
public class ESubmissionDataService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private SpajCoveragePremiumDetailService spajCoveragePremiumDetailService;
    @Autowired
    private EntityManager em;
    @Autowired
    private SpajDocumentService spajDocumentService;
    @Autowired
    private SpajPolicyHolderService spajPolicyHolderService;
    @Autowired
    private SpajInsuredService spajInsuredService;
    @Autowired
    private SpajAgenStatementService spajAgenStatementService;
    @Autowired
    private SpajPremiumPaymentService spajPremiumPaymentService;
    @Autowired
    private SpajBeneficiaryService spajBeneficiaryService;
    @Autowired
    private SpajHealthService spajHealthService;
    @Autowired
    private SpajAdditionalStatService spajAdditionalStatService;
    @Autowired
    private SpajStatementLetterService spajStatementLetterService;
    @Autowired
    private SpajDocumentHistoryService spajDocumentHistoryService;

    private String tipe_dokumen;
    private String no_spaj;
    private String kantor_pemasaran;
    private String no_polis;
    private String no_proposal;
    private String marketing_program;
    private DataCalonPemegangPolis data_calon_pemegang_polis;
    private DataCalonTertanggung data_calon_tertanggung;
    private PerincianPertanggunganDanPremi perincian_pertanggungan_dan_premi;
    private PembayaranPremi pembayaran_premi;
    private Termaslahat[] termaslahat;
    private DataKesehatan data_kesehatan;
    private DataTambahanStatistik data_tambahan_statistik;
    private SuratPernyataan surat_pernyataan;
    private SuratPernyataanAgen surat_pernyataan_agen;
    private ChecklistDokumen checklist_dokumen;
    private String kode_soa;
    private String catatan_underwriting;
    private String[] pending_code;

    @Transactional
    public void save(ESubmissionBaseData eSubmissionBaseData, String spajCode, String noProposal){

        //Saving Spaj_Document
        SpajDocument spajDocument = modelMapper.map(eSubmissionBaseData, SpajDocument.class);
        spajDocumentService.savingSpajDocument(spajDocument, eSubmissionBaseData, spajCode, noProposal);
        //Saving Spaj_Policy_Holder
        spajPolicyHolderService.savingSpajPolicyHolder(spajDocument, eSubmissionBaseData, spajCode);
        //Saving Spaj Insured
        spajInsuredService.savingSpajInsured(spajDocument, eSubmissionBaseData, spajCode);
        //Saving Spaj agent
        spajAgenStatementService.savingSpajAgent(spajDocument, eSubmissionBaseData, spajCode);
        //saving spaj coverage
        spajCoveragePremiumDetailService.savingSpajCoverage(spajDocument, eSubmissionBaseData, spajCode);
        //saving spaj premium
        spajPremiumPaymentService.savingSpajPremium(spajDocument, eSubmissionBaseData, spajCode);
        //saving spaj Beneficiary
        spajBeneficiaryService.savingSpajBeneficiary(spajDocument, eSubmissionBaseData, spajCode);
        //saving spaj health
        spajHealthService.savingSpajHealth(spajDocument, eSubmissionBaseData, spajCode);
       //saving spaj additional
        spajAdditionalStatService.savingSpajAdditional(spajDocument, eSubmissionBaseData, spajCode);
        //saving spaj statement
        spajStatementLetterService.savingSpajStatement(spajDocument, eSubmissionBaseData, spajCode);
        //saving spaj document history
        spajDocumentHistoryService.savingSpajDocumentHistory(spajDocument, eSubmissionBaseData);
    }
}