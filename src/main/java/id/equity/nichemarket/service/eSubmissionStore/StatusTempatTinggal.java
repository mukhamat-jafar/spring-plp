package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class StatusTempatTinggal {
    private String status_tempat_tinggal;
    private String lainnya;
}
