package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class ESubmissionBaseData {
    private String unique_id;
    private String mode;
    private String kode_sumber;
    private ESubmissionDataService data;
    private ESubmissionDokumenData dokumen_data;
}
