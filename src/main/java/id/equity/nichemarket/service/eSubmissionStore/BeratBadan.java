package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class BeratBadan {
    private String pemegang_polis;
    private String tertanggung;
}
