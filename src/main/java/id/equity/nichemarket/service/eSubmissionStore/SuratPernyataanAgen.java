package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class SuratPernyataanAgen {
    private String nama_agen;
    private String kode_agen;
    private String jabatan;
}
