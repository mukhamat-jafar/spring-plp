package id.equity.nichemarket.service.eSubmissionStore;

import lombok.Data;

@Data
public class ChecklistDokumen {
    private String spaj;
    private String fotocopy_identitas_diri;
    private String profil_risiko;
    private String proposal;
    private String copy_bukti_transfer;
    private String lain_lain;

}
