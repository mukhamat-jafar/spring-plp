package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajBeneficiary.CreateSpajBeneficiary;
import id.equity.nichemarket.dto.es.SpajBeneficiary.SpajBeneficiaryDto;
import id.equity.nichemarket.model.es.SpajBeneficiary;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.repository.es.SpajBeneficiaryRepository;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import id.equity.nichemarket.service.eSubmissionStore.Termaslahat;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajBeneficiaryService {
    @Autowired
    private SpajBeneficiaryRepository spajBeneficiaryRepository;
    
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get All Data
    public ResponseEntity<List<SpajBeneficiaryDto>> listSpajBeneficiary() {
        List<SpajBeneficiary> spajBeneficiary= spajBeneficiaryRepository.findAll();
        Type targetType = new TypeToken<List<SpajBeneficiaryDto>>() {}.getType();
        List<SpajBeneficiaryDto> response = modelMapper.map(spajBeneficiary, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajBeneficiaryDto> getSpajBeneficiaryById(Long id) {
        SpajBeneficiary spajBeneficiary = spajBeneficiaryRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Beneficiary id " + id + " is not exist"));
        SpajBeneficiaryDto response = modelMapper.map(spajBeneficiary, SpajBeneficiaryDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajBeneficiaryDto> addSpajBeneficiary(CreateSpajBeneficiary newSpajBeneficiary) {
        SpajBeneficiary spajBeneficiary = modelMapper.map(newSpajBeneficiary, SpajBeneficiary.class);
        SpajBeneficiary SpajBeneficiarySaved = spajBeneficiaryRepository.save(spajBeneficiary);
        SpajBeneficiaryDto response = modelMapper.map(SpajBeneficiarySaved, SpajBeneficiaryDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajBeneficiaryDto> editSpajBeneficiary(CreateSpajBeneficiary updateSpajBeneficiary, Long id) {
        SpajBeneficiary spajBeneficiary = spajBeneficiaryRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Beneficiary id " + id + " is not exist"));
        spajBeneficiary.setSpajBeneficiaryCode(updateSpajBeneficiary.getSpajBeneficiaryCode());
        spajBeneficiary.setSpajDocumentCode(updateSpajBeneficiary.getSpajDocumentCode());
        spajBeneficiary.setSpajNo(updateSpajBeneficiary.getSpajNo());
        spajBeneficiary.setBeneficiary1(updateSpajBeneficiary.getBeneficiary1());
        spajBeneficiary.setRelationship1(updateSpajBeneficiary.getRelationship1());
        spajBeneficiary.setDateOfBirth1(updateSpajBeneficiary.getDateOfBirth1());
        spajBeneficiary.setGender1(updateSpajBeneficiary.getGender1());
        spajBeneficiary.setPercentage1(updateSpajBeneficiary.getPercentage1());
        spajBeneficiary.setActive(updateSpajBeneficiary.isActive());
        spajBeneficiaryRepository.save(spajBeneficiary);
        em.refresh(spajBeneficiary);
        SpajBeneficiaryDto response = modelMapper.map(spajBeneficiary, SpajBeneficiaryDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajBeneficiaryDto> deleteSpajBeneficiary(Long id) {
        SpajBeneficiary spajBeneficiary = spajBeneficiaryRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Beneficiary id " + id + " is not exist"));
        spajBeneficiaryRepository.deleteById(id);
        SpajBeneficiaryDto response = modelMapper.map(spajBeneficiary, SpajBeneficiaryDto.class);

        return ResponseEntity.ok(response);
    }

    //insert data in file json
    @Transactional
    public void savingSpajBeneficiary(SpajDocument spajDocument, ESubmissionBaseData eSubmissionBaseData, String spajCode){

        //master Spaj Beneficiary
        SpajBeneficiary spajBeneficiary = modelMapper.map(eSubmissionBaseData.getData().getTermaslahat(), SpajBeneficiary.class);
        for (Termaslahat termaslahat:eSubmissionBaseData.getData().getTermaslahat()){
            spajBeneficiary.setActive(true);
            spajBeneficiary.setBeneficiary1(termaslahat.getYang_menerima_manfaat());
            spajBeneficiary.setRelationship1(termaslahat.getHub_dengan_calon_tertanggung());
            spajBeneficiary.setDateOfBirth1(termaslahat.getTanggal_lahir());
            spajBeneficiary.setGender1(termaslahat.getJenis_kelamin());
            spajBeneficiary.setPercentage1(termaslahat.getPersentase());

            spajBeneficiary.setSpajNo(spajCode);
            spajBeneficiary.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
            spajBeneficiaryRepository.save(spajBeneficiary);
        }
    }
}