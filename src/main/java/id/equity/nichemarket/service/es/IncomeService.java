package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.Income.CreateIncome;
import id.equity.nichemarket.dto.es.Income.IncomeDto;
import id.equity.nichemarket.model.es.Income;
import id.equity.nichemarket.repository.es.IncomeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class IncomeService {
    @Autowired
    private IncomeRepository incomeRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<IncomeDto>> listIncome() {
        List<Income> income= incomeRepository.findAll();
        Type targetType = new TypeToken<List<IncomeDto>>() {}.getType();
        List<IncomeDto> response = modelMapper.map(income, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<IncomeDto> getIncomeById(Long id) {
        Income income = incomeRepository.findById(id).orElseThrow(()-> new NotFoundException("Income id " + id + " is not exist"));
        IncomeDto response = modelMapper.map(income, IncomeDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<IncomeDto> addIncome(CreateIncome newIncome) {
        Income income = modelMapper.map(newIncome, Income.class);
        Income incomeSaved = incomeRepository.save(income);
        IncomeDto response = modelMapper.map(incomeSaved, IncomeDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<IncomeDto> editIncome(CreateIncome updateIncome, Long id) {
        Income income = incomeRepository.findById(id).orElseThrow(()-> new NotFoundException("Income id " + id + " is not exist"));
        income.setIncomeCode(updateIncome.getIncomeCode());
        income.setDescription(updateIncome.getDescription());
        income.setActive(updateIncome.isActive());
        incomeRepository.save(income);
        IncomeDto response = modelMapper.map(income, IncomeDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<IncomeDto> deleteIncome(Long id) {
        Income income = incomeRepository.findById(id).orElseThrow(()-> new NotFoundException("Income id " + id + " is not exist"));
        incomeRepository.deleteById(id);
        IncomeDto response = modelMapper.map(income, IncomeDto.class);

        return ResponseEntity.ok(response);
    }
}