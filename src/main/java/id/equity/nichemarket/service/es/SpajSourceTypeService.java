package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajSourceType.CreateSpajSourceType;
import id.equity.nichemarket.dto.es.SpajSourceType.SpajSourceTypeDto;
import id.equity.nichemarket.model.es.SpajSourceType;
import id.equity.nichemarket.repository.es.SpajSourceTypeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajSourceTypeService {
    @Autowired
    private SpajSourceTypeRepository SpajSourceTypeRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<SpajSourceTypeDto>> listSpajSourceType() {
        List<SpajSourceType> spajSourceType= SpajSourceTypeRepository.findAll();
        Type targetType = new TypeToken<List<SpajSourceTypeDto>>() {}.getType();
        List<SpajSourceTypeDto> response = modelMapper.map(spajSourceType, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajSourceTypeDto> getSpajSourceTypeById(Long id) {
        SpajSourceType spajSourceType = SpajSourceTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Source Type id " + id + " is not exist"));
        SpajSourceTypeDto response = modelMapper.map(spajSourceType, SpajSourceTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajSourceTypeDto> addSpajSourceType(CreateSpajSourceType newSpajSourceType) {
        SpajSourceType spajSourceType = modelMapper.map(newSpajSourceType, SpajSourceType.class);
        SpajSourceType SpajSourceTypeSaved = SpajSourceTypeRepository.save(spajSourceType);
        SpajSourceTypeDto response = modelMapper.map(SpajSourceTypeSaved, SpajSourceTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajSourceTypeDto> editSpajSourceType(CreateSpajSourceType updateSpajSourceType, Long id) {
        SpajSourceType spajSourceType = SpajSourceTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Source Type id " + id + " is not exist"));
        spajSourceType.setSpajSourceTypeCode(updateSpajSourceType.getSpajSourceTypeCode());
        spajSourceType.setSpajSourceTypeName(updateSpajSourceType.getSpajSourceTypeName());
        spajSourceType.setActive(updateSpajSourceType.isActive());
        SpajSourceTypeRepository.save(spajSourceType);
        SpajSourceTypeDto response = modelMapper.map(spajSourceType, SpajSourceTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajSourceTypeDto> deleteSpajSourceType(Long id) {
        SpajSourceType spajSourceType = SpajSourceTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Source Type id " + id + " is not exist"));
        SpajSourceTypeRepository.deleteById(id);
        SpajSourceTypeDto response = modelMapper.map(spajSourceType, SpajSourceTypeDto.class);

        return ResponseEntity.ok(response);
    }
}