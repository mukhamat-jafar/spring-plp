package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.IdentityType.CreateIdentityType;
import id.equity.nichemarket.dto.es.IdentityType.IdentityTypeDto;
import id.equity.nichemarket.model.es.IdentityType;
import id.equity.nichemarket.repository.es.IdentityTypeRepository;

@Service
public class IdentityTypeService {
	
	@Autowired
	private IdentityTypeRepository identityTypeRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All IdentityType
	public ResponseEntity<List<IdentityTypeDto>> listIdentityType() {
		List<IdentityType> listIdentityTypes = identityTypeRepository.findAll();
		Type targetType = new TypeToken <List<IdentityTypeDto>>() {}.getType();
		List<IdentityTypeDto> response = modelMapper.map(listIdentityTypes, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List IdentityType By Id
	public ResponseEntity<IdentityTypeDto> getIdentityTypeById(Long id) {
		IdentityType IdentityTypeDto = identityTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Identity Type id " + id + " is not exist"));
		IdentityTypeDto response = modelMapper.map(IdentityTypeDto, IdentityTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create IdentityType
	public ResponseEntity<IdentityTypeDto> addIdentityType(CreateIdentityType newIdentityType) {
		IdentityType IdentityType = modelMapper.map(newIdentityType, IdentityType.class);
		IdentityType IdentityTypeSaved = identityTypeRepository.save(IdentityType);
		IdentityTypeDto response = modelMapper.map(IdentityTypeSaved, IdentityTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit IdentityType
	public ResponseEntity<IdentityTypeDto> editIdentityType(CreateIdentityType updateIdentityType, Long id) {
		IdentityType IdentityType = identityTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Identity Type id " + id + " is not exist"));
		
		//manual maps
		IdentityType.setIdentityTypeCode(updateIdentityType.getIdentityTypeCode());
		IdentityType.setDescription(updateIdentityType.getDescription());
		IdentityType.setActive(updateIdentityType.isActive());
		identityTypeRepository.save(IdentityType);
		IdentityTypeDto response = modelMapper.map(IdentityType, IdentityTypeDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete IdentityType
	public ResponseEntity<IdentityTypeDto> deleteIdentityType(Long id) {
		IdentityType IdentityType = identityTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Identity Type id " + id + " is not exist"));
		
		identityTypeRepository.deleteById(id);
		IdentityTypeDto response = modelMapper.map(IdentityType, IdentityTypeDto.class);

		return ResponseEntity.ok(response);
	}
}