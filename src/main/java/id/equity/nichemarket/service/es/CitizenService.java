package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.Citizen.CitizenDto;
import id.equity.nichemarket.dto.es.Citizen.CreateCitizen;
import id.equity.nichemarket.model.es.Citizen;
import id.equity.nichemarket.repository.es.CitizenRepository;

@Service
public class CitizenService {
	
	@Autowired
	private CitizenRepository citizenRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All Citizen
	public ResponseEntity<List<CitizenDto>> listCitizen() {
		List<Citizen> listCitizens = citizenRepository.findAll();
		Type targetType = new TypeToken <List<CitizenDto>>() {}.getType();
		List<CitizenDto> response = modelMapper.map(listCitizens, targetType);


		return ResponseEntity.ok(response);
	}
	
	//List Citizen By Id
	public ResponseEntity<CitizenDto> getCitizenById(Long id) {
		Citizen citizenDto = citizenRepository.findById(id).orElseThrow(()-> new NotFoundException("Citizen id " + id + " is not exist"));
		CitizenDto response = modelMapper.map(citizenDto, CitizenDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Create Citizen
	public ResponseEntity<CitizenDto> addCitizen(CreateCitizen newCitizen) {
		Citizen citizen = modelMapper.map(newCitizen, Citizen.class);
		Citizen citizenSaved = citizenRepository.save(citizen);
		CitizenDto response = modelMapper.map(citizenSaved, CitizenDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Edit Citizen
	public ResponseEntity<CitizenDto> editCitizen(CreateCitizen updateCitizen, Long id) {
		Citizen citizen = citizenRepository.findById(id).orElseThrow(()-> new NotFoundException("Citizen id " + id + " is not exist"));
		
		//manual maps
		citizen.setCitizenCode(updateCitizen.getCitizenCode());
		citizen.setDescription(updateCitizen.getDescription());
		citizen.setActive(updateCitizen.isActive());
		
		citizenRepository.save(citizen);
		CitizenDto response = modelMapper.map(citizen, CitizenDto.class);


		return ResponseEntity.ok(response);
	}
	
	//Delete Citizen
	public ResponseEntity<CitizenDto> deleteCitizen(Long id) {
		Citizen citizen = citizenRepository.findById(id).orElseThrow(()-> new NotFoundException("Citizen id " + id + " is not exist"));

		citizenRepository.deleteById(id);
		CitizenDto response = modelMapper.map(citizen, CitizenDto.class);


		return ResponseEntity.ok(response);
	}
}