package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.Purpose.CreatePurpose;
import id.equity.nichemarket.dto.es.Purpose.PurposeDto;
import id.equity.nichemarket.model.es.Purpose;
import id.equity.nichemarket.repository.es.PurposeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class PurposeService {
    @Autowired
    private PurposeRepository PurposeRespository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<PurposeDto>> listPurpose() {
        List<Purpose> purpose= PurposeRespository.findAll();
        Type targetType = new TypeToken<List<PurposeDto>>() {}.getType();
        List<PurposeDto> response = modelMapper.map(purpose, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<PurposeDto> getPurposeById(Long id) {
        Purpose purpose = PurposeRespository.findById(id).orElseThrow(()-> new NotFoundException("Purpose id " + id + " is not exist"));
        PurposeDto response = modelMapper.map(purpose, PurposeDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<PurposeDto> addPurpose(CreatePurpose newPurpose) {
        Purpose purpose = modelMapper.map(newPurpose, Purpose.class);
        Purpose PurposeSaved = PurposeRespository.save(purpose);
        PurposeDto response = modelMapper.map(PurposeSaved, PurposeDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<PurposeDto> editPurpose(CreatePurpose updatePurpose, Long id) {
        Purpose purpose = PurposeRespository.findById(id).orElseThrow(()-> new NotFoundException("Purpose id " + id + " is not exist"));
        purpose.setPurposeCode(updatePurpose.getPurposeCode());
        purpose.setDescription(updatePurpose.getDescription());
        purpose.setActive(updatePurpose.isActive());
        PurposeRespository.save(purpose);
        PurposeDto response = modelMapper.map(purpose, PurposeDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<PurposeDto> deletePurpose(Long id) {
        Purpose purpose = PurposeRespository.findById(id).orElseThrow(()-> new NotFoundException("Purpose id " + id + " is not exist"));
        PurposeRespository.deleteById(id);
        PurposeDto response = modelMapper.map(purpose, PurposeDto.class);

        return ResponseEntity.ok(response);
    }
}