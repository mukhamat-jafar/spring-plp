package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajDocumentFile.CreateSpajDocumentFile;
import id.equity.nichemarket.dto.es.SpajDocumentFile.SpajDocumentFileDto;
import id.equity.nichemarket.model.es.SpajDocumentFile;
import id.equity.nichemarket.repository.es.SpajDocumentFileRepository;
import id.equity.nichemarket.service.eSubmissionStore.documentData.BaseDocumentData;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajDocumentFileService {
    @Autowired
    private SpajDocumentFileRepository spajDocumentFileRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<SpajDocumentFileDto>> listSpajDocumentFile() {
        List<SpajDocumentFile> SpajDocumentFile = spajDocumentFileRepository.findAll();
        Type targetType = new TypeToken<List<SpajDocumentFileDto>>() {}.getType();
        List<SpajDocumentFileDto> response = modelMapper.map(SpajDocumentFile, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajDocumentFileDto> getSpajDocumentFileById(Long id) {
        SpajDocumentFile spajDocumentFile = spajDocumentFileRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Document File id " + id + " is not exist"));
        SpajDocumentFileDto response = modelMapper.map(spajDocumentFile, SpajDocumentFileDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajDocumentFileDto> addSpajDocumentFile(CreateSpajDocumentFile newSpajDocumentFile) {
        SpajDocumentFile spajDocumentFile = modelMapper.map(newSpajDocumentFile, SpajDocumentFile.class);
        SpajDocumentFile spajDocumentFileSaved = spajDocumentFileRepository.save(spajDocumentFile);
        SpajDocumentFileDto response = modelMapper.map(spajDocumentFileSaved, SpajDocumentFileDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajDocumentFileDto> editSpajDocumentFile(CreateSpajDocumentFile updateSpajDocumentFile, Long id) {
        SpajDocumentFile spajDocumentFile = spajDocumentFileRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Document File id " + id + " is not exist"));

        spajDocumentFile.setSpajDocumentFileCode(updateSpajDocumentFile.getSpajDocumentFileCode());
        spajDocumentFile.setSpajDocumentCode(updateSpajDocumentFile.getSpajDocumentCode());
        spajDocumentFile.setSpajNo(updateSpajDocumentFile.getSpajNo());
        spajDocumentFile.setDmsCode(updateSpajDocumentFile.getDmsCode());
        spajDocumentFile.setFile(updateSpajDocumentFile.getFile());
        spajDocumentFile.setDocumentName(updateSpajDocumentFile.getDocumentName());
        spajDocumentFile.setActive(updateSpajDocumentFile.isActive());

        spajDocumentFileRepository.save(spajDocumentFile);
        SpajDocumentFileDto response = modelMapper.map(spajDocumentFile, SpajDocumentFileDto.class);

        return ResponseEntity.ok(response);
    }

    //Patch Data By Id
    public void updateCertainDocument(BaseDocumentData data, SpajDocumentFile existData){
        CreateSpajDocumentFile update = new CreateSpajDocumentFile();
        update.setFile(data.getDocument_data().getDocument().getFile());
        update.setDocumentName(data.getDocument_data().getDocument().getDocument_name());

    }

    //Delete Data By Id
    public ResponseEntity<SpajDocumentFileDto> deleteSpajDocumentFile(Long id) {
        SpajDocumentFile spajDocumentFile = spajDocumentFileRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Document File id " + id + " is not exist"));
        spajDocumentFileRepository.deleteById(id);
        SpajDocumentFileDto response = modelMapper.map(spajDocumentFile, SpajDocumentFileDto.class);

        return ResponseEntity.ok(response);
    }
}