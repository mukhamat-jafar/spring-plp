package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.BankPayment.BankPaymentDto;
import id.equity.nichemarket.dto.es.BankPayment.CreateBankPayment;
import id.equity.nichemarket.model.es.BankPayment;
import id.equity.nichemarket.repository.es.BankPaymentRepository;

@Service
public class BankPaymentService {
	
	@Autowired
	private BankPaymentRepository bankPaymentRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List All BankPayment
	public ResponseEntity<List<BankPaymentDto>> listBankPayment() {
		List<BankPayment> listBankPayments = bankPaymentRepository.findAll();
		Type targetType = new TypeToken <List<BankPaymentDto>>() {}.getType();
		List<BankPaymentDto> response = modelMapper.map(listBankPayments, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List BankPayment By Id
	public ResponseEntity<BankPaymentDto> getBankPaymentById(Long id) {
		BankPayment bankPayment = bankPaymentRepository.findById(id).orElseThrow(()-> new NotFoundException("Bank Payment id " + id + " is not exist"));
		BankPaymentDto response = modelMapper.map(bankPayment, BankPaymentDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create BankPayment
	public ResponseEntity<BankPaymentDto> addBankPayment(CreateBankPayment newBankPayment) {
		BankPayment bankPayment = modelMapper.map(newBankPayment, BankPayment.class);
		BankPayment bankPaymentSaved = bankPaymentRepository.save(bankPayment);
		BankPaymentDto response = modelMapper.map(bankPaymentSaved, BankPaymentDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit BankPayment
	public ResponseEntity<BankPaymentDto> editBankPayment(CreateBankPayment updateBankPayment, Long id) {
		BankPayment bankPayment = bankPaymentRepository.findById(id).orElseThrow(()-> new NotFoundException("Bank Payment id " + id + " is not exist"));
		
		//manual map
		bankPayment.setBankPaymentCode(updateBankPayment.getBankPaymentCode());
		bankPayment.setDescription(updateBankPayment.getDescription());
		bankPayment.setActive(updateBankPayment.isActive());
		bankPaymentRepository.save(bankPayment);
		BankPaymentDto response = modelMapper.map(bankPayment, BankPaymentDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete BankPayment
	public ResponseEntity<BankPaymentDto> deleteBankPayment(Long id) {
		BankPayment bankPayment = bankPaymentRepository.findById(id).orElseThrow(()-> new NotFoundException("Bank Payment id " + id + " is not exist"));

		bankPaymentRepository.deleteById(id);
		BankPaymentDto response = modelMapper.map(bankPayment, BankPaymentDto.class);

		return ResponseEntity.ok(response);
	}
}