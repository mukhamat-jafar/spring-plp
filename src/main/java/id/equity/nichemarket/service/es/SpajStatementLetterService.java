package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.SpajStatementLetter.CreateSpajStatementLetter;
import id.equity.nichemarket.dto.es.SpajStatementLetter.SpajStatementLetterDto;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.model.es.SpajStatementLetter;
import id.equity.nichemarket.repository.es.SpajStatementLetterRepository;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajStatementLetterService {
    @Autowired
    private SpajStatementLetterRepository spajStatementLetterRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get All Data
    public ResponseEntity<List<SpajStatementLetterDto>> listSpajStatementLetter() {
        List<SpajStatementLetter> spajStatementLetter= spajStatementLetterRepository.findAll();
        Type targetType = new TypeToken<List<SpajStatementLetterDto>>() {}.getType();
        List<SpajStatementLetterDto> response = modelMapper.map(spajStatementLetter, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajStatementLetterDto> getSpajStatementLetterById(Long id) {
        SpajStatementLetter spajStatementLetter = spajStatementLetterRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Statement Letter id " + id + " is not exist"));
        SpajStatementLetterDto response = modelMapper.map(spajStatementLetter, SpajStatementLetterDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajStatementLetterDto> addSpajStatementLetter(CreateSpajStatementLetter newSpajStatementLetter) {
        SpajStatementLetter spajStatementLetter = modelMapper.map(newSpajStatementLetter, SpajStatementLetter.class);
        SpajStatementLetter SpajStatementLetterSaved = spajStatementLetterRepository.save(spajStatementLetter);
        SpajStatementLetterDto response = modelMapper.map(SpajStatementLetterSaved, SpajStatementLetterDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajStatementLetterDto> editSpajStatementLetter(CreateSpajStatementLetter updateSpajStatementLetter, Long id) {
        SpajStatementLetter spajStatementLetter = spajStatementLetterRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Statement Letter id " + id + " is not exist"));
        spajStatementLetter.setSpajStatementLetterCode(updateSpajStatementLetter.getSpaj_statement_letter_code());
        spajStatementLetter.setSpajDocumentCode(updateSpajStatementLetter.getSpaj_document_code());
        spajStatementLetter.setSpajNo(updateSpajStatementLetter.getSpaj_no());
        spajStatementLetter.setSubmissionPlace(updateSpajStatementLetter.getSubmission_place());
        spajStatementLetter.setSubmissionDate(updateSpajStatementLetter.getSubmission_date());
        spajStatementLetter.setActive(updateSpajStatementLetter.is_active());
        spajStatementLetterRepository.save(spajStatementLetter);
        em.refresh(spajStatementLetter);
        SpajStatementLetterDto response = modelMapper.map(spajStatementLetter, SpajStatementLetterDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajStatementLetterDto> deleteSpajStatementLetter(Long id) {
        SpajStatementLetter spajStatementLetter = spajStatementLetterRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Statement Letter id " + id + " is not exist"));
        spajStatementLetterRepository.deleteById(id);
        SpajStatementLetterDto response = modelMapper.map(spajStatementLetter, SpajStatementLetterDto.class);

        return ResponseEntity.ok(response);
    }

    //insert data in file json
    @Transactional
    public void savingSpajStatement(SpajDocument spajDocument, ESubmissionBaseData eSubmissionBaseData, String spajCode){
        //master Spaj statement Letter
        SpajStatementLetter spajStatementLetter = modelMapper.map(eSubmissionBaseData, SpajStatementLetter.class);
        spajStatementLetter.setSubmissionPlace(eSubmissionBaseData.getData().getSurat_pernyataan().getTempat_pengajuan());
        spajStatementLetter.setSubmissionDate(eSubmissionBaseData.getData().getSurat_pernyataan().getTanggal_pengajuan());
        spajStatementLetter.setActive(true);
        spajStatementLetter.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
        spajStatementLetter.setSpajNo(spajCode);
        spajStatementLetterRepository.save(spajStatementLetter);
    }
}