package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.ResidenceStatus.CreateResidenceStatus;
import id.equity.nichemarket.dto.es.ResidenceStatus.ResidenceStatusDto;
import id.equity.nichemarket.model.es.ResidenceStatus;
import id.equity.nichemarket.repository.es.ResidenceStatusRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ResidenceStatusService {

    @Autowired
    private ResidenceStatusRepository residenceStatusRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<ResidenceStatusDto>> listResidenceStatus() {
        List<ResidenceStatus> residenceStatus= residenceStatusRepository.findAll();
        Type targetType = new TypeToken<List<ResidenceStatusDto>>() {}.getType();
        List<ResidenceStatusDto> response = modelMapper.map(residenceStatus, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<ResidenceStatusDto> getResidenceStatusById(Long id) {
        ResidenceStatus residenceStatus = residenceStatusRepository.findById(id).orElseThrow(()-> new NotFoundException("Residence Status id " + id + " is not exist"));
        ResidenceStatusDto response = modelMapper.map(residenceStatus, ResidenceStatusDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<ResidenceStatusDto> addResidenceStatus(CreateResidenceStatus newResidenceStatus) {
        ResidenceStatus residenceStatus = modelMapper.map(newResidenceStatus, ResidenceStatus.class);
        ResidenceStatus residenceStatusSaved = residenceStatusRepository.save(residenceStatus);
        ResidenceStatusDto response = modelMapper.map(residenceStatusSaved, ResidenceStatusDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<ResidenceStatusDto> editResidenceStatus(CreateResidenceStatus updateResidenceStatus, Long id) {
        ResidenceStatus residenceStatus = residenceStatusRepository.findById(id).orElseThrow(()-> new NotFoundException("Residence Status id " + id + " is not exist"));
        residenceStatus.setResidenceStatusCode(updateResidenceStatus.getResidenceStatusCode());
        residenceStatus.setDescription(updateResidenceStatus.getDescription());
        residenceStatus.setActive(updateResidenceStatus.isActive());
        residenceStatusRepository.save(residenceStatus);
        ResidenceStatusDto response = modelMapper.map(residenceStatus, ResidenceStatusDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<ResidenceStatusDto> deleteResidenceStatus(Long id) {
        ResidenceStatus residenceStatus = residenceStatusRepository.findById(id).orElseThrow(()-> new NotFoundException("Residence Status id " + id + " is not exist"));
        residenceStatusRepository.deleteById(id);
        ResidenceStatusDto response = modelMapper.map(residenceStatus, ResidenceStatusDto.class);

        return ResponseEntity.ok(response);
    }
}