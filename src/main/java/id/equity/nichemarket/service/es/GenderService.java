package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.Gender.CreateGender;
import id.equity.nichemarket.dto.si.Gender.GenderDto;
import id.equity.nichemarket.model.es.Gender;
import id.equity.nichemarket.repository.es.GenderRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class GenderService {

	@Autowired
	private GenderRepository genderRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Gender
	public ResponseEntity<List<GenderDto>> listGender(){
		List<Gender> lsGender= genderRepository.findAll();
		Type targetType = new TypeToken<List<GenderDto>>() {}.getType();
		List<GenderDto> response = modelMapper.map(lsGender, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get Gender By Id
	public ResponseEntity<GenderDto> getGenderById(Long id) {
		Gender gender = genderRepository.findById(id).orElseThrow(()-> new NotFoundException("Gender id " + id + " is not exist"));
		GenderDto response = modelMapper.map(gender, GenderDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Gender
	public ResponseEntity<GenderDto> addGender(CreateGender newGender) {
		try {
			Gender existGender = genderRepository.findByGenderCode(newGender.getGenderCode());
			if (existGender.getGenderCode().equals(newGender.getGenderCode())){
				return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		Gender gender = modelMapper.map(newGender, Gender.class);
		Gender genderSaved = genderRepository.save(gender);
		GenderDto response = modelMapper.map(genderSaved, GenderDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Gender
	public ResponseEntity<GenderDto> editGender(CreateGender updateGender, Long id) {
		Gender gender = genderRepository.findById(id).orElseThrow(()-> new NotFoundException("Gender id " + id + " is not exist"));
		gender.setGenderCode(updateGender.getGenderCode());
		gender.setDescription(updateGender.getDescription());
		gender.setActive(updateGender.isActive());
		genderRepository.save(gender);
		GenderDto response = modelMapper.map(gender, GenderDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Gender	
	public ResponseEntity<GenderDto> deleteGender(Long id) {
		Gender findGender = genderRepository.findById(id).orElseThrow(()-> new NotFoundException("Gender id " + id + " is not exist"));
		genderRepository.deleteById(id);
		GenderDto response = modelMapper.map(findGender, GenderDto.class);

		return ResponseEntity.ok(response);
	}
}