package id.equity.nichemarket.service.es;

import java.lang.reflect.Type;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.InsuredRelation.CreateInsuredRelation;
import id.equity.nichemarket.dto.es.InsuredRelation.InsuredRelationDto;
import id.equity.nichemarket.model.es.InsuredRelation;
import id.equity.nichemarket.repository.es.InsuredRelationRepository;

@Service
public class InsuredRelationService {
	
	@Autowired
	private InsuredRelationRepository insuredRelationRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List InsuredRelation
	public ResponseEntity<List<InsuredRelationDto>> listInsuredRelation() {
		List<InsuredRelation> listInsuredRelations = insuredRelationRepository.findAll();
		Type targetType = new TypeToken <List<InsuredRelationDto>>() {}.getType();
		List<InsuredRelationDto> response = modelMapper.map(listInsuredRelations, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List InsuredRelation By Id
	public ResponseEntity<InsuredRelationDto> getInsuredRelationById(Long id) {
		InsuredRelation insuredRelation = insuredRelationRepository.findById(id).orElseThrow(()-> new NotFoundException("Insured Relation id " + id + " is not exist"));
		InsuredRelationDto response = modelMapper.map(insuredRelation, InsuredRelationDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create InsuredRelation
	public ResponseEntity<InsuredRelationDto> addInsuredRelation(CreateInsuredRelation newInsuredRelation) {
		InsuredRelation insuredRelation = modelMapper.map(newInsuredRelation, InsuredRelation.class);
		InsuredRelation insuredRelationSaved = insuredRelationRepository.save(insuredRelation);
		InsuredRelationDto response = modelMapper.map(insuredRelationSaved, InsuredRelationDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit InsuredRelation
	public ResponseEntity<InsuredRelationDto> editInsuredRelation(CreateInsuredRelation updateInsuredRelation, Long id) {
		InsuredRelation insuredRelation = insuredRelationRepository.findById(id).orElseThrow(()-> new NotFoundException("Insured Relation id " + id + " is not exist"));
		
		//manual map
		insuredRelation.setInsuredRelationCode(updateInsuredRelation.getInsuredRelationCode());
		insuredRelation.setDescription(updateInsuredRelation.getDescription());
		insuredRelation.setActive(updateInsuredRelation.isActive());
		
		insuredRelationRepository.save(insuredRelation);
		InsuredRelationDto response = modelMapper.map(insuredRelation, InsuredRelationDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete InsuredRelation
	public ResponseEntity<InsuredRelationDto> deleteInsuredRelation(Long id) {
		InsuredRelation insuredRelation = insuredRelationRepository.findById(id).orElseThrow(()-> new NotFoundException("Insured Relation id " + id + " is not exist"));
		
		insuredRelationRepository.deleteById(id);
		InsuredRelationDto response =modelMapper.map(insuredRelation, InsuredRelationDto.class);

		return ResponseEntity.ok(response);
	}
}