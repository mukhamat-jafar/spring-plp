package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.es.MaritalStatus.CreateMaritalStatus;
import id.equity.nichemarket.dto.es.MaritalStatus.MaritalStatusDto;
import id.equity.nichemarket.model.es.MaritalStatus;
import id.equity.nichemarket.repository.es.MaritalStatusRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class MaritalStatusService {
    @Autowired
    private MaritalStatusRepository maritalStatusRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<MaritalStatusDto>> listMaritalStatus() {
        List<MaritalStatus> maritalStatus= maritalStatusRepository.findAll();
        Type targetType = new TypeToken<List<MaritalStatusDto>>() {}.getType();
        List<MaritalStatusDto> response = modelMapper.map(maritalStatus, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<MaritalStatusDto> getMaritalStatusById(Long id) {
        MaritalStatus maritalStatus = maritalStatusRepository.findById(id).orElseThrow(()-> new NotFoundException("Marital Status id " + id + " is not exist"));
        MaritalStatusDto response = modelMapper.map(maritalStatus, MaritalStatusDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<MaritalStatusDto> addMaritalStatus(CreateMaritalStatus newMaritalStatus) {
        MaritalStatus maritalStatus = modelMapper.map(newMaritalStatus, MaritalStatus.class);
        MaritalStatus maritalStatusSaved = maritalStatusRepository.save(maritalStatus);
        MaritalStatusDto response = modelMapper.map(maritalStatusSaved, MaritalStatusDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<MaritalStatusDto> editMaritalStatus(CreateMaritalStatus updateMaritalStatus, Long id) {
        MaritalStatus maritalStatus = maritalStatusRepository.findById(id).orElseThrow(()-> new NotFoundException("Marital Status id " + id + " is not exist"));
        maritalStatus.setMaritalStatusCode(updateMaritalStatus.getMaritalStatusCode());
        maritalStatus.setDescription(updateMaritalStatus.getDescription());
        maritalStatus.setActive(updateMaritalStatus.isActive());
        maritalStatusRepository.save(maritalStatus);
        MaritalStatusDto response = modelMapper.map(maritalStatus, MaritalStatusDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By id
    public ResponseEntity<MaritalStatusDto> deleteMaritalStatus(Long id) {
        MaritalStatus maritalStatus = maritalStatusRepository.findById(id).orElseThrow(()-> new NotFoundException("Marital Status id " + id + " is not exist"));
        maritalStatusRepository.deleteById(id);
        MaritalStatusDto response = modelMapper.map(maritalStatus, MaritalStatusDto.class);

        return ResponseEntity.ok(response);
    }
}