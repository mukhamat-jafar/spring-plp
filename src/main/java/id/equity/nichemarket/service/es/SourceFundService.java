package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SourceFund.CreateSourceFund;
import id.equity.nichemarket.dto.es.SourceFund.SourceFundDto;
import id.equity.nichemarket.model.es.SourceFund;
import id.equity.nichemarket.repository.es.SourceFundRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class SourceFundService {
    @Autowired
    private SourceFundRepository sourceFundRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<SourceFundDto>> listSourceFund() {
        List<SourceFund> sourceFund= sourceFundRepository.findAll();
        Type targetType = new TypeToken<List<SourceFundDto>>() {}.getType();
        List<SourceFundDto> response = modelMapper.map(sourceFund, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SourceFundDto> getSourceFundById(Long id) {
        SourceFund sourceFund = sourceFundRepository.findById(id).orElseThrow(()-> new NotFoundException("Source Fund id " + id + " is not exist"));
        SourceFundDto response = modelMapper.map(sourceFund, SourceFundDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SourceFundDto> addSourceFund(CreateSourceFund newSourceFund) {
        SourceFund sourceFund = modelMapper.map(newSourceFund, SourceFund.class);
        SourceFund SourceFundSaved = sourceFundRepository.save(sourceFund);
        SourceFundDto response = modelMapper.map(SourceFundSaved, SourceFundDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SourceFundDto> editSourceFund(CreateSourceFund updateSourceFund, Long id) {
        SourceFund sourceFund = sourceFundRepository.findById(id).orElseThrow(()-> new NotFoundException("Source Fund id " + id + " is not exist"));
        sourceFund.setSourceFundCode(updateSourceFund.getSourceFundCode());
        sourceFund.setDescription(updateSourceFund.getDescription());
        sourceFund.setActive(updateSourceFund.isActive());
        sourceFundRepository.save(sourceFund);
        SourceFundDto response = modelMapper.map(sourceFund, SourceFundDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SourceFundDto> deleteSourceFund(Long id) {
        SourceFund sourceFund = sourceFundRepository.findById(id).orElseThrow(()-> new NotFoundException("Source Fund id " + id + " is not exist"));
        sourceFundRepository.deleteById(id);
        SourceFundDto response = modelMapper.map(sourceFund, SourceFundDto.class);

        return ResponseEntity.ok(response);
    }
}