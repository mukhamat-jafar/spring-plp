package id.equity.nichemarket.service.es;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajAdditionalStat.CreateSpajAdditionalStat;
import id.equity.nichemarket.dto.es.SpajAdditionalStat.SpajAdditionalStatDto;
import id.equity.nichemarket.model.es.SpajAdditionalStat;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.repository.es.SpajAdditionalStatRepository;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionBaseData;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class SpajAdditionalStatService {
    @Autowired
    private SpajAdditionalStatRepository spajAdditionalStatRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EntityManager em;

    //Get All Data
    public ResponseEntity<List<SpajAdditionalStatDto>> listSpajAdditionalStat() {
        List<SpajAdditionalStat> spajAdditionalStat= spajAdditionalStatRepository.findAll();
        Type targetType = new TypeToken<List<SpajAdditionalStatDto>>() {}.getType();
        List<SpajAdditionalStatDto> response = modelMapper.map(spajAdditionalStat, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<SpajAdditionalStatDto> getSpajAdditionalStatById(Long id) {
        SpajAdditionalStat spajAdditionalStat = spajAdditionalStatRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Additional Stat id " + id + " is not exist"));
        SpajAdditionalStatDto response = modelMapper.map(spajAdditionalStat, SpajAdditionalStatDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<SpajAdditionalStatDto> addSpajAdditionalStat(CreateSpajAdditionalStat newSpajAdditionalStat) {
        SpajAdditionalStat spajAdditionalStat = modelMapper.map(newSpajAdditionalStat, SpajAdditionalStat.class);
        SpajAdditionalStat SpajAdditionalStatSaved = spajAdditionalStatRepository.save(spajAdditionalStat);
        SpajAdditionalStatDto response = modelMapper.map(SpajAdditionalStatSaved, SpajAdditionalStatDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<SpajAdditionalStatDto> editSpajAdditionalStat(CreateSpajAdditionalStat updateSpajAdditionalStat, Long id) {
        SpajAdditionalStat spajAdditionalStat = spajAdditionalStatRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Additional Stat id " + id + " is not exist"));
        spajAdditionalStat.setSpajAdditionalStatCode(updateSpajAdditionalStat.getSpajAdditionalStatCode());
        spajAdditionalStat.setSpajDocumentCode(updateSpajAdditionalStat.getSpajDocumentCode());
        spajAdditionalStat.setSpajNo(updateSpajAdditionalStat.getSpajNo());
        spajAdditionalStat.setDomicileStatus(updateSpajAdditionalStat.getDomicileStatus());
        spajAdditionalStat.setOtherDomicile(updateSpajAdditionalStat.getOtherDomicile());
        spajAdditionalStat.setGrossIncome(updateSpajAdditionalStat.getGrossIncome());
        spajAdditionalStat.setSourceOfFund(updateSpajAdditionalStat.getSourceOfFund());
        spajAdditionalStat.setOtherSourceOfFund(updateSpajAdditionalStat.getOtherSourceOfFund());
        spajAdditionalStat.setOtherPurposeBy(updateSpajAdditionalStat.getOtherPurposeBy());
        spajAdditionalStat.setActive(updateSpajAdditionalStat.isActive());
        spajAdditionalStatRepository.save(spajAdditionalStat);
        em.refresh(spajAdditionalStat);
        SpajAdditionalStatDto response = modelMapper.map(spajAdditionalStat, SpajAdditionalStatDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<SpajAdditionalStatDto> deleteSpajAdditionalStat(Long id) {
        SpajAdditionalStat spajAdditionalStat = spajAdditionalStatRepository.findById(id).orElseThrow(()-> new NotFoundException("SPAJ Additional Stat id " + id + " is not exist"));
        spajAdditionalStatRepository.deleteById(id);
        SpajAdditionalStatDto response = modelMapper.map(spajAdditionalStat, SpajAdditionalStatDto.class);

        return ResponseEntity.ok(response);
    }

    //insert data in file json
    @Transactional
    public void savingSpajAdditional(SpajDocument spajDocument, ESubmissionBaseData eSubmissionBaseData, String spajCode){
        //master spaj additional stat
        SpajAdditionalStat spajAdditionalStat = modelMapper.map(eSubmissionBaseData, SpajAdditionalStat.class);
        spajAdditionalStat.setDomicileStatus(eSubmissionBaseData.getData().getData_tambahan_statistik().getStatus_tempat_tinggal().getStatus_tempat_tinggal());
        spajAdditionalStat.setOtherDomicile(eSubmissionBaseData.getData().getData_tambahan_statistik().getStatus_tempat_tinggal().getLainnya());
        spajAdditionalStat.setGrossIncome(eSubmissionBaseData.getData().getData_tambahan_statistik().getPenghasilan_kotor_per_tahun());
        spajAdditionalStat.setSourceOfFund(eSubmissionBaseData.getData().getData_tambahan_statistik().getSumber_dana().getSumber_dana());
        spajAdditionalStat.setOtherSourceOfFund(eSubmissionBaseData.getData().getData_tambahan_statistik().getSumber_dana().getLainnya());
        spajAdditionalStat.setPurposeBuy(eSubmissionBaseData.getData().getData_tambahan_statistik().getTujuan_membeli_asuransi().getTujuan_membeli_asuransi());
        spajAdditionalStat.setOtherPurposeBy(eSubmissionBaseData.getData().getData_tambahan_statistik().getTujuan_membeli_asuransi().getLainnya());

        spajAdditionalStat.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
        spajAdditionalStat.setSpajNo(spajCode);
        spajAdditionalStat.setActive(true);
        spajAdditionalStatRepository.save(spajAdditionalStat);
    }
}