package id.equity.nichemarket.service;

import id.equity.nichemarket.exception.ErrorsException;
import id.equity.nichemarket.property.FileStorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileStorageService {
    private final Path fileStorageLocation;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();

        if (!Files.exists(this.fileStorageLocation)) {
            try {
                Files.createDirectory(this.fileStorageLocation);
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public String storeFile(MultipartFile file) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            if(fileName.contains("..")){
                throw new ErrorsException("Sorry, filename contains invalid path sequence " + fileName);
            }

            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException ex) {
            throw new ErrorsException("Could not store file " + fileName + " Please try again!", ex);
        }
    }

    public Resource loadFileAsResource(String fileName) {
        try{
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize() ;
            Resource resource = new UrlResource(filePath.toUri());

            if (!resource.exists()){
                throw new ErrorsException("File Not Found " + fileName);
            }

            return resource;
        } catch (MalformedURLException ex){
            throw new ErrorsException("File Not Found " + fileName, ex);
        }
    }

}
