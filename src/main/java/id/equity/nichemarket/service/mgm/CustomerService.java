package id.equity.nichemarket.service.mgm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.mgm.Customer.CreateCustomer;
import id.equity.nichemarket.dto.mgm.Customer.CustomerDto;
import id.equity.nichemarket.dto.mgm.dataFromLandingPage.DataFromLandingPage;
import id.equity.nichemarket.model.mgm.Customer;
import id.equity.nichemarket.repository.mgm.CustomerRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private EntityManager em;
	
	//List Customer
	public ResponseEntity<List<CustomerDto>> listCustomer(){
		List<Customer> lsCustomer= customerRepository.findAll();
		Type targetType = new TypeToken<List<CustomerDto>>() {}.getType();
		List<CustomerDto> response = modelMapper.map(lsCustomer, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Customer By Id
	public ResponseEntity<CustomerDto> getCustomerById(Long id) {
		Customer customer = customerRepository.findById(id).orElseThrow(()-> new NotFoundException("Customer id " + id + " is not exist"));
		CustomerDto response = modelMapper.map(customer, CustomerDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Customer
	@Transactional
	public ResponseEntity<CustomerDto> addCustomer(CreateCustomer newCustomer) {
		Customer customer = modelMapper.map(newCustomer, Customer.class);
		Customer customerSaved = customerRepository.save(customer);
		em.refresh(customerSaved);
		CustomerDto response = modelMapper.map(customerSaved, CustomerDto.class);
		return ResponseEntity.ok(response);
	}

	//Put Customer
	public ResponseEntity<CustomerDto> editCustomer(CreateCustomer updateCustomer, Long id) {
		Customer customer = customerRepository.findById(id).orElseThrow(()-> new NotFoundException("Customer id " + id + " is not exist"));
		customer.setCustomerCode(updateCustomer.getCustomerCode());
		customer.setCustomerName(updateCustomer.getCustomerName());
		customer.setDateOfBirth(updateCustomer.getDateOfBirth());
		customer.setOccupation(updateCustomer.getOccupation());
		customer.setPhoneNo(updateCustomer.getPhoneNo());
		customer.setEmailAddress(updateCustomer.getEmailAddress());
		customer.setActive(updateCustomer.isActive());
		customerRepository.save(customer);
		CustomerDto response = modelMapper.map(customer, CustomerDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Customer	
	public ResponseEntity<CustomerDto> deleteCustomer(Long id) {
		Customer findCustomer = customerRepository.findById(id).orElseThrow(()-> new NotFoundException("Customer id " + id + " is not exist"));
		customerRepository.deleteById(id);
		CustomerDto response = modelMapper.map(findCustomer, CustomerDto.class);

		return ResponseEntity.ok(response);
	}

	//saving data from Landing Page
	@Transactional
	public CustomerDto savingCustomerData(DataFromLandingPage data){
		Customer customer = modelMapper.map(data, Customer.class);
		customer.setCustomerName(data.getCustomerName());
		customer.setGenderCode(data.getGenderCode());
		customer.setDateOfBirth(data.getDateOfBirth());
		customer.setOccupation(data.getOccupation());
		customer.setPhoneNo(data.getPhoneNo());
		customer.setEmailAddress(data.getEmailAddress());
		customer.setActive(true);
		Customer customerSaved = customerRepository.save(customer);
		em.refresh(customerSaved);
		CustomerDto response = modelMapper.map(customer, CustomerDto.class);
		return response;
	}
}