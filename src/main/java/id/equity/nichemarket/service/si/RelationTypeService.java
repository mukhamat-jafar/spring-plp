package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.RelationType.CreateRelationType;
import id.equity.nichemarket.dto.si.RelationType.RelationTypeDto;
import id.equity.nichemarket.model.si.RelationType;
import id.equity.nichemarket.repository.si.RelationTypeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class RelationTypeService {

    @Autowired
    private RelationTypeRepository relationTypeRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<RelationTypeDto>> listRelationType() {
        List<RelationType> relationType= relationTypeRepository.findAll();
        Type targetType = new TypeToken<List<RelationTypeDto>>() {}.getType();
        List<RelationTypeDto> response = modelMapper.map(relationType, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<RelationTypeDto> getRelationTypeById(Long id) {
        RelationType relationType = relationTypeRepository.findById(id).orElseThrow(()-> new NotFoundException("Relation Type id " + id + " is not exist"));
        RelationTypeDto response = modelMapper.map(relationType, RelationTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<RelationTypeDto> addRelationType(CreateRelationType newRelationType) {
        RelationType relationType = modelMapper.map(newRelationType, RelationType.class);
        RelationType relationTypeSaved = relationTypeRepository.save(relationType);
        RelationTypeDto response = modelMapper.map(relationTypeSaved, RelationTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<RelationTypeDto> editRelationType(CreateRelationType updateRelationType, Long id) {
        RelationType relationType = relationTypeRepository.findById(id).orElseThrow(() -> new NotFoundException("Relation Type id " + id + " is not exist"));

        relationType.setRelationTypeCode(updateRelationType.getRelationTypeCode());
        relationType.setDescription(updateRelationType.getDescription());
        relationType.setActive(updateRelationType.isActive());
        relationTypeRepository.save(relationType);
        RelationTypeDto response = modelMapper.map(relationType, RelationTypeDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete By Id
    public ResponseEntity<RelationTypeDto> deleteRelationType(Long id) {
        RelationType relationType = relationTypeRepository.findById(id).orElseThrow(() -> new NotFoundException("Relation Type id " + id + " is not exist"));
        relationTypeRepository.deleteById(id);
        RelationTypeDto response = modelMapper.map(relationType, RelationTypeDto.class);

        return ResponseEntity.ok(response);
    }
}