package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRate.BaseRateDto;
import id.equity.nichemarket.dto.si.BaseRate.CreateBaseRate;
import id.equity.nichemarket.model.si.BaseRate;
import id.equity.nichemarket.repository.si.BaseRateRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;


@Service
public class BaseRateService {
	
	@Autowired
	private BaseRateRepository baseRateRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List BaseRate
	public ResponseEntity<List<BaseRateDto>>listBaseRate() {
		List<BaseRate> listBaseRates = baseRateRepository.findAll();
		Type targetType = new TypeToken <List<BaseRateDto>>() {}.getType();
		List<BaseRateDto> response = modelMapper.map(listBaseRates, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List BaseRate ById
	public ResponseEntity<BaseRateDto> getBaseRateById(Long id) {
		BaseRate baseRate = baseRateRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rate id " + id + " is not exist"));
		BaseRateDto response = modelMapper.map(baseRate, BaseRateDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create BaseRate
	public ResponseEntity<BaseRateDto> addBaseRate(CreateBaseRate newBaseRate) {
		BaseRate baseRate = modelMapper.map(newBaseRate, BaseRate.class);
		BaseRate BaseRateSaved = baseRateRepository.save(baseRate);
		BaseRateDto response = modelMapper.map(BaseRateSaved, BaseRateDto.class);

		return ResponseEntity.ok(response);
	}

	//Edit BaseRate
	public ResponseEntity<BaseRateDto> editBaserate(CreateBaseRate updateBaseRate, Long id) {
		BaseRate baseRate  = baseRateRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rate id " + id + " is not Exist"));
		
		//manual map
		baseRate.setBaseRateCode(updateBaseRate.getBaseRateCode());
		baseRate.setCategory(updateBaseRate.getCategory());
		baseRate.setProductCode(updateBaseRate.getProductCode());
		baseRate.setValutaCode(updateBaseRate.getValutaCode());
		baseRate.setPaymentPeriodCode(updateBaseRate.getPaymentPeriodCode());
		baseRate.setAsCode(updateBaseRate.getAsCode());
		baseRate.setAge(updateBaseRate.getAge());
		baseRate.setContractPeriod(updateBaseRate.getContractPeriod());
		baseRate.setPremiumPeriod(updateBaseRate.getPremiumPeriod());
		baseRate.setYearTh(updateBaseRate.getYearTh());
		baseRate.setAgeTh(updateBaseRate.getAgeTh());
		baseRate.setRate(updateBaseRate.getRate());
		baseRate.setDivider(updateBaseRate.getDivider());
		baseRate.setActive(updateBaseRate.isActive());

		baseRateRepository.save(baseRate);
		BaseRateDto response = modelMapper.map(baseRate, BaseRateDto.class);

		return ResponseEntity.ok(response);
	}
	//Delete BaseRate
	public ResponseEntity<BaseRateDto> deleteBaserate(Long id) {
		BaseRate baseRate = baseRateRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rate id " + id + " is not exist"));
		baseRateRepository.deleteById(id);
		BaseRateDto response = modelMapper.map(baseRate, BaseRateDto.class);

		return ResponseEntity.ok(response);
	}
}