package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRuleRelation.BaseRuleRelationDto;
import id.equity.nichemarket.dto.si.BaseRuleRelation.CreateBaseRuleRelation;
import id.equity.nichemarket.model.si.BaseRuleRelation;
import id.equity.nichemarket.repository.si.BaseRuleRelationRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRuleRelationService {
    @Autowired
    private BaseRuleRelationRepository baseRuleRelationRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<BaseRuleRelationDto>> listBaseRuleRelation() {
        List<BaseRuleRelation> bRRelation= baseRuleRelationRepository.findAll();
        Type targetType = new TypeToken<List<BaseRuleRelationDto>>() {}.getType();
        List<BaseRuleRelationDto> response = modelMapper.map(bRRelation, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<BaseRuleRelationDto> getBaseRuleRelationById(Long id) {
        BaseRuleRelation bRRelation = baseRuleRelationRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Relation id " + id + " is not exist"));
        BaseRuleRelationDto response = modelMapper.map(bRRelation, BaseRuleRelationDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<BaseRuleRelationDto> addBaseRuleRelation(CreateBaseRuleRelation newBRRelation) {
        BaseRuleRelation bRRelation = modelMapper.map(newBRRelation, BaseRuleRelation.class);
        BaseRuleRelation bRRelationSaved = baseRuleRelationRepository.save(bRRelation);
        BaseRuleRelationDto response = modelMapper.map(bRRelationSaved, BaseRuleRelationDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<BaseRuleRelationDto> editBaseRuleRelation(CreateBaseRuleRelation updateBRRelation, Long id) {
        BaseRuleRelation bRRelation = baseRuleRelationRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Relation id " + id + " is not exist"));
        bRRelation.setBaseRuleRelationCode(updateBRRelation.getBaseRuleRelationCode());
        bRRelation.setProductCode(updateBRRelation.getProductCode());
        bRRelation.setAsCode(updateBRRelation.getAsCode());
        bRRelation.setRelationTypeCode(updateBRRelation.getRelationTypeCode());
        bRRelation.setActive(updateBRRelation.isActive());
        baseRuleRelationRepository.save(bRRelation);
        BaseRuleRelationDto response = modelMapper.map(bRRelation, BaseRuleRelationDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<BaseRuleRelationDto> deleteBaseRuleRelation(Long id) {
        BaseRuleRelation bRRelation = baseRuleRelationRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Relation id " + id + " is not exist"));
        baseRuleRelationRepository.deleteById(id);
        BaseRuleRelationDto response = modelMapper.map(bRRelation, BaseRuleRelationDto.class);

        return ResponseEntity.ok(response);
    }
}