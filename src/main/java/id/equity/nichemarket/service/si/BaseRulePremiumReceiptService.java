package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRulePremiumReceipt.BaseRulePremiumReceiptDto;
import id.equity.nichemarket.dto.si.BaseRulePremiumReceipt.CreateBaseRulePremiumReceipt;
import id.equity.nichemarket.model.si.BaseRulePremiumReceipt;
import id.equity.nichemarket.repository.si.BaseRulePremiumReceiptRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRulePremiumReceiptService {
	
	@Autowired
	private BaseRulePremiumReceiptRepository baseRulePremiumReceiptRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List BaseRulePremiumReceipt
	public ResponseEntity<List<BaseRulePremiumReceiptDto>> listBaseRulePremiumReceipt() {
		List<BaseRulePremiumReceipt> listBaseRulePremiumReceipts = baseRulePremiumReceiptRepository.findAll();
		Type targetType = new TypeToken<List<BaseRulePremiumReceiptDto>>() {}.getType();
		List<BaseRulePremiumReceiptDto> response = modelMapper.map(listBaseRulePremiumReceipts, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get BaseRulePremiumReceipt By Id
	public ResponseEntity<BaseRulePremiumReceiptDto> getBaseRulePremiumReceiptById(Long id) {
		BaseRulePremiumReceipt baseRulePremiumReceipt = baseRulePremiumReceiptRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Receipt id " + id + " is not exist"));
		BaseRulePremiumReceiptDto response = modelMapper.map(baseRulePremiumReceipt, BaseRulePremiumReceiptDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create BaseRulePremiumReceipt
	public ResponseEntity<BaseRulePremiumReceiptDto> addBaseRulePremiumReceipt(CreateBaseRulePremiumReceipt newPremiumReceipt) {
		BaseRulePremiumReceipt baseRulePremiumReceipt = modelMapper.map(newPremiumReceipt, BaseRulePremiumReceipt.class);
		BaseRulePremiumReceipt baseRulePremiumReceiptSaved = baseRulePremiumReceiptRepository.save(baseRulePremiumReceipt);
		BaseRulePremiumReceiptDto response = modelMapper.map(baseRulePremiumReceiptSaved, BaseRulePremiumReceiptDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit BaseRulePremiumReceipt
	public ResponseEntity<BaseRulePremiumReceiptDto> editBaseRulePremiumReceipt(CreateBaseRulePremiumReceipt updatePremiumReceipt,
			Long id) {
		BaseRulePremiumReceipt baseRulePremiumReceipt = baseRulePremiumReceiptRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Receipt id " + id + " is not exist"));
		
		//manual map
		baseRulePremiumReceipt.setBaseRulePremiumReceiptCode(updatePremiumReceipt.getBaseRulePremiumReceiptCode());
		baseRulePremiumReceipt.setProductCode(updatePremiumReceipt.getProductCode());
		baseRulePremiumReceipt.setValutaCode(updatePremiumReceipt.getValutaCode());
		baseRulePremiumReceipt.setPaymentPeriodCode(updatePremiumReceipt.getPaymentPeriodCode());
		baseRulePremiumReceipt.setRate(updatePremiumReceipt.getRate());
		baseRulePremiumReceipt.setActive(updatePremiumReceipt.isActive());
		
		baseRulePremiumReceiptRepository.save(baseRulePremiumReceipt);
		BaseRulePremiumReceiptDto response = modelMapper.map(baseRulePremiumReceipt, BaseRulePremiumReceiptDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete BaseRulePremiumReceipt
	public ResponseEntity<BaseRulePremiumReceiptDto> deleteBaseRulePremiumReceipt(Long id) {
		BaseRulePremiumReceipt baseRulePremiumReceipt = baseRulePremiumReceiptRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Receipt id " + id + " is not exist"));
		baseRulePremiumReceiptRepository.deleteById(id);
		BaseRulePremiumReceiptDto response = modelMapper.map(baseRulePremiumReceipt, BaseRulePremiumReceiptDto.class);

		return ResponseEntity.ok(response);
	}
}