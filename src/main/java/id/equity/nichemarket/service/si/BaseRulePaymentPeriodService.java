package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRulePaymentPeriod.BaseRulePaymentPeriodDto;
import id.equity.nichemarket.dto.si.BaseRulePaymentPeriod.CreateBaseRulePaymentPeriod;
import id.equity.nichemarket.model.si.BaseRulePaymentPeriod;
import id.equity.nichemarket.repository.si.BaseRulePaymentPeriodRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRulePaymentPeriodService {
    @Autowired
    private BaseRulePaymentPeriodRepository baseRulePaymentPeriodRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<BaseRulePaymentPeriodDto>> listBaseRulePaymentPeriod() {
        List<BaseRulePaymentPeriod> lsBaseRulePaymentPeriod= baseRulePaymentPeriodRepository.findAll();
        Type targetType = new TypeToken<List<BaseRulePaymentPeriodDto>>() {}.getType();
        List<BaseRulePaymentPeriodDto> response = modelMapper.map(lsBaseRulePaymentPeriod, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<BaseRulePaymentPeriodDto> getBaseRulePaymentPeriodById(Long id) {
        BaseRulePaymentPeriod baseRulePaymentPeriod = baseRulePaymentPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Period id " + id + " is not exist"));
        BaseRulePaymentPeriodDto response = modelMapper.map(baseRulePaymentPeriod, BaseRulePaymentPeriodDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<BaseRulePaymentPeriodDto> addBaseRulePaymentPeriod(CreateBaseRulePaymentPeriod newBaseRulePaymentPeriod) {
        BaseRulePaymentPeriod baseRulePaymentPeriod = modelMapper.map(newBaseRulePaymentPeriod, BaseRulePaymentPeriod.class);
        BaseRulePaymentPeriod baseRulePaymentPeriodSaved = baseRulePaymentPeriodRepository.save(baseRulePaymentPeriod);
        BaseRulePaymentPeriodDto response = modelMapper.map(baseRulePaymentPeriodSaved, BaseRulePaymentPeriodDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<BaseRulePaymentPeriodDto> editBaseRulePaymentPeriod(CreateBaseRulePaymentPeriod updateBaseRulePaymentPeriod, Long id) {
        BaseRulePaymentPeriod baseRulePaymentPeriod = baseRulePaymentPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Period id " + id + " is not exist"));
        baseRulePaymentPeriod.setBaseRulePaymentPeriodCode(updateBaseRulePaymentPeriod.getBaseRulePaymentPeriodCode());
        baseRulePaymentPeriod.setProductCode(updateBaseRulePaymentPeriod.getProductCode());
        baseRulePaymentPeriod.setValutaCode(updateBaseRulePaymentPeriod.getValutaCode());
        baseRulePaymentPeriod.setPaymentPeriodCode(updateBaseRulePaymentPeriod.getPaymentPeriodCode());
        baseRulePaymentPeriod.setFactor(updateBaseRulePaymentPeriod.getFactor());
        baseRulePaymentPeriod.setActive(updateBaseRulePaymentPeriod.isActive());
        baseRulePaymentPeriodRepository.save(baseRulePaymentPeriod);
        BaseRulePaymentPeriodDto response = modelMapper.map(baseRulePaymentPeriod, BaseRulePaymentPeriodDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<BaseRulePaymentPeriodDto> deleteBaseRulePaymentPeriod(Long id) {
        BaseRulePaymentPeriod baseRulePaymentPeriod = baseRulePaymentPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Period id " + id + " is not exist"));
        baseRulePaymentPeriodRepository.deleteById(id);
        BaseRulePaymentPeriodDto response = modelMapper.map(baseRulePaymentPeriod, BaseRulePaymentPeriodDto.class);

        return ResponseEntity.ok(response);
    }
}
