package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRuleRequired.CreateProductRuleRequired;
import id.equity.nichemarket.dto.si.ProductRuleRequired.ProductRuleRequiredDto;
import id.equity.nichemarket.model.si.ProductRuleRequired;
import id.equity.nichemarket.repository.si.ProductRuleRequiredRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleRequiredService {
	
	@Autowired
	private ProductRuleRequiredRepository productRuleRequiredRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleRequired
	public ResponseEntity<List<ProductRuleRequiredDto>> listProductRuleRequired() {
		List<ProductRuleRequired> listProductRuleRequireds = productRuleRequiredRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleRequiredDto>>() {}.getType();
		List<ProductRuleRequiredDto> response = modelMapper.map(listProductRuleRequireds, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleRequired By Id
	public ResponseEntity<ProductRuleRequiredDto> getProductRuleRequiredById(Long id) {
		ProductRuleRequired productRuleRequired = productRuleRequiredRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Required id " + id + " is not exist"));
		ProductRuleRequiredDto response = modelMapper.map(productRuleRequired, ProductRuleRequiredDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleRequired
	public ResponseEntity<ProductRuleRequiredDto> addProductRuleRequired(CreateProductRuleRequired newProductRuleRequired) {
		ProductRuleRequired productRuleRequired = modelMapper.map(newProductRuleRequired, ProductRuleRequired.class);
		ProductRuleRequired productRuleRequiredSaved = productRuleRequiredRepository.save(productRuleRequired);
		ProductRuleRequiredDto response = modelMapper.map(productRuleRequiredSaved, ProductRuleRequiredDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleRequired
	public ResponseEntity<ProductRuleRequiredDto> editProductRuleRequired(CreateProductRuleRequired updateProductRuleRequired, Long id) {
		ProductRuleRequired productRuleRequired = productRuleRequiredRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Required id " + id + " is not exist"));
		
		//manual map
		productRuleRequired.setProductRuleRequiredCode(updateProductRuleRequired.getProductRuleRequiredCode());
		productRuleRequired.setProductCode(updateProductRuleRequired.getProductCode());
		productRuleRequired.setAsCode(updateProductRuleRequired.getAsCode());
		productRuleRequired.setRelationTypeCode(updateProductRuleRequired.getRelationTypeCode());
		productRuleRequired.setActive(updateProductRuleRequired.isActive());
		
		productRuleRequiredRepository.save(productRuleRequired);
		ProductRuleRequiredDto response = modelMapper.map(productRuleRequired, ProductRuleRequiredDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleRequired
	public ResponseEntity<ProductRuleRequiredDto> deleteProductRuleRequired(Long id) {
		ProductRuleRequired productRuleRequired = productRuleRequiredRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Required id " + id + " is not exist"));
		productRuleRequiredRepository.deleteById(id);
		ProductRuleRequiredDto response = modelMapper.map(productRuleRequired, ProductRuleRequiredDto.class);

		return ResponseEntity.ok(response);
	}	
}