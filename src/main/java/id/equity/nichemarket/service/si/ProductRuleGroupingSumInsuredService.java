package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.ProductRuleGroupingSumInsured.CreateProductRuleGroupingSumInsured;
import id.equity.nichemarket.dto.si.ProductRuleGroupingSumInsured.ProductRuleGroupingSumInsuredDto;
import id.equity.nichemarket.model.si.ProductRuleGroupingSumInsured;
import id.equity.nichemarket.repository.si.ProductRuleGroupingSumInsuredRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleGroupingSumInsuredService {
	
	@Autowired
	private ProductRuleGroupingSumInsuredRepository productRuleGroupingSumInsuredRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleGroupingSumInsured
	public ResponseEntity<List<ProductRuleGroupingSumInsuredDto>> listProductRuleGroupingSumInsured() {
		List<ProductRuleGroupingSumInsured> listproductRuleGroupingSums = productRuleGroupingSumInsuredRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleGroupingSumInsuredDto>>() {}.getType();
		List<ProductRuleGroupingSumInsuredDto> response = modelMapper.map(listproductRuleGroupingSums, targetType);

		return ResponseEntity.ok(response);
	}
	
	//List ProductRuleGroupingSumInsured By id
	public ResponseEntity<ProductRuleGroupingSumInsuredDto> getProductRuleGroupingSumInsuredById(Long id) {
		ProductRuleGroupingSumInsured SumInsured = productRuleGroupingSumInsuredRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Grouping Sum Insured id " + id + " is not exist"));
		ProductRuleGroupingSumInsuredDto response = modelMapper.map(SumInsured, ProductRuleGroupingSumInsuredDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleGroupingSumInsured
	public ResponseEntity<ProductRuleGroupingSumInsuredDto> addProductRuleGroupingSumInsured(CreateProductRuleGroupingSumInsured newSumInsured) {
		ProductRuleGroupingSumInsured sumInsured = modelMapper.map(newSumInsured, ProductRuleGroupingSumInsured.class);
		ProductRuleGroupingSumInsured sumInsuredSaved = productRuleGroupingSumInsuredRepository.save(sumInsured);
		ProductRuleGroupingSumInsuredDto response = modelMapper.map(sumInsuredSaved, ProductRuleGroupingSumInsuredDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleGroupingSumInsured
	public ResponseEntity<ProductRuleGroupingSumInsuredDto> editProductRuleGroupingSumInsured(CreateProductRuleGroupingSumInsured updateSumInsured,
			Long id) {
		ProductRuleGroupingSumInsured sumInsured = productRuleGroupingSumInsuredRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Grouping Sum Insured id " + id + " is not exist"));
		
		//manual map
		sumInsured.setProductRuleGroupingSumInsuredCode(updateSumInsured.getProductRuleGroupingSumInsuredCode());
		sumInsured.setProductCode(updateSumInsured.getProductCode());
		sumInsured.setGroupingCode(updateSumInsured.getGroupingCode());
		sumInsured.setMaxSumInsured(updateSumInsured.getMaxSumInsured());
		sumInsured.setActive(updateSumInsured.isActive());
		
		productRuleGroupingSumInsuredRepository.save(sumInsured);
		ProductRuleGroupingSumInsuredDto response = modelMapper.map(sumInsured, ProductRuleGroupingSumInsuredDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleGroupingSumInsured
	public ResponseEntity<ProductRuleGroupingSumInsuredDto> deleteProductRuleGroupingSumInsured(Long id) {
		ProductRuleGroupingSumInsured sumInsured = productRuleGroupingSumInsuredRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Grouping Sum Insured id " + id + " is not exist"));
		productRuleGroupingSumInsuredRepository.deleteById(id);
		ProductRuleGroupingSumInsuredDto response = modelMapper.map(sumInsured, ProductRuleGroupingSumInsuredDto.class);

		return ResponseEntity.ok(response);
	}	
}