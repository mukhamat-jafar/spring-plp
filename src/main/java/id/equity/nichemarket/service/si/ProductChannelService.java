package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.ProductChannel.CreateProductChannel;
import id.equity.nichemarket.dto.si.ProductChannel.ProductChannelDto;
import id.equity.nichemarket.model.si.ProductChannel;
import id.equity.nichemarket.repository.si.ProductChannelRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductChannelService {

    @Autowired
    private ProductChannelRepository productChannelRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<ProductChannelDto>> listProductChannel() {
        List<ProductChannel> lsProdChannel= productChannelRepository.findAll();
        Type targetType = new TypeToken<List<ProductChannelDto>>() {}.getType();
        List<ProductChannelDto> response = modelMapper.map(lsProdChannel, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<ProductChannelDto> getProductChanneById(Long id) {
        ProductChannel productChannel = productChannelRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Channel id " + id + " is not exist"));
        ProductChannelDto response = modelMapper.map(productChannel, ProductChannelDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<ProductChannelDto> addProductChannel(CreateProductChannel newProdChannel) {
        ProductChannel productChannel = modelMapper.map(newProdChannel, ProductChannel.class);
        ProductChannel productChannelSaved = productChannelRepository.save(productChannel);
        ProductChannelDto response = modelMapper.map(productChannelSaved, ProductChannelDto.class);

        return ResponseEntity.ok(response);
    }

    //Edit Data
    public ResponseEntity<ProductChannelDto> editProductChannel(CreateProductChannel updateProdChannel, Long id) {
        ProductChannel productChannel = productChannelRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Channel id " + id + " is not exist"));
        productChannel.setProductChannelCode(updateProdChannel.getProductChannelCode());
        productChannel.setChannelCode(updateProdChannel.getChannelCode());
        productChannel.setProductCode(updateProdChannel.getProductCode());
        productChannel.setActive(updateProdChannel.isActive());
        productChannelRepository.save(productChannel);
        ProductChannelDto response = modelMapper.map(productChannel, ProductChannelDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data
    public ResponseEntity<ProductChannelDto> deleteProductChannel(Long id) {
        ProductChannel productChannel = productChannelRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Channel id " + id + " is not exist"));
        productChannelRepository.deleteById(id);
        ProductChannelDto response = modelMapper.map(productChannel, ProductChannelDto.class);

        return ResponseEntity.ok(response);
    }
}