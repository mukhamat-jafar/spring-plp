package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.PaymentPeriod.CreatePaymentPeriod;
import id.equity.nichemarket.dto.si.PaymentPeriod.PaymentPeriodDto;
import id.equity.nichemarket.model.si.PaymentPeriod;
import id.equity.nichemarket.repository.si.PaymentPeriodRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class PaymentPeriodService {

	@Autowired
	private PaymentPeriodRepository paymentPeriodRepository;
	
	@Autowired
	private ModelMapper modelMapper;

	//Get All Payment Period
	public ResponseEntity<List<PaymentPeriodDto>> listPaymentPeriod() {
		List<PaymentPeriod> lsPaymentPeriod = paymentPeriodRepository.findAll();
		Type targetType = new TypeToken<List<PaymentPeriodDto>>() {}.getType();
		List<PaymentPeriodDto> response = modelMapper.map(lsPaymentPeriod, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Payment Period By Id
	public ResponseEntity<PaymentPeriodDto> getPaymentPeriodById(Long id) {
		PaymentPeriod paymentPeriod = paymentPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment Period id " + id +" is not exist"));
		PaymentPeriodDto response = modelMapper.map(paymentPeriod, PaymentPeriodDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Payment Period
	public ResponseEntity<PaymentPeriodDto> addPaymentPeriod(CreatePaymentPeriod newPaymentPeriod) {
		PaymentPeriod paymentPeriod = modelMapper.map(newPaymentPeriod, PaymentPeriod.class);
		PaymentPeriod paymentPeriodSaved = paymentPeriodRepository.save(paymentPeriod);
		PaymentPeriodDto response = modelMapper.map(paymentPeriodSaved, PaymentPeriodDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Payment Period
	public ResponseEntity<PaymentPeriodDto> editPaymentPeriod(CreatePaymentPeriod updatePaymentPeriod, Long id) {
		PaymentPeriod paymentPeriod = paymentPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment Period id " + id + " is not exist"));
		paymentPeriod.setPaymentPeriodCode(updatePaymentPeriod.getPaymentPeriodCode());
		paymentPeriod.setDescription(updatePaymentPeriod.getDescription());
		paymentPeriod.setTotYear(updatePaymentPeriod.getTotYear());
		paymentPeriod.setTotMonth(updatePaymentPeriod.getTotMonth());
		paymentPeriod.setActive(updatePaymentPeriod.isActive());
		paymentPeriodRepository.save(paymentPeriod);
		PaymentPeriodDto response = modelMapper.map(paymentPeriod, PaymentPeriodDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Data By Id
	public ResponseEntity<PaymentPeriodDto> deletePaymentPeriod(Long id) {
		PaymentPeriod findPaymentPeriod = paymentPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment Period id " + id + " is not exist"));
		paymentPeriodRepository.deleteById(id);
		PaymentPeriodDto response = modelMapper.map(findPaymentPeriod, PaymentPeriodDto.class);

		return ResponseEntity.ok(response);
	}	
}