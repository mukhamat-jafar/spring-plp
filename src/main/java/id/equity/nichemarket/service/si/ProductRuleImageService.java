package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.ProductRuleImage.CreateProductRuleImage;
import id.equity.nichemarket.dto.si.ProductRuleImage.ProductRuleImageDto;
import id.equity.nichemarket.model.si.ProductRuleImage;
import id.equity.nichemarket.repository.si.ProductRuleImageRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ProductRuleImageService {
	
	@Autowired
	private ProductRuleImageRepository productRuleImageRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List ProductRuleImage
	public ResponseEntity<List<ProductRuleImageDto>> listProductRuleImage() {
		List<ProductRuleImage> listProductRuleImages = productRuleImageRepository.findAll();
		Type targetType = new TypeToken<List<ProductRuleImageDto>>() {}.getType();
		List<ProductRuleImageDto> response = modelMapper.map(listProductRuleImages, targetType);

		return ResponseEntity.ok(response);
	}
	
	//Get ProductRuleImage By Id
	public ResponseEntity<ProductRuleImageDto> getProductRuleImageById(Long id) {
		ProductRuleImage productRuleImage = productRuleImageRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Image id " + id + " is not exist"));
		ProductRuleImageDto response = modelMapper.map(productRuleImage, ProductRuleImageDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Create ProductRuleImage
	public ResponseEntity<ProductRuleImageDto> addProductRuleImage(CreateProductRuleImage newProductRuleImage) {
		ProductRuleImage productRuleImage = modelMapper.map(newProductRuleImage, ProductRuleImage.class);
		ProductRuleImage ProductRuleImageSaved = productRuleImageRepository.save(productRuleImage);
		ProductRuleImageDto response = modelMapper.map(ProductRuleImageSaved, ProductRuleImageDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Edit ProductRuleImage
	public ResponseEntity<ProductRuleImageDto> editProductRuleImage(CreateProductRuleImage updateProductRuleImage, Long id) {
		ProductRuleImage productRuleImage = productRuleImageRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Image id " + id + " is not exist"));
		
		//manual map
		productRuleImage.setProductRuleImageCode(updateProductRuleImage.getProductRuleImageCode());
		productRuleImage.setProductCode(updateProductRuleImage.getProductCode());
		productRuleImage.setTitle(updateProductRuleImage.getTitle());
		productRuleImage.setImageCode(updateProductRuleImage.getImageCode());
		productRuleImage.setActive(updateProductRuleImage.isActive());
		
		productRuleImageRepository.save(productRuleImage);
		ProductRuleImageDto response = modelMapper.map(productRuleImage, ProductRuleImageDto.class);

		return ResponseEntity.ok(response);
	}
	
	//Delete ProductRuleImage
	public ResponseEntity<ProductRuleImageDto> deleteProductRuleImage(Long id) {
		ProductRuleImage productRuleImage = productRuleImageRepository.findById(id).orElseThrow(()-> new NotFoundException("Product Rule Image id " + id + " is not exist"));
		productRuleImageRepository.deleteById(id);
		ProductRuleImageDto response = modelMapper.map(productRuleImage, ProductRuleImageDto.class);

		return ResponseEntity.ok(response);
	}	
}