package id.equity.nichemarket.service.si;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.si.BaseRulePremiumPeriod.BaseRulePremiumPeriodDto;
import id.equity.nichemarket.dto.si.BaseRulePremiumPeriod.CreateBaseRulePremiumPeriod;
import id.equity.nichemarket.model.si.BaseRulePremiumPeriod;
import id.equity.nichemarket.repository.si.BaseRulePremiumPeriodRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class BaseRulePremiumPeriodService {
    @Autowired
    private BaseRulePremiumPeriodRepository baseRulePremiumPeriodRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data
    public ResponseEntity<List<BaseRulePremiumPeriodDto>> listBaseRulePremiumPeriod() {
        List<BaseRulePremiumPeriod> lsBPPeriod= baseRulePremiumPeriodRepository.findAll();
        Type targetType = new TypeToken<List<BaseRulePremiumPeriodDto>>() {}.getType();
        List<BaseRulePremiumPeriodDto> response = modelMapper.map(lsBPPeriod, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data By Id
    public ResponseEntity<BaseRulePremiumPeriodDto> getBaseRulePremiumPeriodById(Long id) {
        BaseRulePremiumPeriod bPPeriod = baseRulePremiumPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Period id " + id + " is not exist"));
        BaseRulePremiumPeriodDto response = modelMapper.map(bPPeriod, BaseRulePremiumPeriodDto.class);

        return ResponseEntity.ok(response);
    }

    //Post Data
    public ResponseEntity<BaseRulePremiumPeriodDto> addBaseRulePremiumPeriod(CreateBaseRulePremiumPeriod newBPPeriod) {
        BaseRulePremiumPeriod bPPeriod = modelMapper.map(newBPPeriod, BaseRulePremiumPeriod.class);
        BaseRulePremiumPeriod bPPeriodSaved = baseRulePremiumPeriodRepository.save(bPPeriod);
        BaseRulePremiumPeriodDto response = modelMapper.map(bPPeriodSaved, BaseRulePremiumPeriodDto.class);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<BaseRulePremiumPeriodDto> editBaseRulePremiumPeriod(CreateBaseRulePremiumPeriod updatebPPeriod, Long id) {
        BaseRulePremiumPeriod bPPeriod = baseRulePremiumPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Period id " + id + " is not exist"));
        bPPeriod.setBaseRulePremiumPeriodCode(updatebPPeriod.getBaseRulePremiumPeriodCode());
        bPPeriod.setProductCode(updatebPPeriod.getProductCode());
        bPPeriod.setPaymentPeriodCode(updatebPPeriod.getPaymentPeriodCode());
        bPPeriod.setMin(updatebPPeriod.getMin());
        bPPeriod.setMax(updatebPPeriod.getMax());
        bPPeriod.setStep(updatebPPeriod.getStep());
        bPPeriod.setMaxAge(updatebPPeriod.getMaxAge());
        bPPeriod.setActive(updatebPPeriod.isActive());
        baseRulePremiumPeriodRepository.save(bPPeriod);
        BaseRulePremiumPeriodDto response = modelMapper.map(bPPeriod, BaseRulePremiumPeriodDto.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<BaseRulePremiumPeriodDto> deleteBaseRulePremiumPeriod(Long id) {
        BaseRulePremiumPeriod bPPeriod = baseRulePremiumPeriodRepository.findById(id).orElseThrow(()-> new NotFoundException("Base Rule Premium Period id " + id + " is not exist"));
        baseRulePremiumPeriodRepository.deleteById(id);
        BaseRulePremiumPeriodDto response = modelMapper.map(bPPeriod, BaseRulePremiumPeriodDto.class);

        return ResponseEntity.ok(response);
    }
}