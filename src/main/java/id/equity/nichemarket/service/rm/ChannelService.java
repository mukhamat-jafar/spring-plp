package id.equity.nichemarket.service.rm;

import id.equity.nichemarket.config.error.NotFoundException;
import id.equity.nichemarket.dto.rm.Channel.CreateChannel;
import id.equity.nichemarket.dto.rm.Channel.ChannelDto;
import id.equity.nichemarket.model.rm.Channel;
import id.equity.nichemarket.repository.rm.ChannelRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ChannelService {

	@Autowired
	private ChannelRepository channelRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//List Channel
	public ResponseEntity<List<ChannelDto>> listChannel(){
		List<Channel> lsChannel= channelRepository.findAll();
		Type targetType = new TypeToken<List<ChannelDto>>() {}.getType();
		List<ChannelDto> response = modelMapper.map(lsChannel, targetType);

		return ResponseEntity.ok(response);
	}

	//Get Channel By Id
	public ResponseEntity<ChannelDto> getChannelById(Long id) {
		Channel channel = channelRepository.findById(id).orElseThrow(()-> new NotFoundException("Channel id " + id + "is not exist"));
		ChannelDto response = modelMapper.map(channel, ChannelDto.class);

		return ResponseEntity.ok(response);
	}

	//Post Channel
	public ResponseEntity<ChannelDto> addChannel(CreateChannel newChannel) {
		Channel channel = modelMapper.map(newChannel, Channel.class);
		Channel channelSaved = channelRepository.save(channel);
		ChannelDto response = modelMapper.map(channelSaved, ChannelDto.class);

		return ResponseEntity.ok(response);
	}

	//Put Channel
	public ResponseEntity<ChannelDto> editChannel(CreateChannel updateChannel, Long id) {
		Channel channel = channelRepository.findById(id).orElseThrow(()-> new NotFoundException("Channel id " + id + "is not exist"));
		channel.setChannelCode(updateChannel.getChannelCode());
		channel.setChannelCodeMapp(updateChannel.getChannelCodeMapp());
		channel.setDescription(updateChannel.getDescription());
		channel.setActive(updateChannel.isActive());
		channelRepository.save(channel);
		ChannelDto response = modelMapper.map(channel, ChannelDto.class);

		return ResponseEntity.ok(response);
	}

	//Delete Channel	
	public ResponseEntity<ChannelDto> deleteChannel(Long id) {
		Channel findChannel = channelRepository.findById(id).orElseThrow(()-> new NotFoundException("Channel id " + id + " is not exist"));
		channelRepository.deleteById(id);
		ChannelDto response = modelMapper.map(findChannel, ChannelDto.class);

		return ResponseEntity.ok(response);
	}
}