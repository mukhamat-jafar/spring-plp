package id.equity.nichemarket.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.equity.nichemarket.config.sftp.SftpConfig;
import id.equity.nichemarket.model.es.SpajDocument;
import id.equity.nichemarket.model.es.SpajDocumentFile;
import id.equity.nichemarket.model.es.SpajDocumentHistory;
import id.equity.nichemarket.repository.es.SpajDocumentFileRepository;
import id.equity.nichemarket.repository.es.SpajDocumentHistoryRepository;
import id.equity.nichemarket.repository.es.SpajDocumentRepository;
import id.equity.nichemarket.retrofit.eSubmission.EditsResponse;
import id.equity.nichemarket.retrofit.eSubmission.EditsService;
import id.equity.nichemarket.service.eSubmissionStore.ESubmissionDokumenData;
import id.equity.nichemarket.service.es.SpajDocumentService;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Service
public class FileService {
    @Autowired
    private SftpConfig.UploadGateway uploadGateway;
    @Autowired
    private SpajDocumentFileRepository spajDocumentFileRepository;
    @Autowired
    private SpajDocumentHistoryRepository spajDocumentHistoryRepository;
    @Autowired
    private SpajDocumentRepository spajDocumentRepository;
    @Autowired
    private EditsService editsService;
    @Autowired
    private SpajDocumentService spajDocumentService;
    @Autowired
    private ModelMapper modelMapper;

    @Value("${file.upload-dir:}")
    private String fileUploadDir;
    @Value("${sftp.remote.directory:/}")
    private String sftpRemoteDirectory;

    private void savingEditResponse(EditsResponse data, String noSpaj){
        SpajDocument spajDocument = spajDocumentRepository.findBySpajNo(noSpaj);
        if (data != null){
            spajDocument.setReference(data.getReference());
            spajDocumentRepository.save(spajDocument);
        }
        SpajDocumentHistory spajDocumentHistoryExist = spajDocumentHistoryRepository.findByDocumentCode(spajDocument.getSpajDocumentCode());

        if (spajDocumentHistoryExist != null) {
            if(data == null){
                spajDocumentHistoryExist.setDescription("Failed to send NASABAH_" + noSpaj + ".json");
                spajDocumentHistoryExist.setStatus("FAILED");
            } else {
                spajDocumentHistoryExist.setDescription(data.getDesc());
                spajDocumentHistoryExist.setStatus(data.getStatus());
            }
            spajDocumentHistoryExist.setSpajDocumentCode(spajDocument.getSpajDocumentCode());
            spajDocumentHistoryRepository.save(spajDocumentHistoryExist);
        }
    }

    public void uploadToSFTP(String noSpaj){
        try {
            List<SpajDocumentFile> list = spajDocumentFileRepository.findByNoSpaj(noSpaj);
            String spajLocalDirectory = fileUploadDir + noSpaj + "/";
            JSONObject documentData = new JSONObject();
            JSONObject subDocumentData = new JSONObject();
            JSONObject documentList = new JSONObject();
            for (SpajDocumentFile data : list) {
                File fileUpload = new File(spajLocalDirectory + data.getDocumentName());
                uploadGateway.upload(fileUpload, sftpRemoteDirectory + noSpaj);
                documentList.put(data.getDmsCode(), data.getDocumentName());
            }
            subDocumentData.put("no_spaj", noSpaj);
            subDocumentData.put("document", documentList);
            documentData.put("data", subDocumentData);

            FileWriter writer = new FileWriter(spajLocalDirectory + noSpaj + "_Image.json");
            writer.write(documentData.toString());
            writer.close();
            uploadGateway.upload(new File(spajLocalDirectory + noSpaj + "_Image.json"),
                    sftpRemoteDirectory+ noSpaj);
            ObjectMapper mapping = new ObjectMapper();
            File dataNasabah = new File(spajLocalDirectory + "NASABAH_" + noSpaj + ".json");
            ESubmissionDokumenData jsonNasabah = mapping.readValue(dataNasabah, ESubmissionDokumenData.class);
            EditsResponse responseEdits = null;
            try {
                responseEdits = editsService.postDataToEdits(jsonNasabah);
            } catch (NullPointerException e){
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //Provide code to save response to database
            savingEditResponse(responseEdits, noSpaj);
        } catch (IOException err) {
            err.printStackTrace();
        }
    }

    public void updateEditsResponse(SpajDocumentHistory spajDocumentHistory, SpajDocument spajDocument, EditsResponse response) {
        spajDocumentHistory.setStatus(response.getStatus());
        spajDocumentHistory.setDescription(response.getDesc());
        spajDocumentHistoryRepository.save(spajDocumentHistory);
        spajDocument.setReference(response.getReference());
        spajDocumentRepository.save(spajDocument);
    }
}