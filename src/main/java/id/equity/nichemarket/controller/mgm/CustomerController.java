package id.equity.nichemarket.controller.mgm;

import id.equity.nichemarket.dto.mgm.Customer.CreateCustomer;
import id.equity.nichemarket.dto.mgm.Customer.CustomerDto;
import id.equity.nichemarket.service.mgm.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/customers")
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<CustomerDto>> listCustomer(){
		return customerService.listCustomer();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<CustomerDto> getCustomerById(@PathVariable Long id) {
		return customerService.getCustomerById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<CustomerDto> addCustomer(@RequestBody CreateCustomer newCustomer) {
		return customerService.addCustomer(newCustomer);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<CustomerDto> editCustomer(@RequestBody CreateCustomer updateCustomer, @PathVariable Long id) {
		return customerService.editCustomer(updateCustomer, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<CustomerDto> deleteInvest(@PathVariable Long id) {
		return customerService.deleteCustomer(id);
	}	
}