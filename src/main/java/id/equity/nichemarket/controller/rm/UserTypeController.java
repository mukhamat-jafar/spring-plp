package id.equity.nichemarket.controller.rm;

import id.equity.nichemarket.config.error.ResourceNotFoundException;
import id.equity.nichemarket.dto.rm.userType.CreateUserTypeDto;
import id.equity.nichemarket.dto.rm.userType.UserTypeDto;
import id.equity.nichemarket.model.rm.UserType;
import id.equity.nichemarket.repository.rm.UserTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.reflect.Type;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1/users-types")
public class UserTypeController {
    @Autowired
    private UserTypeRepository userTypeRepository;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<List<UserTypeDto>> find() {
        Iterable<UserType> users = userTypeRepository.findAll();
        Type targetListType = new TypeToken<List<UserTypeDto>>() {}.getType();
        List<UserTypeDto> userDtos = modelMapper.map(users, targetListType);

        return ResponseEntity.ok(userDtos);
    }

    @PostMapping
    public ResponseEntity<UserTypeDto> create(
            @Valid
            @RequestBody
                    CreateUserTypeDto createUserTypeDto
    ) {
        UserType userTypeToSave = modelMapper.map(createUserTypeDto, UserType.class);
        UserType userType = userTypeRepository.save(userTypeToSave);
        UserTypeDto userTypeDto = modelMapper.map(userType, UserTypeDto.class);

        return ResponseEntity.status(HttpStatus.CREATED).body(userTypeDto);
    }

    @GetMapping("{id}")
    public ResponseEntity<UserTypeDto> get(@PathVariable Long id) throws ResourceNotFoundException {
        UserType user = userTypeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        UserTypeDto userDto = modelMapper.map(user, UserTypeDto.class);

        return ResponseEntity.ok(userDto);
    }

    @PatchMapping("{id}")
    public ResponseEntity<UserTypeDto> patch(
            @PathVariable
            Long id,
            @Valid
            @RequestBody
            CreateUserTypeDto createUserTypeDto
    ) throws ResourceNotFoundException {
        UserType userType = userTypeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        modelMapper.map(createUserTypeDto, userType);
        UserType userTypeSaved = userTypeRepository.save(userType);
        UserTypeDto userTypeDto = modelMapper.map(userTypeSaved, UserTypeDto.class);

        return ResponseEntity.ok(userTypeDto);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<UserTypeDto> remove(@PathVariable Long id) throws ResourceNotFoundException {
        UserType user = userTypeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Resource with id '%d' not found", id)));
        UserTypeDto userDto = modelMapper.map(user, UserTypeDto.class);

        userTypeRepository.delete(user);

        return ResponseEntity.ok(userDto);
    }

}
