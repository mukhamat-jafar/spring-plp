package id.equity.nichemarket.controller.rm;

import id.equity.nichemarket.dto.rm.GenderRM.CreateGenderRM;
import id.equity.nichemarket.dto.rm.GenderRM.GenderRMDto;
import id.equity.nichemarket.dto.si.Gender.CreateGender;
import id.equity.nichemarket.service.rm.GenderRMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/gender-rm")
public class GenderRMController {

	@Autowired
	private GenderRMService genderRMService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<GenderRMDto>> listGender(){
		return genderRMService.listGender();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<GenderRMDto> getGenderById(@PathVariable Long id) {
		return genderRMService.getGenderById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<GenderRMDto> addGender(@RequestBody CreateGender newGender) {
		return genderRMService.addGender(newGender);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<GenderRMDto> editGender(@RequestBody CreateGenderRM updateGender, @PathVariable Long id) {
		return genderRMService.editGender(updateGender, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<GenderRMDto> deleteInvest(@PathVariable Long id) {
		return genderRMService.deleteGender(id);
	}	
}