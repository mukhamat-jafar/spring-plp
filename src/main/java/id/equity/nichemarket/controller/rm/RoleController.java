package id.equity.nichemarket.controller.rm;

import id.equity.nichemarket.dto.rm.Role.CreateRole;
import id.equity.nichemarket.dto.rm.Role.RoleDto;
import id.equity.nichemarket.service.rm.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/roles")
public class RoleController {

	@Autowired
	private RoleService roleService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<RoleDto>> listRole(){
		return roleService.listRole();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<RoleDto> getRoleById(@PathVariable Long id) {
		return roleService.getRoleById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<RoleDto> addRole(@RequestBody CreateRole newRole) {
		return roleService.addRole(newRole);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<RoleDto> editRole(@RequestBody CreateRole updateRole, @PathVariable Long id) {
		return roleService.editRole(updateRole, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<RoleDto> deleteRole(@PathVariable Long id) {
		return roleService.deleteRole(id);
	}	
}