package id.equity.nichemarket.controller.rm;

import id.equity.nichemarket.dto.rm.UserRole.CreateUserRole;
import id.equity.nichemarket.dto.rm.UserRole.UserRoleDto;
import id.equity.nichemarket.service.rm.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/user-roles")
public class UserRoleController {

	@Autowired
	private UserRoleService userRoleService;
	
	//GET ALL DATA
	@GetMapping
	public ResponseEntity<List<UserRoleDto>> listUserRole(){
		return userRoleService.listUserRole();
	}

	//GET DATA BY ID
	@GetMapping("{id}")
	public ResponseEntity<UserRoleDto> getUserRoleById(@PathVariable Long id) {
		return userRoleService.getUserRoleById(id);
	}
	
	//POST DATA
	@PostMapping
	public ResponseEntity<UserRoleDto> addUserRole(@RequestBody CreateUserRole newUserRole) {
		return userRoleService.addUserRole(newUserRole);
	}
	
	//PUT DATA
	@PutMapping("{id}")
	public ResponseEntity<UserRoleDto> editUserRole(@RequestBody CreateUserRole updateUserRole, @PathVariable Long id) {
		return userRoleService.editUserRole(updateUserRole, id);
	}
	
	//DETELE DATA
	@DeleteMapping("{id}")
	public ResponseEntity<UserRoleDto> deleteUserRole(@PathVariable Long id) {
		return userRoleService.deleteUserRole(id);
	}	
}