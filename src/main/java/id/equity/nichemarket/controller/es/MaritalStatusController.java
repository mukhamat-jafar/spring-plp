package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.MaritalStatus.CreateMaritalStatus;
import id.equity.nichemarket.dto.es.MaritalStatus.MaritalStatusDto;
import id.equity.nichemarket.service.es.MaritalStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/marital-status")
public class MaritalStatusController {
    @Autowired
    private MaritalStatusService maritalStatusService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<MaritalStatusDto>> listMaritalStatus(){
        return maritalStatusService.listMaritalStatus();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<MaritalStatusDto> getMaritalStatusById(@PathVariable Long id) {
        return maritalStatusService.getMaritalStatusById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<MaritalStatusDto> addMaritalStatus (@RequestBody CreateMaritalStatus newMaritalStatus) {
        return maritalStatusService.addMaritalStatus(newMaritalStatus);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<MaritalStatusDto> editMaritalStatus(@RequestBody CreateMaritalStatus updateMaritalStatus, @PathVariable Long id) {
        return maritalStatusService.editMaritalStatus(updateMaritalStatus, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<MaritalStatusDto> deleteMaritalStatus(@PathVariable Long id) {
        return maritalStatusService.deleteMaritalStatus(id);
    }
}