package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajPolicyHolder.CreateSpajPolicyHolder;
import id.equity.nichemarket.dto.es.SpajPolicyHolder.SpajPolicyHolderDto;
import id.equity.nichemarket.service.es.SpajPolicyHolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-policy-holders")
public class SpajPolicyHolderController {
    @Autowired
    private SpajPolicyHolderService spajPolicyHolderService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajPolicyHolderDto>> listSpajPolicyHolder(){
        return spajPolicyHolderService.listSpajPolicyHolder();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajPolicyHolderDto> getSpajPolicyHolderById(@PathVariable Long id) {
        return spajPolicyHolderService.getSpajPolicyHolderById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajPolicyHolderDto> addSpajPolicyHolder (@RequestBody CreateSpajPolicyHolder newSpajPolicyHolder) {
        return spajPolicyHolderService.addSpajPolicyHolder(newSpajPolicyHolder);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajPolicyHolderDto> editSpajPolicyHolder(@RequestBody CreateSpajPolicyHolder updateSpajPolicyHolder, @PathVariable Long id) {
        return spajPolicyHolderService.editSpajPolicyHolder(updateSpajPolicyHolder, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajPolicyHolderDto> deleteSpajPolicyHolder(@PathVariable Long id) {
        return spajPolicyHolderService.deleteSpajPolicyHolder(id);
    }
}