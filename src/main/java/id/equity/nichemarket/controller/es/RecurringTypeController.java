package id.equity.nichemarket.controller.es;

import java.util.List;


import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.RecurringType.CreateRecurringType;
import id.equity.nichemarket.dto.es.RecurringType.RecurringTypeDto;
import id.equity.nichemarket.service.es.RecurringTypeService;

@RestController
@RequestMapping("api/v1/recurring-types")
public class RecurringTypeController {
	
	@Autowired
	private RecurringTypeService recurringTypeService;
	
	//get All RecurringType
	@GetMapping
	public ResponseEntity<List<RecurringTypeDto>> listRecurringType(){
		return recurringTypeService.listRecurringType();
	}
	
	//Get RecurringType By Id
	@GetMapping("{id}")
	public ResponseEntity<RecurringTypeDto> getRecurringTypeById(@PathVariable Long id) {
		return recurringTypeService.getRecurringTypeById(id);
	}
	
	//Post RecurringType
	@PostMapping
	public ResponseEntity<RecurringTypeDto> addRecurringType(@RequestBody CreateRecurringType newRecurringType) {
		return recurringTypeService.addRecurringType(newRecurringType);
	}
	
	//Put RecurringType
	@PutMapping("{id}")
	public ResponseEntity<RecurringTypeDto> editRecurringType(@RequestBody CreateRecurringType updateRecurringType, @PathVariable Long id) {
		return recurringTypeService.editRecurringType(updateRecurringType, id);
	}
	
	//Delete RecurringType
	@DeleteMapping("{id}")
	public ResponseEntity<RecurringTypeDto> deleteRecurringType(@PathVariable Long id) {
		return recurringTypeService.deleteRecurringType(id);
	}
}