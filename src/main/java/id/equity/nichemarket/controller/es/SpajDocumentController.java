package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajDocument.CreateSpajDocument;
import id.equity.nichemarket.dto.es.SpajDocument.SpajDocumentDto;
import id.equity.nichemarket.service.es.SpajDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-documents")
public class SpajDocumentController {
    @Autowired
    private SpajDocumentService spajDocumentService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajDocumentDto>> listSpajDocument(){
        return spajDocumentService.listSpajDocument();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajDocumentDto> getSpajDocumentById(@PathVariable Long id) {
        return spajDocumentService.getSpajDocumentById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajDocumentDto> addSpajDocument (@RequestBody CreateSpajDocument newSpajDocument) {
        return spajDocumentService.addSpajDocument(newSpajDocument);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajDocumentDto> editSpajDocument(@RequestBody CreateSpajDocument updateSpajDocument, @PathVariable Long id) {
        return spajDocumentService.editSpajDocument(updateSpajDocument, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajDocumentDto> deleteSpajDocument(@PathVariable Long id) {
        return spajDocumentService.deleteSpajDocument(id);
    }
}