package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajCoveragePremiumDetail.CreateSpajCoveragePremiumDetail;
import id.equity.nichemarket.dto.es.SpajCoveragePremiumDetail.SpajCoveragePremiumDetailDto;
import id.equity.nichemarket.service.es.SpajCoveragePremiumDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-coverage-premium-details")
public class SpajCoveragePremiumDetailController {
    @Autowired
    private SpajCoveragePremiumDetailService spajCoveragePremiumDetailService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajCoveragePremiumDetailDto>> listSpajCoveragePremiumDetail(){
        return spajCoveragePremiumDetailService.listSpajCoveragePremiumDetail();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajCoveragePremiumDetailDto> getSpajCoveragePremiumDetailById(@PathVariable Long id) {
        return spajCoveragePremiumDetailService.getSpajCoveragePremiumDetailById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajCoveragePremiumDetailDto> addSpajCoveragePremiumDetail (@RequestBody CreateSpajCoveragePremiumDetail newSpajCoveragePremiumDetail) {
        return spajCoveragePremiumDetailService.addSpajCoveragePremiumDetail(newSpajCoveragePremiumDetail);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajCoveragePremiumDetailDto> editSpajCoveragePremiumDetail(@RequestBody CreateSpajCoveragePremiumDetail updateSpajCoveragePremiumDetail, @PathVariable Long id) {
        return spajCoveragePremiumDetailService.editSpajCoveragePremiumDetail(updateSpajCoveragePremiumDetail, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajCoveragePremiumDetailDto> deleteSpajCoveragePremiumDetail(@PathVariable Long id) {
        return spajCoveragePremiumDetailService.deleteSpajCoveragePremiumDetail(id);
    }
}