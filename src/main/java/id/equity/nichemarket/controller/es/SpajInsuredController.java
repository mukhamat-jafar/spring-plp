package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajInsured.CreateSpajInsured;
import id.equity.nichemarket.dto.es.SpajInsured.SpajInsuredDto;
import id.equity.nichemarket.service.es.SpajInsuredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-insureds")
public class SpajInsuredController {
    @Autowired
    private SpajInsuredService spajInsuredService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajInsuredDto>> listSpajInsured(){
        return spajInsuredService.listSpajInsured();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajInsuredDto> getSpajInsuredById(@PathVariable Long id) {
        return spajInsuredService.getSpajInsuredById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajInsuredDto> addSpajInsured (@RequestBody CreateSpajInsured newSpajInsured) {
        return spajInsuredService.addSpajInsured(newSpajInsured);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajInsuredDto> editSpajInsured(@RequestBody CreateSpajInsured updateSpajInsured, @PathVariable Long id) {
        return spajInsuredService.editSpajInsured(updateSpajInsured, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajInsuredDto> deleteSpajInsured(@PathVariable Long id) {
        return spajInsuredService.deleteSpajInsured(id);
    }
}