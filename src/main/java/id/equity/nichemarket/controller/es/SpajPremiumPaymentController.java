package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajPremiumPayment.CreateSpajPremiumPayment;
import id.equity.nichemarket.dto.es.SpajPremiumPayment.SpajPremiumPaymentDto;
import id.equity.nichemarket.service.es.SpajPremiumPaymentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-premium-payments")
public class SpajPremiumPaymentController {
    
	@Autowired
    private SpajPremiumPaymentService spajPremiumPaymentService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajPremiumPaymentDto>> listSpajPremiumPayment(){
        return spajPremiumPaymentService.listSpajPremiumPayment();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajPremiumPaymentDto> getSpajPremiumPaymentById(@PathVariable Long id) {
        return spajPremiumPaymentService.getSpajPremiumPaymentById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajPremiumPaymentDto> addSpajPremiumPayment (@RequestBody CreateSpajPremiumPayment newSpajPremiumPayment) {
        return spajPremiumPaymentService.addSpajPremiumPayment(newSpajPremiumPayment);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajPremiumPaymentDto> editSpajPremiumPayment(@RequestBody CreateSpajPremiumPayment updateSpajPremiumPayment, @PathVariable Long id) {
        return spajPremiumPaymentService.editSpajPremiumPayment(updateSpajPremiumPayment, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajPremiumPaymentDto> deleteSpajPremiumPayment(@PathVariable Long id) {
        return spajPremiumPaymentService.deleteSpajPremiumPayment(id);
    }
}