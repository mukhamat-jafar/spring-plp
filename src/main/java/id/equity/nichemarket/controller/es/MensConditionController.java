package id.equity.nichemarket.controller.es;

import java.util.List;


import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.MensCondition.CreateMensCondition;
import id.equity.nichemarket.dto.es.MensCondition.MensConditionDto;
import id.equity.nichemarket.service.es.MensConditionService;
@RestController
@RequestMapping("api/v1/mens-conditions")
public class MensConditionController {
	
	@Autowired
	private MensConditionService mensConditionService;
	
	//get All MensCondition
	@GetMapping
	public ResponseEntity<List<MensConditionDto>> listMensCondition(){
		return mensConditionService.listMensCondition();
	}
	
	//Get MensCondition By Id
	@GetMapping("{id}")
	public ResponseEntity<MensConditionDto> getMensConditionById(@PathVariable Long id) {
		return mensConditionService.getMensConditionById(id);
	}
	
	//Post MensCondition
	@PostMapping
	public ResponseEntity<MensConditionDto> addMensCondition(@RequestBody CreateMensCondition newMensCondition) {
		return mensConditionService.addMensCondition(newMensCondition);
	}
	
	//Put MensCondition
	@PutMapping("{id}")
	public ResponseEntity<MensConditionDto> editMensCondition(@RequestBody CreateMensCondition updateMensCondition, @PathVariable Long id) {
		return mensConditionService.editMensCondition(updateMensCondition, id);
	}
	
	//Delete MensCondition
	@DeleteMapping("{id}")
	public ResponseEntity<MensConditionDto> deleteMensCondition(@PathVariable Long id) {
		return mensConditionService.deleteMensCondition(id);
	}
}