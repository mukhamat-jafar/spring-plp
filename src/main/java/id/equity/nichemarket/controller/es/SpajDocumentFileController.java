package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajDocumentFile.CreateSpajDocumentFile;
import id.equity.nichemarket.dto.es.SpajDocumentFile.SpajDocumentFileDto;
import id.equity.nichemarket.service.es.SpajDocumentFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-document-files")
public class SpajDocumentFileController {
    @Autowired
    private SpajDocumentFileService spajDocumentFileService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajDocumentFileDto>> listSpajDocumentFile(){
        return spajDocumentFileService.listSpajDocumentFile();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajDocumentFileDto> getSpajDocumentFileById(@PathVariable Long id) {
        return spajDocumentFileService.getSpajDocumentFileById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajDocumentFileDto> addSpajDocumentFile (@RequestBody CreateSpajDocumentFile newSpajDocumentFile) {
        return spajDocumentFileService.addSpajDocumentFile(newSpajDocumentFile);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajDocumentFileDto> editSpajDocumentFile(@RequestBody CreateSpajDocumentFile updateSpajDocumentFile, @PathVariable Long id) {
        return spajDocumentFileService.editSpajDocumentFile(updateSpajDocumentFile, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajDocumentFileDto> deleteSpajDocumentFile(@PathVariable Long id) {
        return spajDocumentFileService.deleteSpajDocumentFile(id);
    }
}
