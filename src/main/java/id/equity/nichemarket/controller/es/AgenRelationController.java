package id.equity.nichemarket.controller.es;

import java.util.List;


import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.es.AgenRelation.AgenRelationDto;
import id.equity.nichemarket.dto.es.AgenRelation.CreateAgenRelation;
import id.equity.nichemarket.service.es.AgenRelationService;

@RestController
@RequestMapping("api/v1/agen-relations")
public class AgenRelationController {
	
	@Autowired
	private AgenRelationService agenRelationService;
	
	//get All AgenRelation
	@GetMapping
	public ResponseEntity<List<AgenRelationDto>> listAgenRelation(){
		return agenRelationService.listAgenRelation();
	}
	
	//Get agenRelation By Id
	@GetMapping("{id}")
	public ResponseEntity<AgenRelationDto> getAgenRelationById(@PathVariable Long id) {
		return agenRelationService.getAgenRelationById(id);
	}
	
	//Post AgenRelation
	@PostMapping
	public ResponseEntity<AgenRelationDto> addAgenRelation(@RequestBody CreateAgenRelation newAgenRelation) {
		return agenRelationService.addAgenRelation(newAgenRelation);
	}
	
	//Put AgenRelation
	@PutMapping("{id}")
	public ResponseEntity<AgenRelationDto> editAgenRelation(@RequestBody CreateAgenRelation updateAgenRelation, @PathVariable Long id) {
		return agenRelationService.editAgenRelation(updateAgenRelation, id);
	}
	
	//Delete AgenRelation
	@DeleteMapping("{id}")
	public ResponseEntity<AgenRelationDto> deleteAgenRelation(@PathVariable Long id) {
		return agenRelationService.deleteAgenRelation(id);
	}
}