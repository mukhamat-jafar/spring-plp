package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajAdditionalStat.CreateSpajAdditionalStat;
import id.equity.nichemarket.dto.es.SpajAdditionalStat.SpajAdditionalStatDto;
import id.equity.nichemarket.service.es.SpajAdditionalStatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-additional-stats")
public class SpajAdditionalStatController {
    @Autowired
    private SpajAdditionalStatService spajAdditionalStatService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajAdditionalStatDto>> listSpajAdditionalStat(){
        return spajAdditionalStatService.listSpajAdditionalStat();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajAdditionalStatDto> getSpajAdditionalStatById(@PathVariable Long id) {
        return spajAdditionalStatService.getSpajAdditionalStatById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajAdditionalStatDto> addSpajAdditionalStat (@RequestBody CreateSpajAdditionalStat newSpajAdditionalStat) {
        return spajAdditionalStatService.addSpajAdditionalStat(newSpajAdditionalStat);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajAdditionalStatDto> editSpajAdditionalStat(@RequestBody CreateSpajAdditionalStat updateSpajAdditionalStat, @PathVariable Long id) {
        return spajAdditionalStatService.editSpajAdditionalStat(updateSpajAdditionalStat, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajAdditionalStatDto> deleteSpajAdditionalStat(@PathVariable Long id) {
        return spajAdditionalStatService.deleteSpajAdditionalStat(id);
    }
}