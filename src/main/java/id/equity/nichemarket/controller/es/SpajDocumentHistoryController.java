package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.SpajDocumentHistory.CreateSpajDocumentHistory;
import id.equity.nichemarket.dto.es.SpajDocumentHistory.SpajDocumentHistoryDto;
import id.equity.nichemarket.service.es.SpajDocumentHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/spaj-document-histories")
public class SpajDocumentHistoryController {

    @Autowired
    private SpajDocumentHistoryService spajDocumentHistoryService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<SpajDocumentHistoryDto>> listSpajDocumentHistory(){
        return spajDocumentHistoryService.listSpajDocumentHistory();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<SpajDocumentHistoryDto> getSpajDocumentHistoryById(@PathVariable Long id) {
        return spajDocumentHistoryService.getSpajDocumentHistoryById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<SpajDocumentHistoryDto> addSpajDocumentHistory (@RequestBody CreateSpajDocumentHistory newSpajDocumentHistory) {
        return spajDocumentHistoryService.addSpajDocumentHistory(newSpajDocumentHistory);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<SpajDocumentHistoryDto> editSpajDocumentHistory(@RequestBody CreateSpajDocumentHistory updateSpajDocumentHistory, @PathVariable Long id) {
        return spajDocumentHistoryService.editSpajDocumentHistory(updateSpajDocumentHistory, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<SpajDocumentHistoryDto> deleteSpajDocumentHistory(@PathVariable Long id) {
        return spajDocumentHistoryService.deleteSpajDocumentHistory(id);
    }
}