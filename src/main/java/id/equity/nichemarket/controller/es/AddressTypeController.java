package id.equity.nichemarket.controller.es;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.es.AddressType.AddressTypeDto;
import id.equity.nichemarket.dto.es.AddressType.CreateAddressType;
import id.equity.nichemarket.service.es.AddressTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/address-types")
public class AddressTypeController {
    @Autowired
    private AddressTypeService addressTypeService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<AddressTypeDto>> listAddressType(){
        return addressTypeService.listAddressType();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<AddressTypeDto> getAddressTypeById(@PathVariable Long id) {
        return addressTypeService.getAddressTypeById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<AddressTypeDto> addAddressType(@RequestBody CreateAddressType newAddressType) {
        return addressTypeService.addAddressType(newAddressType);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<AddressTypeDto> editAddressType(@RequestBody CreateAddressType updateAddressType, @PathVariable Long id) {
        return addressTypeService.editAddressType(updateAddressType, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<AddressTypeDto> deleteAddressType(@PathVariable Long id) {
        return addressTypeService.deleteAddressType(id);
    }
}