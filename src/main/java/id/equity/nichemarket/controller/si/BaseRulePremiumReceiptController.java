package id.equity.nichemarket.controller.si;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.si.BaseRulePremiumReceipt.BaseRulePremiumReceiptDto;
import id.equity.nichemarket.dto.si.BaseRulePremiumReceipt.CreateBaseRulePremiumReceipt;
import id.equity.nichemarket.service.si.BaseRulePremiumReceiptService;

@RestController
@RequestMapping("api/v1/base-rule-premium-receipts")
public class BaseRulePremiumReceiptController {
	
	@Autowired
	private BaseRulePremiumReceiptService baseRulePremiumReceiptService;
	
	//Get All BaseRulePremiumReceipt
	@GetMapping
	public ResponseEntity<List<BaseRulePremiumReceiptDto>> listBaseRulePremiumReceipt(){
		return baseRulePremiumReceiptService.listBaseRulePremiumReceipt();
	}
	
	//Get BaseRulePremiumReceipt By Id
	@GetMapping("{id}")
	public ResponseEntity<BaseRulePremiumReceiptDto> getBaseRulePremiumReceiptById(@PathVariable Long id) {
		return baseRulePremiumReceiptService.getBaseRulePremiumReceiptById(id);
	}
	
	//Post BaseRulePremiumReceipt
	@PostMapping
	public ResponseEntity<BaseRulePremiumReceiptDto> addBaseRulePremiumReceipt(@RequestBody CreateBaseRulePremiumReceipt newPremiumReceipt) {
		return baseRulePremiumReceiptService.addBaseRulePremiumReceipt(newPremiumReceipt);
	}
	
	//Put BaseRulePremiumReceipt
	@PutMapping("{id}")
	public ResponseEntity<BaseRulePremiumReceiptDto> editBaseRulePremiumReceipt(@RequestBody CreateBaseRulePremiumReceipt updatePremiumReceipt, @PathVariable Long id) {
		return baseRulePremiumReceiptService.editBaseRulePremiumReceipt(updatePremiumReceipt, id);
	}
	
	//Delete BaseRulePremiumReceipt
	@DeleteMapping("{id}")
	public ResponseEntity<BaseRulePremiumReceiptDto> deleteBaseRulePremiumReceipt(@PathVariable Long id) {
		return baseRulePremiumReceiptService.deleteBaseRulePremiumReceipt(id);
	}
}