package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleImage.CreateProductRuleImage;
import id.equity.nichemarket.dto.si.ProductRuleImage.ProductRuleImageDto;
import id.equity.nichemarket.service.si.ProductRuleImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-images")
public class ProductRuleImageController {
	
	@Autowired
	private ProductRuleImageService productRuleImageService;
	
	//get All ProductRuleImage
	@GetMapping
	public ResponseEntity<List<ProductRuleImageDto>> listProductRuleImage(){
		return productRuleImageService.listProductRuleImage();
	}
	
	//Get ProductRuleImage By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleImageDto> getProductRuleImageById(@PathVariable Long id) {
		return productRuleImageService.getProductRuleImageById(id);
	}
	
	//Post ProductRuleImage
	@PostMapping
	public ResponseEntity<ProductRuleImageDto> addProductRuleImage(@RequestBody CreateProductRuleImage id) {
		return productRuleImageService.addProductRuleImage(id);
	}
	
	//Edit ProductRuleImage
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleImageDto> editProductRuleImage(@RequestBody CreateProductRuleImage updateProductRuleImage, @PathVariable Long id) {
		return productRuleImageService.editProductRuleImage(updateProductRuleImage, id);
	}
	
	//Delete ProductRuleImage
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleImageDto> deleteProductRuleImage(@PathVariable Long id) {
		return productRuleImageService.deleteProductRuleImage(id);
	}
}