package id.equity.nichemarket.controller.si;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.si.BaseRulePolicyFee.BaseRulePolicyFeeDto;
import id.equity.nichemarket.dto.si.BaseRulePolicyFee.CreateBaseRulePolicyFee;

import id.equity.nichemarket.service.si.BaseRulePolicyFeeService;

@RestController
@RequestMapping("api/v1/base-rule-policy-fees")
public class BaseRulePolicyFeeController {
	
	@Autowired
	private BaseRulePolicyFeeService baseRulePolicyFeeService;
	
	//get All BaseRulePolicy
	@GetMapping
	public ResponseEntity<List<BaseRulePolicyFeeDto>> listBaseRulePolicyFee(){
		return baseRulePolicyFeeService.listBaseRulePolicyFee();
	}
	
	//Get BaseRulePolicy By Id
	@GetMapping("{id}")
	public ResponseEntity<BaseRulePolicyFeeDto> getBaseRulePolicyFeeById(@PathVariable Long id) {
		return baseRulePolicyFeeService.getBaseRulePolicyFeeById(id);
	}
	
	//Post BaseRulePolicy
	@PostMapping
	public ResponseEntity<BaseRulePolicyFeeDto> addBaseRulePolicyFee(@RequestBody CreateBaseRulePolicyFee newBaseRulePolicy) {
		return baseRulePolicyFeeService.addBaseRulePolicyFee(newBaseRulePolicy);
	}
	
	//Put BaseRulePolicy
	@PutMapping("{id}")
	public ResponseEntity<BaseRulePolicyFeeDto> editBaseRulePolicyFee(@RequestBody CreateBaseRulePolicyFee updateBaseRulePolicy, @PathVariable Long id) {
		return baseRulePolicyFeeService.editBaseRulePolicyFee(updateBaseRulePolicy, id);
	}
	
	//Delete BaseRulePolicy
	@DeleteMapping("{id}")
	public ResponseEntity<BaseRulePolicyFeeDto> deleteBaseRulePolicyFee(@PathVariable Long id) {
		return baseRulePolicyFeeService.deleteBaseRulePolicyFee(id);
	}
}