package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleGroupingSumInsured.CreateProductRuleGroupingSumInsured;
import id.equity.nichemarket.dto.si.ProductRuleGroupingSumInsured.ProductRuleGroupingSumInsuredDto;
import id.equity.nichemarket.service.si.ProductRuleGroupingSumInsuredService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-grouping-sum-insureds")
public class ProductRuleGroupingSumInsuredController {
	
	@Autowired
	private ProductRuleGroupingSumInsuredService productRuleGroupingSumInsuredService;
	
	//get All ProductRuleGroupingSumInsured
	@GetMapping
	public ResponseEntity<List<ProductRuleGroupingSumInsuredDto>> listProductRuleGroupingSumInsured(){
		return productRuleGroupingSumInsuredService.listProductRuleGroupingSumInsured();
	}
	
	//Get ProductRuleGroupingSumInsured By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleGroupingSumInsuredDto> getProductRuleGroupingSumInsuredById(@PathVariable Long id) {
		return productRuleGroupingSumInsuredService.getProductRuleGroupingSumInsuredById(id);
	}
	
	//Post ProductRuleGroupingSumInsured
	@PostMapping
	public ResponseEntity<ProductRuleGroupingSumInsuredDto> addProductRuleGroupingSumInsured(@RequestBody CreateProductRuleGroupingSumInsured newProductRuleGroupingSumInsured) {
		return productRuleGroupingSumInsuredService.addProductRuleGroupingSumInsured(newProductRuleGroupingSumInsured);
	}
	
	//Edit ProductRuleGroupingSumInsured
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleGroupingSumInsuredDto> editProductRuleGroupingSumInsured(@RequestBody CreateProductRuleGroupingSumInsured updateProductRuleGroupingSumInsured, @PathVariable Long id) {
		return productRuleGroupingSumInsuredService.editProductRuleGroupingSumInsured(updateProductRuleGroupingSumInsured, id);
	}
	
	//Delete ProductRuleGroupingSumInsured
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleGroupingSumInsuredDto> deleteProductRuleGroupingSumInsured(@PathVariable Long id) {
		return productRuleGroupingSumInsuredService.deleteProductRuleGroupingSumInsured(id);
	}
}