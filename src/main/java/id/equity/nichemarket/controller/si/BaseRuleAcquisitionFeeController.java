package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRuleAcquisitionFee.CreateBaseRuleAcquisitionFee;
import id.equity.nichemarket.dto.si.BaseRuleAcquisitionFee.BaseRuleAcquisitionFeeDto;
import id.equity.nichemarket.service.si.BaseRuleAcquisitionFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-acquisition-fees")
public class BaseRuleAcquisitionFeeController {

	@Autowired
	private BaseRuleAcquisitionFeeService baseRuleAcquisitionFeeService;
	
	//GET ALL DATA
		@GetMapping
		public ResponseEntity<List<BaseRuleAcquisitionFeeDto>> listBaseRuleAcquisitionFee(){
			return baseRuleAcquisitionFeeService.listBaseRuleAcquisitionFee();
		}
		
		//GET DATA BY ID
		@GetMapping("{id}")
		public ResponseEntity<BaseRuleAcquisitionFeeDto> getBaseRuleAcquisitionFeeById(@PathVariable Long id) {
			return baseRuleAcquisitionFeeService.getBaseRuleAcquisitionFeeById(id);
		}
		
		//POST DATA
		@PostMapping
		public ResponseEntity<BaseRuleAcquisitionFeeDto> addBaseRuleAcquisitionFee(@RequestBody CreateBaseRuleAcquisitionFee newBRAFee) {
			return baseRuleAcquisitionFeeService.addBaseRuleAcquisitionFee(newBRAFee);
		}
		
		//PUT DATA
		@PutMapping("{id}")
		public ResponseEntity<BaseRuleAcquisitionFeeDto> editBaseRuleAcquisitionFee(@RequestBody CreateBaseRuleAcquisitionFee updateBRAFee, @PathVariable Long id) {
			return baseRuleAcquisitionFeeService.editBaseRuleAcquisitionFee(updateBRAFee, id);
		}
		
		//DETELE DATA
		@DeleteMapping("{id}")
		public ResponseEntity<BaseRuleAcquisitionFeeDto> deleteBaseRuleAcquisitionFee(@PathVariable Long id) {
			return baseRuleAcquisitionFeeService.deleteBaseRuleAcquisitionFee(id);
		}
}