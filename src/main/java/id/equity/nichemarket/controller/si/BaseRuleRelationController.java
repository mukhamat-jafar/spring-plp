package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.BaseRuleRelation.BaseRuleRelationDto;
import id.equity.nichemarket.dto.si.BaseRuleRelation.CreateBaseRuleRelation;
import id.equity.nichemarket.service.si.BaseRuleRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-relations")
public class BaseRuleRelationController {
    @Autowired
    private BaseRuleRelationService baseRuleRelationService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<BaseRuleRelationDto>> listBaseRuleRelation(){
        return baseRuleRelationService.listBaseRuleRelation();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<BaseRuleRelationDto> getBaseRuleRelationById(@PathVariable Long id) {
        return baseRuleRelationService.getBaseRuleRelationById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<BaseRuleRelationDto> addBaseRuleRelation(@RequestBody CreateBaseRuleRelation newBRRelation) {
        return baseRuleRelationService.addBaseRuleRelation(newBRRelation);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<BaseRuleRelationDto> editBaseRuleRelation(@RequestBody CreateBaseRuleRelation updateBRRelation, @PathVariable Long id) {
        return baseRuleRelationService.editBaseRuleRelation(updateBRRelation, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<BaseRuleRelationDto> deleteBaseRuleRelation(@PathVariable Long id) {
        return baseRuleRelationService.deleteBaseRuleRelation(id);
    }
}