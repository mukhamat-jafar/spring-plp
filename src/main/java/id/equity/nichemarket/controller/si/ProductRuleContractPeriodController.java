package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleContractPeriod.CreateProductRuleContractPeriod;
import id.equity.nichemarket.dto.si.ProductRuleContractPeriod.ProductRuleContractPeriodDto;
import id.equity.nichemarket.service.si.ProductRuleContractPeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-contract-periods")
public class ProductRuleContractPeriodController {
	
	@Autowired
	private ProductRuleContractPeriodService productRuleContractPeriodService;
	
	//get All ProductRuleContractPeriod
	@GetMapping
	public ResponseEntity<List<ProductRuleContractPeriodDto>> listProductRuleContractPeriod(){
		return productRuleContractPeriodService.listProductRuleContractPeriod();
	}
	
	//Get ProductRuleContractPeriod By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleContractPeriodDto> getProductRuleContractPeriodById(@PathVariable Long id) {
		return productRuleContractPeriodService.getProductRuleContractPeriodById(id);
	}
	
	//Post ProductRuleContractPeriod
	@PostMapping
	public ResponseEntity<ProductRuleContractPeriodDto> addProductRuleContractPeriod(@RequestBody CreateProductRuleContractPeriod id) {
		return productRuleContractPeriodService.addProductRuleContractPeriod(id);
	}
	
	//Edit ProductRuleContractPeriod
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleContractPeriodDto> editProductRuleContractPeriod(@RequestBody CreateProductRuleContractPeriod updateProductRuleContractPeriod, @PathVariable Long id) {
		return productRuleContractPeriodService.editProductRuleContractPeriod(updateProductRuleContractPeriod, id);
	}
	
	//Delete ProductRuleContractPeriod
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleContractPeriodDto> deleteProductRuleContractPeriod(@PathVariable Long id) {
		return productRuleContractPeriodService.deleteProductRuleContractPeriod(id);
	}
}