package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.ProductRuleNote.CreateProductRuleNote;
import id.equity.nichemarket.dto.si.ProductRuleNote.ProductRuleNoteDto;
import id.equity.nichemarket.service.si.ProductRuleNoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product-rule-notes")
public class ProductRuleNoteController {
	
	@Autowired
	private ProductRuleNoteService productRuleNoteService;
	
	//get All ProductRuleNote
	@GetMapping
	public ResponseEntity<List<ProductRuleNoteDto>> listProductRuleNote(){
		return productRuleNoteService.listProductRuleNote();
	}
	
	//Get ProductRuleNote By Id
	@GetMapping("{id}")
	public ResponseEntity<ProductRuleNoteDto> getProductRuleNoteById(@PathVariable Long id) {
		return productRuleNoteService.getProductRuleNoteById(id);
	}
	
	//Post ProductRuleNote
	@PostMapping
	public ResponseEntity<ProductRuleNoteDto> addProductRuleNote(@RequestBody CreateProductRuleNote id) {
		return productRuleNoteService.addProductRuleNote(id);
	}
	
	//Edit ProductRuleNote
	@PutMapping("{id}")
	public ResponseEntity<ProductRuleNoteDto> editProductRuleNote(@RequestBody CreateProductRuleNote updateProductRuleNote, @PathVariable Long id) {
		return productRuleNoteService.editProductRuleNote(updateProductRuleNote, id);
	}
	
	//Delete ProductRuleNote
	@DeleteMapping("{id}")
	public ResponseEntity<ProductRuleNoteDto> deleteProductRuleNote(@PathVariable Long id) {
		return productRuleNoteService.deleteProductRuleNote(id);
	}
}