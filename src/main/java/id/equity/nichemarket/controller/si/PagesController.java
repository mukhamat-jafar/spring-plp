package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.dto.si.Pages.CreatePages;
import id.equity.nichemarket.dto.si.Pages.PagesDto;
import id.equity.nichemarket.service.si.PagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/pages")
public class PagesController {

	@Autowired
	private PagesService pagesService;
	
	//Get Payment Period
	@GetMapping
	public ResponseEntity<List<PagesDto>> listPages() {
		return pagesService.listPages();
	}
	
	//Get Payment Period Id
	@GetMapping("{id}")
	public ResponseEntity<PagesDto> getPagesById(@PathVariable Long id) {
		return pagesService.getPagesById(id);
	}
	
	//Post Payment Period
	@PostMapping
	public ResponseEntity<PagesDto> addPages(@RequestBody CreatePages newPages) {
		return pagesService.addPages(newPages);
	}
	
	//Put Payment Period
	@PutMapping("{id}")
	public ResponseEntity<PagesDto> editPages(@RequestBody CreatePages updatePages, @PathVariable Long id) {
		return pagesService.editPages(updatePages, id);
	}
	
	//Delete Payment Period
	@DeleteMapping("{id}")
	public ResponseEntity<PagesDto> deletePages(@PathVariable Long id) {
		return pagesService.deletePages(id);
	}
}