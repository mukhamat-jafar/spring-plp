package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRulePaymentPeriod.BaseRulePaymentPeriodDto;
import id.equity.nichemarket.dto.si.BaseRulePaymentPeriod.CreateBaseRulePaymentPeriod;
import id.equity.nichemarket.service.si.BaseRulePaymentPeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-payment-periods")
public class BaseRulePaymentPeriodController {
    @Autowired
    private BaseRulePaymentPeriodService baseRulePaymentPeriodService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<BaseRulePaymentPeriodDto>> listBaseRulePaymentPeriod(){
        return baseRulePaymentPeriodService.listBaseRulePaymentPeriod();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<BaseRulePaymentPeriodDto> getBaseRulePaymentPeriodById(@PathVariable Long id) {
        return baseRulePaymentPeriodService.getBaseRulePaymentPeriodById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<BaseRulePaymentPeriodDto> addBaseRulePaymentPeriod(@RequestBody CreateBaseRulePaymentPeriod newBaseRulePaymentPeriod) {
        return baseRulePaymentPeriodService.addBaseRulePaymentPeriod(newBaseRulePaymentPeriod);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<BaseRulePaymentPeriodDto> editBaseRulePaymentPeriod(@RequestBody CreateBaseRulePaymentPeriod updateBaseRulePaymentPeriod, @PathVariable Long id) {
        return baseRulePaymentPeriodService.editBaseRulePaymentPeriod(updateBaseRulePaymentPeriod, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<BaseRulePaymentPeriodDto> deleteBaseRulePaymentPeriod(@PathVariable Long id) {
        return baseRulePaymentPeriodService.deleteBaseRulePaymentPeriod(id);
    }
}