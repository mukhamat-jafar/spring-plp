package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.RelationType.CreateRelationType;
import id.equity.nichemarket.dto.si.RelationType.RelationTypeDto;
import id.equity.nichemarket.service.si.RelationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/relation-types")
public class RelationTypeController {
    @Autowired
    private RelationTypeService relationTypeService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<RelationTypeDto>> listRelationType(){
        return relationTypeService.listRelationType();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<RelationTypeDto> getRelationTypeById(@PathVariable Long id) {
        return relationTypeService.getRelationTypeById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<RelationTypeDto> addRelationType(@RequestBody CreateRelationType newRelationType) {
        return relationTypeService.addRelationType(newRelationType);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<RelationTypeDto> editRelationType(@RequestBody CreateRelationType updateRelationType, @PathVariable Long id) {
        return relationTypeService.editRelationType(updateRelationType, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<RelationTypeDto> deleteRelationType(@PathVariable Long id) {
        return relationTypeService.deleteRelationType(id);
    }
}