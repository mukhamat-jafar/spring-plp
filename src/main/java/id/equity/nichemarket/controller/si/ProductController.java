package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.Product.CreateProduct;
import id.equity.nichemarket.dto.si.Product.ProductDto;
import id.equity.nichemarket.service.si.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/products")
public class ProductController {
	
	@Autowired
	private ProductService productService;

	@GetMapping
	public ResponseEntity<List<ProductDto>> listProduct(){
		return productService.listProduct();
	}
	
	@GetMapping("{id}")
	public ResponseEntity<ProductDto> getProductById(@PathVariable Long id) {
		return productService.getProductById(id);
	}
	
	@PostMapping
	public ResponseEntity<ProductDto> addProduct(@RequestBody CreateProduct newProduct) {
		return productService.addProduct(newProduct);
	}
	
	@PutMapping("{id}")
	public ResponseEntity<ProductDto> editProduct(@RequestBody CreateProduct updateProduct, @PathVariable Long id) {
		return productService.editProduct(updateProduct, id);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<ProductDto> deleteProduct(@PathVariable Long id) {
		return productService.deleteProduct(id);
	}
}