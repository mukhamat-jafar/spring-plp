package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.Valuta.CreateValuta;
import id.equity.nichemarket.dto.si.Valuta.ValutaDto;
import id.equity.nichemarket.service.si.ValutaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/valutas")
public class ValutaController {
	
	@Autowired
	private ValutaService valutaService;

	@GetMapping
	public ResponseEntity<List<ValutaDto>> listValuta(){
		return valutaService.listValuta();
	}

	@GetMapping("{id}")
	public ResponseEntity<ValutaDto> getValutaById(@PathVariable Long id) {
		return valutaService.getValutaById(id);
	}

	@PostMapping
	public ResponseEntity<ValutaDto> addValuta(@RequestBody CreateValuta newValuta ) {
		return valutaService.addValuta(newValuta);
	}

	@PutMapping("{id}")
	public ResponseEntity<ValutaDto> editValuta(@RequestBody CreateValuta updateValuta, @PathVariable Long id) {
		return valutaService.editValuta(updateValuta, id);
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<ValutaDto> deleteValuta(@PathVariable Long id) {
		return valutaService.deleteValuta(id);
	}
}