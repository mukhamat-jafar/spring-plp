package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.Variable.CreateVariable;
import id.equity.nichemarket.dto.si.Variable.VariableDto;
import id.equity.nichemarket.service.si.VariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/variables")
public class VariableController {

	@Autowired
	private VariableService variableService;
	
	//Get All Data
	@GetMapping
	public ResponseEntity<List<VariableDto>> listVariable(){
		return variableService.listVariable();
	}
	
	//Get Data By Id
	@GetMapping("{id}")
	public ResponseEntity<VariableDto> getVariableById(@PathVariable Long id) {
		return variableService.getVariableById(id);
	}
	
	//POST Data
	@PostMapping
	public ResponseEntity<VariableDto> addVariable(@RequestBody CreateVariable newVariable) {
		return variableService.addVariable(newVariable);
	}
	
	//PUT Data By Id
	@PutMapping("{id}")
	public ResponseEntity<VariableDto> editVariable(@RequestBody CreateVariable updateVariable, @PathVariable Long id) {
		return variableService.editVariable(updateVariable, id);
	}
	
	//Delete Data By Id
	@DeleteMapping("{id}")
	public ResponseEntity<VariableDto> deleteVariable(@PathVariable Long id) {
		return variableService.deleteVariable(id);
	}
}