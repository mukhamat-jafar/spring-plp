package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.ServiceUnit.CreateServiceUnit;
import id.equity.nichemarket.dto.si.ServiceUnit.ServiceUnitDto;
import id.equity.nichemarket.service.si.ServiceUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/service-units")
public class SerivceUnitController {

	@Autowired
	private ServiceUnitService serviceUnitService;
	
	//Get All Service Unit
	@GetMapping
	public ResponseEntity<List<ServiceUnitDto>> listServiceUnit(){
		return serviceUnitService.listServiceUnit();
	}
	
	//Get Service Unit By Id
	@GetMapping("{id}")
	public ResponseEntity<ServiceUnitDto> getServiceUnitById(@PathVariable Long id) {
		return serviceUnitService.getServiceUnitById(id);
	}
	
	//Post Service Unit
	@PostMapping
	public ResponseEntity<ServiceUnitDto> addServiceUnit(@RequestBody CreateServiceUnit newServiceUnit) {
		return serviceUnitService.addServiceUnit(newServiceUnit);
	}
	
	//Put Service Unit
	@PutMapping("{id}")
	public ResponseEntity<ServiceUnitDto> editServiceUnit(@RequestBody CreateServiceUnit updateServiceUnit, @PathVariable Long id) {
		return serviceUnitService.editServiceUnit(updateServiceUnit, id);
	}
	
	//Delete Service Unit
	@DeleteMapping("{id}")
	public ResponseEntity<ServiceUnitDto> deleteServiceUnit(@PathVariable Long id) {
		return serviceUnitService.deleteServiceUnit(id);
	}
}