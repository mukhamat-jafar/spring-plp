package id.equity.nichemarket.controller.si;

import java.util.List;

import id.equity.nichemarket.config.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.si.BaseRulePremiumPeriodic.BaseRulePremiumPeriodicDto;
import id.equity.nichemarket.dto.si.BaseRulePremiumPeriodic.CreateBaseRulePremiumPeriodic;
import id.equity.nichemarket.service.si.BaseRulePremiumPeriodicService;

@RestController
@RequestMapping("api/v1/base-rule-premium-periodic")
public class BaseRulePremiumPeriodicController {
	
	@Autowired
	private BaseRulePremiumPeriodicService baseRulePremiumPeriodicService;
	
	//Get All BaseRulePremiumPeriodic
	@GetMapping
	public ResponseEntity<List<BaseRulePremiumPeriodicDto>> listBaseRulePremiumPeriodic(){
		return baseRulePremiumPeriodicService.listBaseRulePremiumPeriodic();
	}
	
	//Get BaseRulePremiumPeriodic By Id
	@GetMapping("{id}")
	public ResponseEntity<BaseRulePremiumPeriodicDto> getBaseRulePremiumPeriodicById(@PathVariable Long id) {
		return baseRulePremiumPeriodicService.getBaseRulePremiumPeriodicById(id);
	}
	
	//Post BaseRulePremiumPeriodic
	@PostMapping
	public ResponseEntity<BaseRulePremiumPeriodicDto> addBaseRulePremiumPeriodic(@RequestBody CreateBaseRulePremiumPeriodic newPremiumPeriodic) {
		return baseRulePremiumPeriodicService.addBaseRulePremiumPeriodic(newPremiumPeriodic);
	}
	
	//Put BaseRulePremiumPeriodic
	@PutMapping("{id}")
	public ResponseEntity<BaseRulePremiumPeriodicDto> editBaseRulePremiumPeriodic(@RequestBody CreateBaseRulePremiumPeriodic updatePremiumPeriodic, @PathVariable Long id) {
		return baseRulePremiumPeriodicService.editBaseRulePremiumPeriodic(updatePremiumPeriodic, id);
	}
	
	//Delete BaseRulePremiumPeriodic
	@DeleteMapping("{id}")
	public ResponseEntity<BaseRulePremiumPeriodicDto> deleteBaseRulePremiumPeriodic(@PathVariable Long id) {
		return baseRulePremiumPeriodicService.deleteBaseRulePremiumPeriodic(id);
	}
}