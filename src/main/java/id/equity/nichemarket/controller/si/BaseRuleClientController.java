package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRuleClient.BaseRuleClientDto;
import id.equity.nichemarket.dto.si.BaseRuleClient.CreateBaseRuleClient;
import id.equity.nichemarket.service.si.BaseRuleClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-clients")
public class BaseRuleClientController {
    @Autowired
    private BaseRuleClientService baseRuleClientService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<BaseRuleClientDto>> listBaseRuleClient(){
        return baseRuleClientService.listBaseRuleClient();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<BaseRuleClientDto> getBaseRuleClientById(@PathVariable Long id) {
        return baseRuleClientService.getBaseRuleClientById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<BaseRuleClientDto> addBaseRuleClient(@RequestBody CreateBaseRuleClient newBaseRuleClient) {
        return baseRuleClientService.addBaseRuleClient(newBaseRuleClient);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<BaseRuleClientDto> editBaseRuleClient(@RequestBody CreateBaseRuleClient updateBaseRuleClient, @PathVariable Long id) {
        return baseRuleClientService.editBaseRuleClient(updateBaseRuleClient, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<BaseRuleClientDto> deleteBaseRuleClient(@PathVariable Long id) {
        return baseRuleClientService.deleteBaseRuleClient(id);
    }
}