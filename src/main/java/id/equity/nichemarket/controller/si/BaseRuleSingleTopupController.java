package id.equity.nichemarket.controller.si;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.equity.nichemarket.dto.si.BaseRuleSingleTopup.BaseRuleSingleTopupDto;
import id.equity.nichemarket.dto.si.BaseRuleSingleTopup.CreateBaseRuleSingleTopup;
import id.equity.nichemarket.service.si.BaseRuleSingleTopupService;

@RestController
@RequestMapping("api/v1/base-rule-single-topup")
public class BaseRuleSingleTopupController {
	
	@Autowired
	private BaseRuleSingleTopupService baseRuleSingleTopupService;
	
	//get All SingleTopup
	@GetMapping
	public ResponseEntity<List<BaseRuleSingleTopupDto>> listBaseRuleSingleTopup(){
		return baseRuleSingleTopupService.listBaseRuleSingleTopup();
	}
	
	//Get PeriodicTopup By Id
	@GetMapping("{id}")
	public ResponseEntity<BaseRuleSingleTopupDto> getBaseRuleSingleTopupById(@PathVariable Long id) {
		return baseRuleSingleTopupService.getBaseRuleSingleTopupById(id);
	}
	
	//Post PeriodicTopup
	@PostMapping
	public ResponseEntity<BaseRuleSingleTopupDto> addBaseRuleSingleTopup(@RequestBody CreateBaseRuleSingleTopup newSingleTopup) {
		return baseRuleSingleTopupService.addBaseRuleSingleTopup(newSingleTopup);
	}
	
	//Edit PeriodicTopup
	@PutMapping("{id}")
	public ResponseEntity<BaseRuleSingleTopupDto> editBaseRuleSingleTopup(@RequestBody CreateBaseRuleSingleTopup updateSingleTopup, @PathVariable Long id) {
		return baseRuleSingleTopupService.editBaseRuleSingleTopup(updateSingleTopup, id);
	}
	
	//Delete PeriodicTopup
	@DeleteMapping("{id}")
	public ResponseEntity<BaseRuleSingleTopupDto> deleteBaseRuleSingleTopup(@PathVariable Long id) {
		return baseRuleSingleTopupService.deleteBaseRuleSingleTopup(id);
	}	
}