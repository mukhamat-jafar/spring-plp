package id.equity.nichemarket.controller.si;

import id.equity.nichemarket.config.response.BaseResponse;
import id.equity.nichemarket.dto.si.BaseRuleRedeemFee.BaseRuleRedeemFeeDto;
import id.equity.nichemarket.dto.si.BaseRuleRedeemFee.CreateBaseRuleRedeemFee;

import id.equity.nichemarket.service.si.BaseRuleRedeemFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/base-rule-redeem-fees")
public class BaseRuleRedeemFeeController {
    @Autowired
    private BaseRuleRedeemFeeService baseRuleRedeemFeeService;

    //GET ALL DATA
    @GetMapping
    public ResponseEntity<List<BaseRuleRedeemFeeDto>> listBaseRuleRedeemFee(){
        return baseRuleRedeemFeeService.listBaseRuleRedeemFee();
    }

    //GET DATA BY ID
    @GetMapping("{id}")
    public ResponseEntity<BaseRuleRedeemFeeDto> getBaseRuleRedeemFeeById(@PathVariable Long id) {
        return baseRuleRedeemFeeService.getBaseRuleRedeemFeeById(id);
    }

    //POST DATA
    @PostMapping
    public ResponseEntity<BaseRuleRedeemFeeDto> addBaseRuleRedeemFee(@RequestBody CreateBaseRuleRedeemFee newBSRFee) {
        return baseRuleRedeemFeeService.addBaseRuleRedeemFee(newBSRFee);
    }

    //PUT DATA
    @PutMapping("{id}")
    public ResponseEntity<BaseRuleRedeemFeeDto> editBaseRuleRedeemFee(@RequestBody CreateBaseRuleRedeemFee updateBSRFee, @PathVariable Long id) {
        return baseRuleRedeemFeeService.editBaseRuleRedeemFee(updateBSRFee, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<BaseRuleRedeemFeeDto> deleteBaseRuleRedeemFee(@PathVariable Long id) {
        return baseRuleRedeemFeeService.deleteBaseRuleRedeemFee(id);
    }
}