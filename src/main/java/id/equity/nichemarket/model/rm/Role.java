package id.equity.nichemarket.model.rm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="m_role")
public class Role extends Auditable<String>{
	@Column(unique = true, length = 10, nullable = false, name = "role_code")
	private String roleCode;

	@Column(length = 100, name = "description")
	private String description;

	@Column(name = "is_active")
	private boolean isActive;
}
