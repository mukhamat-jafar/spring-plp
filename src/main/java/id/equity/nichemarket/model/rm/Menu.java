package id.equity.nichemarket.model.rm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="m_menu")
public class Menu extends Auditable<String>{
	@Column(unique = true, length = 10, nullable = false, name = "menu_code")
	private String menuCode;

	@Column(length = 30, name = "description")
	private String description;

	@Column(length = 30, name = "uri")
	private String uri;

	@Column(name = "parent_id")
	private Integer parentId;

	@Column(name = "is_active")
	private boolean isActive;
}
