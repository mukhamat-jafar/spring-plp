package id.equity.nichemarket.model.rm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@Table(name = "m_user_type")
public class UserType extends Auditable<String> {
	
    @Column(length = 10)
    private String code;

    @Column(length = 30)
    private String description;

    @Column(name = "is_active")
    private boolean isActive;
}
