package id.equity.nichemarket.model.rm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="m_user_channel")
public class UserChannel extends Auditable<String>{
	@Column(unique = true, length = 10, nullable = false, name = "user_channel_code")
	private String userChannelCode;

	@Column(length = 30, name = "username")
	private String username;

	@Column(length = 10, name = "channel_code")
	private String channelCode;

	@Column(name = "is_active")
	private boolean isActive;
}
