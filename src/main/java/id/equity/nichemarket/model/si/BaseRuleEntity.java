package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_entity")
public class BaseRuleEntity extends Auditable<String> {

    @Column(unique = true, length = 6, nullable = false, name = "base_rule_entitas_code")
    private String baseRuleEntitasCode;

    @Column(nullable = false, length = 10, name = "product_code")
    private String productCode;

    @Column(nullable = false, name = "is_premium_periodic")
    private boolean isPremiumPeriodic;

    @Column(nullable = false, name = "is_topup_periodic")
    private boolean isTopupPeriodic;

    @Column(nullable = false, name = "is_single_topup")
    private boolean isSingleTopup;

    @Column(nullable = false, name = "is_retirement")
    private boolean isRetirement;

    @Column(nullable = false, name = "retirement_age")
    private Integer retirementAge;

    @Column(nullable = false, name = "is_interest_thp")
    private Boolean isInterestThp;

    @Column(nullable = false, name = "interest_thp")
    private Integer interestThp;

    @Column(nullable = false, name = "is_allocation_fund")
    private boolean isAllocationFund;

    @Column(nullable = false, name = "is_mutation_fund")
    private boolean isMutationFund;

    @Column(nullable = false, name = "is_contract_period")
    private boolean isContractPeriod;

    @Column(nullable = false, name = "is_premium_period")
    private boolean isPremiumPeriod;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
