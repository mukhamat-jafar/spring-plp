package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_benefit_item")
public class ProductRuleBenefitItem extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_benefit_item_code")
	private String productRuleBenefitItemCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(nullable = false, name = "\"order\"")
	private Integer order;

	@Column(length = 250, name = "description")
	private String description;

	@Column(length = 120, name = "value")
	private String value;
	
	@Column(name = "is_active")
	private boolean isActive;
}