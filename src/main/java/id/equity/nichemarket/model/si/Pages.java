package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import id.equity.nichemarket.audit.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "s_page")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class Pages extends Auditable<String> {
	
	@Column(unique = true, length = 3, nullable = false, name = "page_code")
	private String pageCode;

	@Column(nullable = false, length=50, name = "description")
	private String description;

	@Column(nullable = false, name = "\"order\"")
	private Integer order;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}