package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_premium_periodic")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class BaseRulePremiumPeriodic extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "base_rule_premium_periodic_code")
	private String baseRulePremiumPeriodicCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 10, name = "valuta_code")
	private String valutaCode;
	
	@Column(length = 10, name = "payment_period_code")
	private String paymentPeriodCode;
	
	@Column(name = "min_premium")
	private Double minPremium;
	
	@Column(name = "max_premium")
	private Double maxPremium;
	
	@Column(name = "step_premium")
	private Double stepPremium;
	
	@Column(name = "is_active")
	private boolean isActive;
}
