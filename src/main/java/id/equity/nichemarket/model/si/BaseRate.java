package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "t_base_rate")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class BaseRate extends Auditable<String> {
	
	@Column(unique = true, length = 12, nullable = false, name = "base_rate_code")
	private String baseRateCode;
	
	@Column(length = 3, nullable = false, name = "category")
	private String category;
	
	@Column(length = 10, nullable = false, name = "product_code")
	private String productCode;
	
	@Column(length = 6, nullable = false, name = "valuta_code")
	private String valutaCode;
	
	@Column(length = 3, nullable = false, name = "payment_period_code")
	private String paymentPeriodCode;
	
	@Column(length = 2, nullable = false, name = "as_code")
	private String asCode;
	
	@Column(length = 2, nullable = false, name = "age")
	private int age;
	
	@Column(length = 10, nullable = false, name = "contract_period")
	private int contractPeriod;
	
	@Column(length = 10, nullable = false, name = "premium_period")
	private int premiumPeriod;
	
	@Column(length = 10, nullable = false, name = "year_th")
	private int yearTh;
	
	@Column(length = 10, nullable = false, name = "age_th")
	private int ageTh;
	
	@Column(length = 10, nullable = false, name = "rate")
	private int rate;
	
	@Column(length = 10, nullable = false, name = "divider")
	private int divider;
	
	@Column(length = 2, nullable = false, name = "is_active")
	private boolean isActive;
}

