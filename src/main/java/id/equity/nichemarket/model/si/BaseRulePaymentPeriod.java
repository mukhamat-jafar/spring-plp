package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_payment_period")
public class BaseRulePaymentPeriod extends Auditable<String> {
    @Column(unique = true, length = 10, nullable = false, name = "base_rule_payment_period_code")
    private String baseRulePaymentPeriodCode;

    @Column(nullable = false, length = 10, name = "productCode")
    private String productCode;

    @Column(nullable = false, length = 6, name = "valuta_code")
    private String valutaCode;

    @Column(nullable = false, length = 10, name = "payment_period_code")
    private String paymentPeriodCode;

    @Column(nullable = false, name = "factor")
    private Integer factor;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
