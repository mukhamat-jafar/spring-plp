package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_activate")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class ProductRuleActivate extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_activate_code")
	private String productRuleActivateCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 6, name = "valuta_code")
	private String valutaCode;
	
	@Column(length = 10, name = "payment_period_code")
	private String paymentPeriodCode;
	
	@Column(length = 1, name = "gender_code")
	private String genderCode;
	
	@Column(length = 10, name = "as_code")
	private String asCode;

	@Column(length = 10, name = "relation_type_code")
	private String relationTypeCode;

	@Column(name = "min")
	private Integer min;
	
	@Column(name = "max")
	private Integer max;
	
	@Column(length = 60, name = "rule_premium")
	private String rulePremium;
	
	@Column(name = "is_active")
	private boolean isActive;
}