package id.equity.nichemarket.model.si;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_benefit")
public class ProductRuleBenefit extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_benefit_code")
	private String productRuleBenefitCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 6, name = "valuta_code")
	private String valutaCode;

	@Column(length = 120, name = "title")
	private String title;

	@Column(length = 120, name = "header_title")
	private String headerTitle;

	@Column(length = 120, name = "header_value")
	private String headerValue;

	@Column(length = 120, name = "footer_title")
	private String footerTitle;

	@Column(length = 120, name = "footer_value")
	private String footerValue;
	
	@Column(name = "is_active")
	private boolean isActive;
}