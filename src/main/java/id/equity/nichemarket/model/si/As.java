package id.equity.nichemarket.model.si;


import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "m_as")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class As extends Auditable<String> {
		
	@Column(unique = true, length = 2, nullable = false, name = "as_code")
	private String asCode;
	
	@Column(nullable = false, length = 30, name = "description")
	private String description;
	
	@Column(nullable = false, length = 10, name = "as_code_string")
	private String asCodeString;
	
	@Column(nullable = false, length = 10, name = "is_active")
	private boolean isActive;
}