package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import id.equity.nichemarket.audit.Auditable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "t_product_rate")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class ProductRate extends Auditable<String> {

	@Column(unique = true, length = 12, nullable = false, name = "product_rate_code")
	private String productRateCode;

	@Column(length = 2, nullable = false, name = "category")
	private String category;

	@Column(length = 10, nullable = false, name = "product_code")
	private String productCode;

	@Column(length = 6, nullable = false, name = "valuta_code")
	private String valutaCode;

	@Column(length = 3, nullable = false, name = "payment_period_code")
	private String paymentPeriodCode;

	@Column(length = 2, nullable = false, name = "as_code")
	private String asCode;

	@Column(length = 1, nullable = false, name = "gender_code")
	private String genderCode;

	@Column(nullable = false, name = "age_min")
	private Integer ageMin;

	@Column(nullable = false, name = "age_max")
	private Integer ageMax;

	@Column(nullable = false, name = "contract_period")
	private Integer contractPeriod;

	@Column(nullable = false, name = "premium_period")
	private Integer premiumPeriod;

	@Column(nullable = false, name = "classs")
	private Integer classs;

	@Column(nullable = false, name = "risk")
	private Integer risk;

	@Column(nullable = false, name = "rate")
	private Double rate;

	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}
