package id.equity.nichemarket.model.si;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "t_product_rule_grouping")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class ProductRuleGrouping extends Auditable<String> {
	
	@Column(unique = true, length = 10, nullable = false, name = "product_rule_grouping_code")
	private String productRuleGroupingCode;
	
	@Column(length = 10, name = "product_code")
	private String productCode;
	
	@Column(length = 6, name = "valuta_code")
	private String valutaCode;
	
	@Column(length = 10, name = "product_code_ref")
	private String productCodeRef;
	
	@Column(name = "min")
	private Integer min;
	
	@Column(name = "max")
	private Integer max;
	
	@Column(name = "mandatory")
	private Integer mandatory;
	
	@Column(length = 2, name = "grouping_code")
	private String groupingCode;
	
	@Column(name = "is_active")
	private boolean isActive;
}