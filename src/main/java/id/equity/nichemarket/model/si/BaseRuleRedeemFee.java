package id.equity.nichemarket.model.si;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "t_base_rule_redeem_fee")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@EntityListeners(AuditingEntityListener.class)
public class BaseRuleRedeemFee extends Auditable<String> {

    @Column(unique = true, length = 6, nullable = false, name = "base_rule_redeem_fee_code")
    private String baseRuleRedeemFeeCode;

    @Column(length = 10, nullable = false, name = "product_code")
    private String productCode;

    @Column(length = 6, nullable = false, name = "valuta_code")
    private String valutaCode;

    @Column(length = 3, nullable = false, name = "payment_period_code")
    private String paymentPeriodCode;

    @Column(nullable = false, name = "year_th")
    private Integer yearTh;

    @Column(nullable = false, name = "rate")
    private Double rate;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
