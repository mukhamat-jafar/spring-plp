package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@Entity
@Table(name = "m_appointment_time")
public class AppointmentTime extends Auditable<String> {
		
	@Column(unique = true, length = 3, nullable = false, name = "appointment_time_code")
	private String appointmentTimeCode;
	
	@Column(nullable = false, length = 50, name = "description")
	private String description;
	
	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}