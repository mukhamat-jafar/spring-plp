package id.equity.nichemarket.model.mgm;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_recipient_type")
public class RecipientType extends Auditable<String> {
		
	@Column(unique = true, length = 3, nullable = false, name = "recipient_type_code")
	private String recipientTypeCode;
	
	@Column(nullable = false, length = 50, name = "description")
	private String description;

	@Column(nullable = false, name = "is_internal")
	private boolean isInternal;

	@Column(nullable = false, name = "is_active")
	private boolean isActive;
}