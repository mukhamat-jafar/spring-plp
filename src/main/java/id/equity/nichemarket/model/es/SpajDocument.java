package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_document")
public class SpajDocument extends Auditable<String> {

    @Column(unique = true, length = 12, name = "spaj_document_code")
    private String spajDocumentCode;

    @Column(nullable = false, name = "unique_id")
    private String uniqueId;

    @Column(length = 1, nullable = false)
    private String mode;

    @Column(length = 12, nullable = false)
    private String source;

    @Column(length = 15, nullable = false, name = "document_type")
    private String documentType;

    @Column(length = 12)
    private String reference;

    @Column(length = 11, name = "spaj_no")
    private String spajNo;

    @Column(length = 10, nullable = false, name = "marketing_office")
    private String marketingOffice;

    @Column(length = 30, nullable = false, name = "policy_no")
    private String policyNo;

    @Column(length = 11, name = "proposal_no")
    private String proposalNo;

    @Column(length = 15, name = "marketing_program")
    private String marketingProgram;

    @Column(length = 15, name = "soa_code")
    private String soaCode;

    @Column(length = 500, name = "underwriting_notes")
    private boolean underwritingNotes;
}