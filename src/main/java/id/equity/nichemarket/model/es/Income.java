package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_income")
public class Income extends Auditable<String> {
    @Column(unique = true, length = 4, nullable = false, name = "income_code")
    private String incomeCode;
    @Column(length = 30, nullable = false, name = "description")
    private String description;
    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
