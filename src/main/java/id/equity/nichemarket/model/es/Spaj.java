package id.equity.nichemarket.model.es;


import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Spaj extends Auditable<String> {

    @Column(unique = true, length = 12, name = "spaj_code")
    private String spajCode;

    @Column(nullable = false, length = 2, name = "is_active")
    private boolean isActive;
}
