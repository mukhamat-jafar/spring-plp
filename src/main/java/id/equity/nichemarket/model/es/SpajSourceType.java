package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_source_type")
public class SpajSourceType extends Auditable<String> {

    @Column(unique = true, length = 12, nullable = false, name = "spaj_source_type_code")
    private String spajSourceTypeCode;

    @Column(length = 15, nullable = false, name = "spaj_source_type_name")
    private String spajSourceTypeName;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}
