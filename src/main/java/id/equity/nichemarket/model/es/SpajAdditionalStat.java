package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_spaj_additional_stat")
public class SpajAdditionalStat extends Auditable<String> {

    @Column(unique = true, length = 12, name = "spaj_additional_stat_code")
    private String spajAdditionalStatCode;

    @Column(length = 12, name = "spaj_document_code")
    private String spajDocumentCode;

    @Column(length = 12, name = "spaj_no")
    private String spajNo;

    @Column(length = 4, name = "domicile_status")
    private String domicileStatus;

    @Column(length = 60, name = "other_domicile")
    private String otherDomicile;

    @Column(length = 20, name = "gross_income")
    private String grossIncome;

    @Column(length = 4, name = "source_of_fund")
    private String sourceOfFund;

    @Column(length = 30, name = "other_source_of_fund")
    private String otherSourceOfFund;

    @Column(length = 4, name = "purpose_buy")
    private String purposeBuy;

    @Column(length = 30, name = "other_purpose_by")
    private String otherPurposeBy;

    @Column(nullable = false, name = "is_active")
    private boolean isActive;

}
