package id.equity.nichemarket.model.es;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import id.equity.nichemarket.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "m_document_checklist")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DocumentChecklist extends Auditable<String>{
	
	@Column(unique = true, length = 60, nullable = false, name = "payment_method_code")
    private String paymentMethodCode;

	@Column(length = 100, nullable = false,name = "description")
    private String description;
	
	@Column(length = 6, nullable = false, name = "tmp_code")
    private String tmpCode;
	
	@Column(nullable = false, name = "is_active")
    private boolean isActive;
}