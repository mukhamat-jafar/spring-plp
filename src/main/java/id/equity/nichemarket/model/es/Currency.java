package id.equity.nichemarket.model.es;

import id.equity.nichemarket.audit.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_currency")
public class Currency extends Auditable<String> {
	
    @Column(unique = true, length = 6, nullable = false, name = "currency_code")
    private String currencyCode;
    @Column(length = 100, nullable = false, name = "description")
    private String description;
    @Column(nullable = false, name = "is_active")
    private boolean isActive;
}