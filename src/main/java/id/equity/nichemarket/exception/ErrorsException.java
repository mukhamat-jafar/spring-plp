package id.equity.nichemarket.exception;

public class ErrorsException extends RuntimeException{
    public ErrorsException(String message) {
        super(message);
    }

    public ErrorsException(String message, Throwable cause) {
        super(message, cause);
    }
}
