package id.equity.nichemarket.dto.mgm.dataFromLandingPage;

import lombok.Data;

@Data
public class DataFromLandingPage2 {
    private String message_type;
    private String type_code;
    private String variable_no_1;
    private String variable_name_1;
    private String variable_date_1;
    private String variable_sum_1;
    private String variable_date_2;
    private String variable_sum_2;
    private String sent_date;
    private String phone_no;
    private String email_address;
    private String valuta_id;
    private String va_bca;
    private String va_permata;
    private String nav;
    private String gender_code;
    private String variable_no_2;
    private String variable_sum_3;
    private String variable_name_2;
    private String notes;
    private String variable_no_3;

}
