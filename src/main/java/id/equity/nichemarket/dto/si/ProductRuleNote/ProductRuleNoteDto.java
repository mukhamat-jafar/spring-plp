package id.equity.nichemarket.dto.si.ProductRuleNote;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ProductRuleNoteDto {
	
	private long id;
	private String productRuleNoteCode;
	private String productCode;
	private String valutaCode;
	private String pageCode;
	private String fillPhPbPf;
	private String fillHD;
	private Integer order;
	private String description;
	private String otherDescription;
	private boolean isActive;
}