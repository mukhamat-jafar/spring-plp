package id.equity.nichemarket.dto.si.PaymentPeriod;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreatePaymentPeriod extends PaymentPeriodDto {

}
