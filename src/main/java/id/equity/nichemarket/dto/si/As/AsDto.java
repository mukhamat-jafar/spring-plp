package id.equity.nichemarket.dto.si.As;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AsDto {
	

	private long id;
	private String asCode;
	private String description;
	private String asCodeString;
	private boolean isActive;
}
