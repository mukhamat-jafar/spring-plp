package id.equity.nichemarket.dto.si.BaseRuleAs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRuleAsDto {

	private long id;
	private String baseRuleAsCode;
	private String productCode;
	private String asCode;
	private int minAge;
	private int maxAge;
	private boolean isActive;
}