package id.equity.nichemarket.dto.si.ProductChannel;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductChannelDto {
    private Long id;
    private String productChannelCode;
    private String channelCode;
    private String productCode;
    private boolean isActive;
}
