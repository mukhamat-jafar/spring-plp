package id.equity.nichemarket.dto.si.ProductRuleActivate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductRuleActivateDto {
	
	private long id;
	private String productRuleActivateCode;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private String genderCode;
	private String asCode;
	private String relationTypeCode;
	private Integer min;
	private Integer max;
	private String rulePremium;
	private boolean isActive;
}