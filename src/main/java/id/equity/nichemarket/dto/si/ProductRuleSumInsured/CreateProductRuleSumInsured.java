package id.equity.nichemarket.dto.si.ProductRuleSumInsured;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleSumInsured extends ProductRuleSumInsuredDto {

}
