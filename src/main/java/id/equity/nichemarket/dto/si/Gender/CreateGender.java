package id.equity.nichemarket.dto.si.Gender;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateGender extends GenderDto {
}
