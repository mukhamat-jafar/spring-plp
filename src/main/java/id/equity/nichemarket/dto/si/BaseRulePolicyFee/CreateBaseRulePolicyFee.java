package id.equity.nichemarket.dto.si.BaseRulePolicyFee;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateBaseRulePolicyFee extends BaseRulePolicyFeeDto {

}
