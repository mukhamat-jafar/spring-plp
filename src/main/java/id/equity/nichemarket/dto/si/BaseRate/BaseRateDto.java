package id.equity.nichemarket.dto.si.BaseRate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRateDto {
	
	private long id;
	private String baseRateCode;
	private String category;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private String asCode;
	private int age;
	private int contractPeriod;
	private int premiumPeriod;
	private int yearTh;
	private int ageTh;
	private int rate;
	private int divider;
	private boolean isActive;
}
