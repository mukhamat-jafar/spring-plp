package id.equity.nichemarket.dto.si.ProductRate;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRate extends ProductRateDto{

}
