package id.equity.nichemarket.dto.si.InvestType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class InvestTypeDto {
	
	private long id;
	private String investTypeCode;
	private String description;
	private boolean isActive;
}
