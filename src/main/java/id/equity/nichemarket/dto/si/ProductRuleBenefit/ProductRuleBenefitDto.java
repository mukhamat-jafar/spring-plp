package id.equity.nichemarket.dto.si.ProductRuleBenefit;

import lombok.Data;

@Data
public class ProductRuleBenefitDto {
	
	private long id;
	private String productRuleBenefitCode;
	private String productCode;
	private String valutaCode;
	private String title;
	private String headerTitle;
	private String headerValue;
	private String footerTitle;
	private String footerValue;
	private boolean isActive;
}