package id.equity.nichemarket.dto.si.BaseRulePaymentPeriod;

import lombok.Data;

@Data
public class BaseRulePaymentPeriodDto {
    private Long id;
    private String baseRulePaymentPeriodCode;
    private String productCode;
    private String valutaCode;
    private String paymentPeriodCode;
    private Integer factor;
    private boolean isActive;
}
