package id.equity.nichemarket.dto.si.BaseRuleContractPeriod;

import lombok.Data;

@Data
public class BaseRuleContractPeriodDto {
    private Long id;
    private String baseRuleContractPeriodCode;
    private String productCode;
    private String paymentPeriodCode;
    private Integer min;
    private Integer max;
    private Integer step;
    private Integer maxAge;
    private boolean isActive;

}
