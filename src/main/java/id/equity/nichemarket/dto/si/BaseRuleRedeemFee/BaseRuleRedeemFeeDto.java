package id.equity.nichemarket.dto.si.BaseRuleRedeemFee;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseRuleRedeemFeeDto {
    private Long id;
    private String baseRuleRedeemFeeCode;
    private String productCode;
    private String valutaCode;
    private String paymentPeriodCode;
    private Integer yearTh;
    private Double rate;
    private boolean isActive;
}
