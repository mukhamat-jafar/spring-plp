package id.equity.nichemarket.dto.si.ProductRuleClass;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleClass extends ProductRuleClassDto {

}
