package id.equity.nichemarket.dto.si.ProductRuleGroupingSumInsured;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleGroupingSumInsured extends ProductRuleGroupingSumInsuredDto{

}
