package id.equity.nichemarket.dto.si.ProductRuleBenefitItem;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleBenefitItem extends ProductRuleBenefitItemDto {

}
