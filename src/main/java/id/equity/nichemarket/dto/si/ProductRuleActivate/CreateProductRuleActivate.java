package id.equity.nichemarket.dto.si.ProductRuleActivate;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleActivate extends ProductRuleActivateDto{

}
