package id.equity.nichemarket.dto.si.ProductRuleRisk;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleRisk extends ProductRuleRiskDto {

}
