package id.equity.nichemarket.dto.si.ProductRuleImage;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateProductRuleImage extends ProductRuleImageDto {

}
