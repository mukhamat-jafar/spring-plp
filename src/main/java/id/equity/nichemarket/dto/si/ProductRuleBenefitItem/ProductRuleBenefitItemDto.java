package id.equity.nichemarket.dto.si.ProductRuleBenefitItem;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ProductRuleBenefitItemDto {
	
	private long id;
	private String productRuleBenefitItemCode;
	private String productCode;
	private Integer order;
	private String description;
	private String value;
	private boolean isActive;
}