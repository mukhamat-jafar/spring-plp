package id.equity.nichemarket.dto.si.ProductRuleClass;

import lombok.Data;

@Data
public class ProductRuleClassDto {
	
	private long id;
	private String productRuleClassCode;
	private String productCode;
	private String paymentPeriodCode;
	private Double maxSumInsuredBase;
	private Double maxPremiumPeriodic;
	private Double maxTuPeriodic;
	private Double classes;
	private boolean isActive;
}