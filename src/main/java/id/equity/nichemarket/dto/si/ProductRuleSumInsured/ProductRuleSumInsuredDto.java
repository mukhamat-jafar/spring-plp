package id.equity.nichemarket.dto.si.ProductRuleSumInsured;

import lombok.Data;

@Data
public class ProductRuleSumInsuredDto {
	
	private long id;
	private String productRuleSumInsuredCode;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private String asCode;
	private Integer minAge;
	private Integer maxAge;
	private Double minPremiumPeriodic;
	private Double maxPremiumPeriodic;
	private Double minSumInsuredBase;
	private Double maxSumInsuredBase;
	private String classes;
	private String risk;
	private String minSumInsuredFormula;
	private String maxSumInsuredFormula;
	private Double stepSumInsured;
	private boolean isActive;
}