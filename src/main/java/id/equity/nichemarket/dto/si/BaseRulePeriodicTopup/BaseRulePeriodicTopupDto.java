package id.equity.nichemarket.dto.si.BaseRulePeriodicTopup;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseRulePeriodicTopupDto {
	
	private long id;
	private String baseRulePeriodicTopupCode;
	private String productCode;
	private String valutaCode;
	private String paymentPeriodCode;
	private Integer min;
	private Integer max;
	private Integer step;
	private boolean isActive;
}