package id.equity.nichemarket.dto.es.Income;

import lombok.Data;

@Data
public class IncomeDto {
    private Long id;
    private String incomeCode;
    private String description;
    private boolean isActive;
}
