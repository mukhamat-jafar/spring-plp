package id.equity.nichemarket.dto.es.AddressType;

import lombok.Data;

@Data
public class AddressTypeDto {
    private Long id;
    private String addressTypeCode;
    private String description;
    private boolean isActive;
}
