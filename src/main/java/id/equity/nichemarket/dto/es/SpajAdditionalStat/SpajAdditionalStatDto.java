package id.equity.nichemarket.dto.es.SpajAdditionalStat;

import lombok.Data;

@Data
public class SpajAdditionalStatDto {
    private Long id;
    private String spajAdditionalStatCode;
    private String spajDocumentCode;
    private String spajNo;
    private String domicileStatus;
    private String otherDomicile;
    private String grossIncome;
    private String sourceOfFund;
    private String otherSourceOfFund;
    private String otherPurposeBy;
    private boolean isActive;
}
