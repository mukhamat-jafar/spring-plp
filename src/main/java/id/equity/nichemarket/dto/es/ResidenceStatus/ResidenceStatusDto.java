package id.equity.nichemarket.dto.es.ResidenceStatus;

import lombok.Data;

@Data
public class ResidenceStatusDto {
    private Long id;
    private String residenceStatusCode;
    private String description;
    private boolean isActive;
}
