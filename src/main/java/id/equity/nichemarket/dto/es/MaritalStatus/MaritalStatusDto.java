package id.equity.nichemarket.dto.es.MaritalStatus;

import lombok.Data;

@Data
public class MaritalStatusDto {

    private Long id;
    private String maritalStatusCode;
    private String description;
    private boolean isActive;
}
