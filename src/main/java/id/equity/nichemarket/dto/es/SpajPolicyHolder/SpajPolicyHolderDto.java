package id.equity.nichemarket.dto.es.SpajPolicyHolder;

import lombok.Data;

import java.sql.Date;

@Data
public class SpajPolicyHolderDto {
    private Long id;
    private String spajPolicyHolderCode;
    private String spajDocumentCode;
    private String spajNo;
    private String fullName;
    private String placeOfBirth;
    private Date dateOfBirth;
    private String identityType;
    private String identityNo;
    private String citizenship;
    private String npwpNo;
    private String gender;
    private String maritalStatus;
    private String religion;
    private String religionOther;
    private String homeAddress1;
    private String homeAddress2;
    private String homeAddress3;
    private String homeCity;
    private String homeZipcode;
    private String homeProvince;
    private String homeCountry;
    private String homePhone;
    private String homeHandphone;
    private String emailAddress;
    private String lastEducation;
    private String office;
    private String position;
    private String industry;
    private String officeAddress1;
    private String officeAddress2;
    private String officeAddress3;
    private String officeCity;
    private String officeZipcode;
    private String officeProvince;
    private String officeCountry;
    private String officePhone;
    private String officeHandphone;
    private String correspondenceAddress;
    private String otherCorrespondenceAddress1;
    private String otherCorrespondenceAddress2;
    private String otherCorrespondenceAddress3;
    private String otherCorrespondenceCity;
    private String otherCorrespondenceZipcode;
    private String otherCorrespondenceProvince;
    private String otherCorrespondenceCountry;
    private String otherCorrespondencePhone;
    private String otherCorrespondenceHandphone;
    private String relationship;
    private String otherRelationship;
    private String jobLevel;
    private boolean isActive;
}
