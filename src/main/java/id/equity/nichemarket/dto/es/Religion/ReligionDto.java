package id.equity.nichemarket.dto.es.Religion;

import lombok.Data;

@Data
public class ReligionDto {
    
	private Long id;
	private String religionCode;
    private String description;
    private boolean isActive;
}