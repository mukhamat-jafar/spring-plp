package id.equity.nichemarket.dto.es.SpajDocument;

import lombok.Data;

@Data
public class SpajDocumentDto {
    private Long id;
    private String spajDocumentCode;
    private String uniqueId;
    private String mode;
    private String source;
    private String documentType;
    private String reference;
    private String spajNo;
    private String marketingOffice;
    private String policyNo;
    private String proposalNo;
    private String marketingProgram;
    private String soaCode;
    private boolean underwritingNotes;
}
