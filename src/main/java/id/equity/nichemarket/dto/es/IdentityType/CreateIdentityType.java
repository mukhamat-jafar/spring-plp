package id.equity.nichemarket.dto.es.IdentityType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateIdentityType extends IdentityTypeDto{ 
	
}
