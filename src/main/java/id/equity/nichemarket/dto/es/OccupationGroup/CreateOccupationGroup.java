package id.equity.nichemarket.dto.es.OccupationGroup;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateOccupationGroup extends OccupationGroupDto{ 

}
