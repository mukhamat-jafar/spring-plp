package id.equity.nichemarket.dto.es.SpajPremiumPayment;

import lombok.Data;

@Data
public class SpajPremiumPaymentDto {
    
	private Long id;
	private String spajPremiumPaymentCode;
	private String spajDocumentCode;
	private String spajNo;
	private String premiumPaymentMethod;
	private String bankName;
	private String otherBank;
	private boolean isActive;
}