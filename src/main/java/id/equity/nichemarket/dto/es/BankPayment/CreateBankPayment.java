package id.equity.nichemarket.dto.es.BankPayment;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateBankPayment extends BankPaymentDto{ 
	
}
