package id.equity.nichemarket.dto.es.Province;

import lombok.Data;

@Data
public class ProvinceDto {
    private Long id;
    private String provinceCode;
    private String description;
    private boolean isActive;
}
