package id.equity.nichemarket.dto.es.ResidenceStatus;

import lombok.Data;

@Data
public class CreateResidenceStatus extends ResidenceStatusDto {
}
