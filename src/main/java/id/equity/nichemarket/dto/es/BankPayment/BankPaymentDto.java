package id.equity.nichemarket.dto.es.BankPayment;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BankPaymentDto {
	
	private long id;
	private String bankPaymentCode;
	private String description;
	private boolean isActive;
}
