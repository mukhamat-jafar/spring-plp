package id.equity.nichemarket.dto.es.Purpose;

import lombok.Data;

@Data
public class PurposeDto {
    private Long id;
    private String purposeCode;
    private String description;
    private boolean isActive;
}
