package id.equity.nichemarket.dto.es.OccupationGroup;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OccupationGroupDto {
	
	private long id;
	private String occupationGroupCode;
	private String description;
	private boolean isActive;
}
