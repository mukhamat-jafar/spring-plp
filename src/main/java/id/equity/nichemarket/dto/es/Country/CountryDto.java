package id.equity.nichemarket.dto.es.Country;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountryDto {
	
	private long id;
	private String countryCode;
	private String description;
	private boolean isActive;
}
