package id.equity.nichemarket.dto.es.InsuredRelation;

import lombok.Data;

@Data
public class InsuredRelationDto {
    
	private Long id;
	private String insuredRelationCode;
    private String description;
    private boolean isActive;
}