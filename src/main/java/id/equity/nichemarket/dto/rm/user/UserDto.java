package id.equity.nichemarket.dto.rm.user;

import id.equity.nichemarket.validator.usertype.ExistUserTypeValidator;
import lombok.Data;

import javax.persistence.MappedSuperclass;


@Data
@MappedSuperclass
public class UserDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    @ExistUserTypeValidator
    private String userTypeCode;
}
