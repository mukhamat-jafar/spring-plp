package id.equity.nichemarket.dto.rm.Menu;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MenuDto {
	
	private Long id;
	private String menuCode;
	private String description;
	private String uri;
	private Integer parentId;
	private boolean isActive;
}
