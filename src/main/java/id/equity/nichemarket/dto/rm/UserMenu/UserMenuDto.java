package id.equity.nichemarket.dto.rm.UserMenu;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@NoArgsConstructor
public class UserMenuDto {
	
	private Long id;
	private String userMenuCode;
	private String username;
	private String menuCode;
	private boolean isActive;
}
