package id.equity.nichemarket.dto.rm.GenderRM;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GenderRMDto {
	
	private Long id;
	private String genderCode;
	private String description;
	private boolean isActive;
}
