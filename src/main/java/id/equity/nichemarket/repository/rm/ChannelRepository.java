package id.equity.nichemarket.repository.rm;

import id.equity.nichemarket.model.rm.Channel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChannelRepository extends JpaRepository<Channel, Long> {

}
