package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query("select c from Customer c where c.customerCode = ?1")
    Customer findByCustomerCode(String customerCode);
}