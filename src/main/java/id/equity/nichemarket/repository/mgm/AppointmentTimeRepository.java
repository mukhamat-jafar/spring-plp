package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.AppointmentTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentTimeRepository extends JpaRepository<AppointmentTime, Long> {
}