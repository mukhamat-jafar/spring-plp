package id.equity.nichemarket.repository.mgm;

import id.equity.nichemarket.model.mgm.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
}