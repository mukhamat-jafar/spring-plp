package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductChannel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductChannelRepository extends JpaRepository<ProductChannel, Long> {

}
