package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.BaseRuleInvestType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseRuleInvestTypeRepository extends JpaRepository<BaseRuleInvestType, Long> {
}
