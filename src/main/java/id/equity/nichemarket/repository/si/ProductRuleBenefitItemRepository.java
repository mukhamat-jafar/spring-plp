package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRuleBenefitItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRuleBenefitItemRepository extends JpaRepository<ProductRuleBenefitItem, Long>{

}
