package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.BaseRuleClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BaseRuleClientRepository extends JpaRepository<BaseRuleClient, Long> {
}
