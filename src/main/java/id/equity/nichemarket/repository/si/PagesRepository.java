package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.Pages;
import id.equity.nichemarket.model.si.PaymentPeriod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagesRepository extends JpaRepository<Pages, Long> {

}
