package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRuleSumInsured;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRuleSumInsuredRepository extends JpaRepository<ProductRuleSumInsured, Long>{

}
