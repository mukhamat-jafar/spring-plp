package id.equity.nichemarket.repository.si;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.si.BaseRuleAdmin;

@Repository
public interface BaseRuleAdminRepository extends JpaRepository<BaseRuleAdmin, Long>{

}
