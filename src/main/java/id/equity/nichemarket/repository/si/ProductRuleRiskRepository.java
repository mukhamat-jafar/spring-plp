package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRuleRisk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRuleRiskRepository extends JpaRepository<ProductRuleRisk, Long>{

}
