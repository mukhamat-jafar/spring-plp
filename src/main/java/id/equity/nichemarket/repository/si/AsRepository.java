package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.As;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AsRepository extends JpaRepository<As, Long>{

}
