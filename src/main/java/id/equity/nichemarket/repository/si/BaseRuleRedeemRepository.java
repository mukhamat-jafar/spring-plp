package id.equity.nichemarket.repository.si;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.si.BaseRuleRedeem;

@Repository
public interface BaseRuleRedeemRepository extends JpaRepository<BaseRuleRedeem, Long>{

}
