package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRuleRequired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRuleRequiredRepository extends JpaRepository<ProductRuleRequired, Long>{

}
