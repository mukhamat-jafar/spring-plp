package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.BaseRuleRedeemFee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BaseRuleRedeemFeeDtoRepository extends JpaRepository<BaseRuleRedeemFee, Long> {
}
