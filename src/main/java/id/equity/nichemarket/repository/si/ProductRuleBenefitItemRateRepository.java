package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRuleBenefitItemRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRuleBenefitItemRateRepository extends JpaRepository<ProductRuleBenefitItemRate, Long>{

}
