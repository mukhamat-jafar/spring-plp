package id.equity.nichemarket.repository.si;

import id.equity.nichemarket.model.si.ProductRuleClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRuleClassRepository extends JpaRepository<ProductRuleClass, Long>{

}
