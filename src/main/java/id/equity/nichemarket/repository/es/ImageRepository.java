package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
}
