package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajCoveragePremiumDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpajCoveragePremiumDetailRepository extends JpaRepository<SpajCoveragePremiumDetail, Long> {
}
