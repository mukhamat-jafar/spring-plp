package id.equity.nichemarket.repository.es;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import id.equity.nichemarket.model.es.Proposal;

@Repository
public interface ProposalRepository extends JpaRepository<Proposal, Long>{

}
