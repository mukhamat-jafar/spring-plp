package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRespository extends JpaRepository<Currency, Long> {
}
