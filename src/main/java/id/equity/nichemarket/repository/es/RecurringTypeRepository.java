package id.equity.nichemarket.repository.es;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
import id.equity.nichemarket.model.es.RecurringType;

@Repository
public interface RecurringTypeRepository extends JpaRepository<RecurringType, Long>{

}
