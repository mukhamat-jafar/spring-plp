package id.equity.nichemarket.repository.es;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import id.equity.nichemarket.model.es.OccupationGroup;

@Repository
public interface OccupationGroupRepository extends JpaRepository<OccupationGroup, Long>{

}
