package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajDocumentHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpajDocumentHistoryRepository extends JpaRepository<SpajDocumentHistory, Long> {
    @Query("select s from SpajDocumentHistory s where s.status IN (:failed, :faileddoc)")
    List<SpajDocumentHistory> findByStatus(String failed, String faileddoc);

    @Query("select s from SpajDocumentHistory s where s.spajDocumentCode = ?1")
    SpajDocumentHistory findByDocumentCode(String spajDocumentCode);
}
