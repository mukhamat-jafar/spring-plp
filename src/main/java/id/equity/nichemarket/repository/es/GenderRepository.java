package id.equity.nichemarket.repository.es;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import id.equity.nichemarket.model.es.Gender;

@Repository
public interface GenderRepository extends JpaRepository<Gender, Long> {
    @Query("select g from Gender g where g.genderCode = ?1")
    Gender findByGenderCode(String genderCOde);
}
