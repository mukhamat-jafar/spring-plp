package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.SpajPolicyHolder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpajPolicyHolderRepository extends JpaRepository<SpajPolicyHolder, Long> {
}
