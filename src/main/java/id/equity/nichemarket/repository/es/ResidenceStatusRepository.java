package id.equity.nichemarket.repository.es;

import id.equity.nichemarket.model.es.ResidenceStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResidenceStatusRepository extends JpaRepository<ResidenceStatus, Long> {
}
